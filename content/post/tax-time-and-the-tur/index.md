---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Tax Time and the TUR (Transit Tax Credit)
subtitle: ""
slug: tax-time-and-the-tur
summary: "Crunching the numbers of my Transit Usage Report."
authors: []
categories:
  - R
tags: []
date: 2018-02-24
lastmod: 2018-02-25
rmd_hash: 303092cfacbe272c

---

*Disclaimer: None of the contents of this blog post should be constrewed as tax advice. If you have any questions about your taxes, speak to an expert.*

It is tax time in Canada, which means that there are a ton of numbers to look over and details to review. One of those details is my public transit spending. Canadians can claim their public transit spending on [Line 364--Public transit amount](https://www.canada.ca/en/revenue-agency/services/tax/individuals/topics/about-your-tax-return/tax-return/completing-a-tax-return/deductions-credits-expenses/line-364-public-transit-amount.html). The credit was discontinued last year in a surprisingly hypocritical move by our "pro-environment" government. This year is the last year that the credit can be claimed, for the period from January 1, 2017 to June 30, 2017.

If you are a Metropass user, it is super easy to calculate your transit fare. You simply need to add up the cost of each Metropass you purchased for the period. It has been a very long time since I have used a Metropass, as I usually end up spending less than the \$116.75 that a Post-Secondary Metropass costs. Normally, day-use purchases such as tickets or tokens do not qualify for the Public Transit Credit. Presto cards, however, do qualify, based on some [specific criteria](https://www.canada.ca/en/revenue-agency/services/tax/individuals/topics/about-your-tax-return/tax-return/completing-a-tax-return/deductions-credits-expenses/line-364-public-transit-amount/eligibility.html) laid out on the CRA website.

Presto users can claim the cost of any series of at least 32 one-way trips taken on a single transit service within a 31-day (or shorter) period. To do so, one must first download their [Transit Usage Report](https://www.prestocard.ca/en/about/tax-credit). In the post, I will poke around my own TUR in R to see if I can maximize the amount that I can claim. The data in this report is real, but I have removed the location information for a smidgen of personal privacy. I should also note that I first opened my TUR in LibreOffice to clean up the date and amount column, which weren't R-friendly.

As this is an R post. First, I will load the libraries I will be using:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tidyr.tidyverse.org'>tidyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://lubridate.tidyverse.org'>lubridate</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://zoo.R-Forge.R-project.org/'>zoo</a></span><span class='o'>)</span>
</code></pre>

</div>

Now I will read in my TUR from the `.csv` file, clean up the date using `lubridate`, and filter to the TTC, which is the only agency that I use enough to qualify for the tax credit.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nf'>readr</span><span class='nf'>::</span><span class='nf'><a href='https://readr.tidyverse.org/reference/read_delim.html'>read_csv</a></span><span class='o'>(</span><span class='s'>"../../../static/files/csv/CONOR_TUR_2018.csv"</span>, col_types <span class='o'>=</span> <span class='s'>"Dccd"</span><span class='o'>)</span>

<span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nv'>dat</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Date <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/ymd.html'>ymd</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>`Transit Agency`</span> <span class='o'>==</span> <span class='s'>"Toronto Transit Commission"</span><span class='o'>)</span>
  
<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 4</span></span>
<span class='c'>#&gt;   Date       `Transit Agency`           Type         Amount</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>                      </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>         </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2017-01-01 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2017-01-02 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2017-01-02 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2017-01-03 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2017-01-03 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2017-01-04 Toronto Transit Commission Fare Payment     -</span><span style='color: #BB0000;'>3</span></span>
</code></pre>

</div>

First, let's see if I was right to forego Metropasses last year:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Yearmon <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Yearmon</span>, <span class='nv'>Amount</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>sum</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 9 x 2</span></span>
<span class='c'>#&gt;   Yearmon   Amount</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;yearmon&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> Jan 2017     -</span><span style='color: #BB0000;'>66</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> Feb 2017    -</span><span style='color: #BB0000;'>126</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> Mar 2017    -</span><span style='color: #BB0000;'>120</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> Apr 2017     -</span><span style='color: #BB0000;'>84</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> Aug 2017     -</span><span style='color: #BB0000;'>42</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> Sep 2017    -</span><span style='color: #BB0000;'>102</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>7</span><span> Oct 2017    -</span><span style='color: #BB0000;'>108</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>8</span><span> Nov 2017    -</span><span style='color: #BB0000;'>132</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>9</span><span> Dec 2017    -</span><span style='color: #BB0000;'>108</span></span>
</code></pre>

</div>

Oops, it looks like I should have bought a Metropass in February, March, and November of last year, as it would have been cheaper than what I paid in individual (\$3) fares. Shoot!

All is not lost, however, because I can still claim some of that transit spending back on my tax return. The TUR has each transaction listed individually, but I'm interested in how many transactions I make over a given time period. As such, I will first add up any transactions made on the same day of the week, and also pad the list with complete dates from January 1 to June 30.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nv'>dat</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>Amount</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>trips <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/context.html'>n</a></span><span class='o'>(</span><span class='o'>)</span>, amount <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>Amount</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nv'>ungroup</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>


<span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nv'>dat</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/complete.html'>complete</a></span><span class='o'>(</span>Date <span class='o'>=</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/full_seq.html'>full_seq</a></span><span class='o'>(</span><span class='nv'>Date</span>, period <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span>, fill <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>trips <span class='o'>=</span> <span class='m'>0</span>, amount <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Date</span> <span class='o'>&lt;=</span> <span class='s'>"2017-06-30"</span><span class='o'>)</span>

<span class='nv'>dat</span><span class='o'>[</span><span class='m'>94</span><span class='o'>:</span><span class='m'>102</span>,<span class='o'>]</span> <span class='c'># Just as an example</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 9 x 3</span></span>
<span class='c'>#&gt;   Date       trips amount</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2017-04-04     2     -</span><span style='color: #BB0000;'>6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2017-04-05     0      0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2017-04-06     2     -</span><span style='color: #BB0000;'>6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2017-04-07     2     -</span><span style='color: #BB0000;'>6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2017-04-08     0      0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2017-04-09     0      0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>7</span><span> 2017-04-10     2     -</span><span style='color: #BB0000;'>6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>8</span><span> 2017-04-11     1     -</span><span style='color: #BB0000;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>9</span><span> 2017-04-12     2     -</span><span style='color: #BB0000;'>6</span></span>
</code></pre>

</div>

The CRA doesn't specify that the 31-day periods that we should examine have to line up with the start of the month, so I'll check for rolling 31-day periods with start dates on January 1 through January 31.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>out</span> <span class='o'>&lt;-</span> <span class='kc'>NULL</span>
<span class='kr'>for</span> <span class='o'>(</span><span class='nv'>i</span> <span class='kr'>in</span> <span class='m'>1</span><span class='o'>:</span><span class='m'>31</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='c'># Calculate cumulative trips and fares over rolling 31-day periods</span>
  <span class='nv'>result</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>trips <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>)</span><span class='o'>]</span>, width <span class='o'>=</span> <span class='m'>31</span>, by <span class='o'>=</span> <span class='m'>31</span>,
                                      FUN <span class='o'>=</span> <span class='nv'>sum</span>, partial <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
                   amount <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>amount</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>)</span><span class='o'>]</span>, width <span class='o'>=</span> <span class='m'>31</span>, by <span class='o'>=</span> <span class='m'>31</span>,
                                        FUN <span class='o'>=</span> <span class='nv'>sum</span>, partial <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nv'>result</span> <span class='o'>&lt;-</span> <span class='nv'>result</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>trips</span> <span class='o'>&gt;=</span> <span class='m'>32</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>sum</span><span class='o'>)</span>
  <span class='nv'>out</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nv'>out</span>, <span class='nv'>result</span><span class='o'>)</span>
<span class='o'>}</span>

<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>out</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 2</span></span>
<span class='c'>#&gt;   trips amount</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>    85   -</span><span style='color: #BB0000;'>255</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>    82   -</span><span style='color: #BB0000;'>246</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>    82   -</span><span style='color: #BB0000;'>246</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>    84   -</span><span style='color: #BB0000;'>252</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>    84   -</span><span style='color: #BB0000;'>252</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span>    82   -</span><span style='color: #BB0000;'>246</span></span>
</code></pre>

</div>

Let's see what my maximum claim might be.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>out</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>out</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>out</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>)</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>

<span class='c'>#&gt; [1] 118</span>

<span class='nv'>out</span><span class='o'>$</span><span class='nv'>amount</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>out</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>out</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>)</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>

<span class='c'>#&gt; [1] -354</span>
</code></pre>

</div>

I can claim 118 trips, which cost a total of \$354, if I consider my rolling 31-day periods to have started on January 21.

Now, the above assumes that the 31-day periods mandated by the CRA be continuous, e.g. January 1 to 31, February 1 to March 2, March 3 to April 2, etc. This isn't actually specified, anywhere on their CRA website, though. I thought it might be interesting to instead look at combinations of independent 31-day periods with *no overlap*. That means that I can try to maximize my claim be windowing my 31-day periods appropriately, while not double-claiming any single day. Let's take a look.

First, I will get cumulative totals for *all* rolling 31-day periods from January 1 through the end of my data, and get the list of dates contained in each period. Since my periods are right-aligned, the dates will be $T_i - 30 \ldots T_i$.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat2</span> <span class='o'>&lt;-</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span><span class='nv'>dat</span>,
              cum_trips <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>trips</span>, width <span class='o'>=</span> <span class='m'>31</span>, FUN <span class='o'>=</span> <span class='nv'>sum</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span>,
                                          partial <span class='o'>=</span> <span class='kc'>TRUE</span>, align <span class='o'>=</span> <span class='s'>"right"</span><span class='o'>)</span>,
              cum_amount <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>amount</span>, width <span class='o'>=</span> <span class='m'>31</span>, FUN <span class='o'>=</span> <span class='nv'>sum</span>,  na.rm <span class='o'>=</span> <span class='kc'>TRUE</span>,
                                      partial <span class='o'>=</span> <span class='kc'>TRUE</span>, align <span class='o'>=</span> <span class='s'>"right"</span><span class='o'>)</span>,
              dates <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>Date</span>,
                             <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.Date.html'>seq.Date</a></span><span class='o'>(</span>from <span class='o'>=</span> <span class='nv'>x</span> <span class='o'>-</span> <span class='m'>30</span>, to <span class='o'>=</span> <span class='nv'>x</span>, by <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span>
              <span class='o'>)</span>

<span class='c'># Filter only those periods that have 32 or more trips.</span>
<span class='nv'>dat2</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>dat2</span>, <span class='nv'>cum_trips</span> <span class='o'>&gt;=</span> <span class='m'>32</span><span class='o'>)</span>

<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>dat2</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 6</span></span>
<span class='c'>#&gt;   Date       trips amount cum_trips cum_amount dates      </span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span>     </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2017-02-16     2     -</span><span style='color: #BB0000;'>6</span><span>        33        -</span><span style='color: #BB0000;'>99</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2017-02-17     2     -</span><span style='color: #BB0000;'>6</span><span>        35       -</span><span style='color: #BB0000;'>105</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2017-02-18     1     -</span><span style='color: #BB0000;'>3</span><span>        36       -</span><span style='color: #BB0000;'>108</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2017-02-19     0      0        36       -</span><span style='color: #BB0000;'>108</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2017-02-20     0      0        36       -</span><span style='color: #BB0000;'>108</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2017-02-21     1     -</span><span style='color: #BB0000;'>3</span><span>        37       -</span><span style='color: #BB0000;'>111</span><span> </span><span style='color: #555555;'>&lt;date [31]&gt;</span></span>
</code></pre>

</div>

Now I am interested in finding out how these periods can be combined. As such, I will craft a function that generates combinations of $m$ such periods, and selects only those that have $31 * m$ unique dates, i.e. no overlapping dates. In my case, I will look for combinations of between 1 and 4 periods. There are two reasons for using a maximum of four periods: first, I was overseas from April 26 to August, so I know that it is futile to look for more 31-day periods from January 1 to June 30; second, and more pragmatically, at $m = 5$ the combination process becomes very slow.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>get_combs</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>m</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='s'>"Getting combinations for"</span>, <span class='nv'>m</span>, <span class='s'>"31-day period(s)."</span><span class='o'>)</span><span class='o'>)</span>
  <span class='c'># Get list of combinations of our date lists</span>
  <span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/utils/combn.html'>combn</a></span><span class='o'>(</span><span class='nv'>dat2</span><span class='o'>$</span><span class='nv'>dates</span>, <span class='nv'>m</span>, simplify <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span>
  <span class='c'># Flatten the second levels (so we have a list of dates, and not a list of lists)</span>
  <span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='nv'>unlist</span><span class='o'>)</span>
  <span class='c'># Get the lengths of unique values in each combination</span>
  <span class='nv'>lengths_unique</span> <span class='o'>&lt;-</span> <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span><span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span><span class='o'>)</span>
  <span class='c'># Choose combinations that have as many unique values as days</span>
  <span class='nv'>combs</span><span class='o'>[</span><span class='nv'>lengths_unique</span> <span class='o'>==</span> <span class='nv'>m</span><span class='o'>*</span><span class='m'>31</span><span class='o'>]</span>
<span class='o'>}</span>

<span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/do.call.html'>do.call</a></span><span class='o'>(</span><span class='s'>"c"</span>, <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>4</span>, <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span><span class='nf'>get_combs</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; [1] "Getting combinations for 1 31-day period(s)."</span>
<span class='c'>#&gt; [1] "Getting combinations for 2 31-day period(s)."</span>
<span class='c'>#&gt; [1] "Getting combinations for 3 31-day period(s)."</span>
<span class='c'>#&gt; [1] "Getting combinations for 4 31-day period(s)."</span>
</code></pre>

</div>

Now that I have combinations of 31-day periods with no overlap, I want to get some values back.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>get_values</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span>trips <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>[</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>%in%</span> <span class='nv'>x</span><span class='o'>]</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
    cost <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>amount</span><span class='o'>[</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>%in%</span> <span class='nv'>x</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
<span class='o'>}</span>

<span class='nv'>values</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/do.call.html'>do.call</a></span><span class='o'>(</span><span class='s'>"bind_rows"</span>, <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='nv'>get_values</span><span class='o'>)</span><span class='o'>)</span>

<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>values</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 2</span></span>
<span class='c'>#&gt;   trips  cost</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>    33   -</span><span style='color: #BB0000;'>99</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>    35  -</span><span style='color: #BB0000;'>105</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>    36  -</span><span style='color: #BB0000;'>108</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>    36  -</span><span style='color: #BB0000;'>108</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>    36  -</span><span style='color: #BB0000;'>108</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span>    37  -</span><span style='color: #BB0000;'>111</span></span>
</code></pre>

</div>

So, did all of that hard work pay off? Let's see:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>[</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>]</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>

<span class='c'>#&gt; [1] 118</span>

<span class='nv'>values</span><span class='o'>$</span><span class='nv'>cost</span><span class='o'>[</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>]</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>

<span class='c'>#&gt; [1] -354</span>
</code></pre>

</div>

Oops, it doesn't look like it! It seems that I had already captured the maximum number of trips in my previous method, so this experiment with combinations proved to be futile. My claim is still at 118 trips with a total cost of \$354.

What's next? Well, I supposed another idea would be to compare all the possible periods of 31 days *or less* that also contain 32 trips or more. ~~For me, I think I'll call it quits with the claim of \$354, after all, this exercise is probably going to cost me more in lost marking time than it is worth on my tax return!~~

**Update 2018/02/25:** It seems I can't rest until I solve something. I had a couple of thoughts as I tried to sleep last night on how I could implement this test. Here is what I came up with.

First, I will find all rolling periods of between 15 and 31 days that contain at least 32 one-way trips.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>get_rolls</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>days</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span><span class='nv'>dat</span>,
         cum_trips <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>trips</span>, width <span class='o'>=</span> <span class='nv'>days</span>, FUN <span class='o'>=</span> <span class='nv'>sum</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span>,
                                          partial <span class='o'>=</span> <span class='kc'>TRUE</span>, align <span class='o'>=</span> <span class='s'>"right"</span><span class='o'>)</span>,
         cum_amount <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/rollapply.html'>rollapplyr</a></span><span class='o'>(</span><span class='nv'>amount</span>, width <span class='o'>=</span> <span class='nv'>days</span>, FUN <span class='o'>=</span> <span class='nv'>sum</span>,  na.rm <span class='o'>=</span> <span class='kc'>TRUE</span>,
                                      partial <span class='o'>=</span> <span class='kc'>TRUE</span>, align <span class='o'>=</span> <span class='s'>"right"</span><span class='o'>)</span>,
         dates <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>$</span><span class='nv'>Date</span>,
                             <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.Date.html'>seq.Date</a></span><span class='o'>(</span>from <span class='o'>=</span> <span class='nv'>x</span> <span class='o'>-</span> <span class='o'>(</span><span class='nv'>days</span> <span class='o'>-</span> <span class='m'>1</span><span class='o'>)</span>, to <span class='o'>=</span> <span class='nv'>x</span>, by <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span>,
         period_size <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='nv'>days</span>, <span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>dat</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>cum_trips</span> <span class='o'>&gt;=</span> <span class='m'>32</span><span class='o'>)</span>
<span class='o'>}</span>

<span class='nv'>rolls</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/do.call.html'>do.call</a></span><span class='o'>(</span><span class='s'>"bind_rows"</span>, <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='m'>15</span><span class='o'>:</span><span class='m'>31</span>, <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span><span class='nf'>get_rolls</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Now, I have 526 periods with 32 trips or more that I could combine. Unfortunately, I won't actually be able to do this! Let's digress for a minute and look at the [number of possible combinations](https://en.wikipedia.org/wiki/Combination#Number_of_k-combinations) for 526 periods using [Ramanujan's approximation](https://en.wikipedia.org/wiki/Factorial#Rate_of_growth_and_approximations_for_large_n). The number of possible combinations of $k$ items from a set of $n$ can be calculated as:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Code from: https://stackoverflow.com/questions/40527010/r-how-can-i-calculate-large-numbers-in-n-choose-k</span>

<span class='nv'>ramanujan</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>{</span>
  <span class='nv'>n</span><span class='o'>*</span><span class='nf'><a href='https://rdrr.io/r/base/Log.html'>log</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span> <span class='o'>-</span> <span class='nv'>n</span> <span class='o'>+</span> <span class='nf'><a href='https://rdrr.io/r/base/Log.html'>log</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>*</span><span class='o'>(</span><span class='m'>1</span> <span class='o'>+</span> <span class='m'>4</span><span class='o'>*</span><span class='nv'>n</span><span class='o'>*</span><span class='o'>(</span><span class='m'>1</span><span class='o'>+</span><span class='m'>2</span><span class='o'>*</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>/</span><span class='m'>6</span> <span class='o'>+</span> <span class='nf'><a href='https://rdrr.io/r/base/Log.html'>log</a></span><span class='o'>(</span><span class='nv'>pi</span><span class='o'>)</span><span class='o'>/</span><span class='m'>2</span>
<span class='o'>}</span>

<span class='nv'>ncombs</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>n</span>,<span class='nv'>k</span><span class='o'>)</span><span class='o'>{</span>
  <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/Log.html'>exp</a></span><span class='o'>(</span><span class='nf'>ramanujan</span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span> <span class='o'>-</span> <span class='nf'>ramanujan</span><span class='o'>(</span><span class='nv'>k</span><span class='o'>)</span> <span class='o'>-</span> <span class='nf'>ramanujan</span><span class='o'>(</span><span class='nv'>n</span><span class='o'>-</span><span class='nv'>k</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
<span class='o'>}</span>
</code></pre>

</div>

Therefore, the approximate number of possible combinations of three periods is:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>ncombs</span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>rolls</span><span class='o'>)</span>, <span class='m'>3</span><span class='o'>)</span>

<span class='c'>#&gt; [1] 24117431</span>
</code></pre>

</div>

While that number isn't *huge*, it doesn't take long for the combinations to fill the 8 gigs of RAM on my laptop and cause the machine to lock up. A more advanced approach is necessary, but also out of my wheelhouse. For now, as a proof of concept, I'll just run the combinations with $k$ of 1 (528 periods) and 2 ($\ensuremath{1.3808\times 10^{5}}$ combinations) using a slightly tweaked version of the formula I used above.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>get_combs_from_rolls</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='s'>"Getting combinations for"</span>, <span class='nv'>n</span>, <span class='s'>"period(s)."</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/utils/combn.html'>combn</a></span><span class='o'>(</span><span class='nv'>rolls</span><span class='o'>$</span><span class='nv'>dates</span>, <span class='nv'>n</span>, simplify <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span>
  <span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='nv'>unlist</span><span class='o'>)</span>
  <span class='nv'>lengths_unique</span> <span class='o'>&lt;-</span> <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span><span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span><span class='o'>)</span>
  <span class='c'># Choose combinations that have as many unique values as days</span>
  <span class='nv'>combs</span><span class='o'>[</span><span class='nv'>lengths_unique</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/lengths.html'>lengths</a></span><span class='o'>(</span><span class='nv'>combs</span><span class='o'>)</span><span class='o'>]</span>
<span class='o'>}</span>

<span class='nv'>combs</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/do.call.html'>do.call</a></span><span class='o'>(</span><span class='s'>"c"</span>, <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>2</span>, <span class='kr'>function</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span><span class='nf'>get_combs_from_rolls</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>}</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; [1] "Getting combinations for 1 period(s)."</span>
<span class='c'>#&gt; [1] "Getting combinations for 2 period(s)."</span>
</code></pre>

</div>

Now let's get the values.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>values</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/do.call.html'>do.call</a></span><span class='o'>(</span><span class='s'>"bind_rows"</span>, <span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nv'>combs</span>, <span class='nv'>get_values</span><span class='o'>)</span><span class='o'>)</span>
<span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>[</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>]</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>

<span class='c'>#&gt; [1] 87</span>

<span class='nv'>values</span><span class='o'>$</span><span class='nv'>cost</span><span class='o'>[</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span> <span class='o'>==</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>values</span><span class='o'>$</span><span class='nv'>trips</span><span class='o'>)</span><span class='o'>]</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>

<span class='c'>#&gt; [1] -261</span>
</code></pre>

</div>

So, looks like a top combination of two periods gives me 87 trips at a total cost of \$261. While this number is still lower than the numbers I calculated above (and therefore, less useful to me on my tax return), I am pretty sure that this method could theoretically produce the absolute maximum return based on finding the best combination of periods that have at least 32 trips. I do admit, however, that the difference, if any, from the quick and easy method at the top of this post is probably so minute that it would likely make zero real-world difference on one's tax return. At least I learned a lot about the `apply` family of functions in R!

------------------------------------------------------------------------

*This post was compiled on 2020-10-09 11:17:02. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package     * version    date       lib source                         </span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  downlit       0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  dplyr       * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fs            1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  glue          1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)</span>
<span class='c'>#&gt;  knitr         1.30       2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  lubridate   * 1.7.9      2020-06-08 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  pillar        1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  purrr         0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  Rcpp          1.0.5      2020-07-06 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  readr         1.3.1      2018-12-21 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  rlang         0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  rmarkdown     2.3        2020-06-18 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  stringi       1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  stringr       1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  tibble        3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tidyr       * 1.1.2      2020-08-27 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  vctrs         0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  withr         2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  xfun          0.18       2020-09-29 [2] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  zoo         * 1.8-8      2020-05-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

