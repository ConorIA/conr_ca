---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Heat warning weather at YYZ
slug: toronto-heat-warning-weather-yyz
summary: "An analysis of heat warning weather over the past 30 years at Toronto Pearson"
authors: []
tags:
  - Toronto
  - heat
categories:
  - R
date: 2018-07-22
lastmod: 2018-07-23
featured: false
draft: false
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
rmd_hash: 479ed1cd874e6916
html_dependencies:
- <script src="htmlwidgets-1.5.1/htmlwidgets.js"></script>
- <script src="plotly-binding-4.9.2.1/plotly.js"></script>
- <script src="typedarray-0.1/typedarray.min.js"></script>
- <script src="jquery-1.11.3/jquery.min.js"></script>
- <link href="crosstalk-1.1.0.1/css/crosstalk.css" rel="stylesheet" />
- <script src="crosstalk-1.1.0.1/js/crosstalk.min.js"></script>
- <link href="plotly-htmlwidgets-css-1.52.2/plotly-htmlwidgets.css" rel="stylesheet"
  />
- <script src="plotly-main-1.52.2/plotly-latest.min.js"></script>

---

I have, somewhat by accident, become a bit of a cold weather expert for Toronto. My published research (Anderson and Gough [2017](#ref-Anderson_2017_Evolution)) and my [blog](https://conr.ca/post/2018-01-11_revisiting_december_temps/) [explorations](https://conr.ca/post/2018-01-14_winter_polar_events_in_to/) made me a natural choice to ask questions of when the weather got cold. So, when I got an email this week from UTSC communications to ask about *heat* in the city, I was a little taken aback. Was the sweltering weather that we saw at the start of July something that we can expect more of? I wasn't sure, but as someone who is too frugal to pay for AC, this topic *is* of interest to me! So, I decided to take a look. Read on!

Definitions
-----------

To start this post, we need to set out some definitions. In Toronto, a heat warning is issued when the hot weather could pose a risk to people or pets. The city follows the Environment and Climate Change Canada (ECCC) thresholds. You can find the definitions for ECCC [here](https://web.archive.org/web/20180721155743/https://www.canada.ca/en/environment-climate-change/services/types-weather-forecasts-use/public/criteria-alerts.html#heat), and for Toronto [here](https://web.archive.org/web/20180721155733/https://www.toronto.ca/community-people/health-wellness-care/health-programs-advice/extreme-heat-and-heat-related-illness/harmonized-heat%20warning-and-information-system/). In short, heat warnings are issued by ECCC when two or more consecutive days have projected $T_{max}$ at or above 31°C and $T_{min}$ at or above 20°C, *or*, when the humidex is projected to reach or exceed 40 on two or more consecutive days. Toronto uses two classes of heat warnings: "normal" heat warnings when the conditions are forecast to last two days, or "extended" heat warnings, when the forecast conditions will be around for three or more days.

It is also important to define the humidex, a uniquely Canadian metric. According to the ECCC Meteorological Service of Canada [glossary](https://web.archive.org/web/20180721161336/http://climate.weather.gc.ca/glossary_e.html), the humidex is essentially a measure of how hot the weather "feels" when we factor in the humidity. The following equations summarize the humidex:

$$
\begin{align*}
\mathrm{Humidex} &= T_{air} + h \\\\\\
h &= 0.5555 \times (e - 10.0) \\\\\\
e &= 6.11 \times \mathrm{exp}^{(5417.7530 \times (\frac{1}{273.16} - \frac{1}{T_{dew}} ))} \\\\\\
\mathrm{exp} &= 2.71828
\end{align*}
$$

In the above equations, $T_{dew}$ refers to the dew-point temperature, converted to Kelvin. While the glossary definition used 273.16 K as the conversion to 0°C, I assume that this is a typo, or an error. See this very detailed [StackExchange Answer](https://chemistry.stackexchange.com/questions/29050/what-is-the-significance-of-273-16-k/29053#29053) for some hints about where the number might have come from. I'll use 273.15 K as my 0°C point (but it won't make much of a difference). The $\mathrm{exp}$ in the above equation represents the Euler number, a special constant. The very large number is a rounded constant based on the properties of water and vapour. The humidex is expressed in integers, so we round the above to the nearest whole number.

We can wrap all this up into a nice R function

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>hdx</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>tair</span>, <span class='nv'>tdew</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nv'>tdew</span> <span class='o'>=</span> <span class='nv'>tdew</span> <span class='o'>+</span> <span class='m'>273.15</span>
  <span class='nv'>e</span> <span class='o'>=</span> <span class='m'>6.11</span> <span class='o'>*</span> <span class='nf'><a href='https://rdrr.io/r/base/Log.html'>exp</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>)</span><span class='o'>^</span><span class='o'>(</span><span class='m'>5417.7530</span>  <span class='o'>*</span> <span class='o'>(</span><span class='o'>(</span><span class='m'>1</span> <span class='o'>/</span> <span class='m'>273.16</span><span class='o'>)</span> <span class='o'>-</span> <span class='o'>(</span><span class='m'>1</span> <span class='o'>/</span> <span class='nv'>tdew</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nv'>h</span> <span class='o'>=</span> <span class='m'>0.5555</span> <span class='o'>*</span> <span class='o'>(</span><span class='nv'>e</span> <span class='o'>-</span> <span class='m'>10</span><span class='o'>)</span>
  <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>tair</span> <span class='o'>+</span> <span class='nv'>h</span><span class='o'>)</span>
<span class='o'>}</span>
</code></pre>

</div>

So, for a temperature of 30 °C and a dew-point temperature of 15 °C, we'd have a humidex of 34.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/claut/man/hdx.html'>hdx</a></span><span class='o'>(</span><span class='m'>30</span>, <span class='m'>15</span><span class='o'>)</span>

<span class='c'>#&gt; [1] 34</span>
</code></pre>

</div>

Obviously heat warnings are issued *before* a heat event, but in this case, my analysis is retrospective. As such, I'll look at what I'll call "heat warning *weather*", that is, past conditions that met the forecast conditions and would have merited a heat warning. This is what I'll be referring to from now on. I can't guarantee that heat warning were actually emitted for the events that I detect.

Preparing the analysis
----------------------

I typically work with daily data, but daily data doesn't include humidex values. We'll have to use hourly data instead. First, I'll load the libraries I'll use throughout this post, and then I'll do a search of stations with hourly data for the past \~30 years

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/ConorIA/canadaHCDx/'>canadaHCDx</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/ConorIA/claut'>claut</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://lubridate.tidyverse.org'>lubridate</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://plotly-r.com'>plotly</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://purrr.tidyverse.org'>purrr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tibble.tidyverse.org/'>tibble</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tidyr.tidyverse.org'>tidyr</a></span><span class='o'>)</span>
</code></pre>

</div>

### Finding the station

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/canadaHCDx/man/find_station.html'>find_station</a></span><span class='o'>(</span><span class='s'>"Toronto"</span>, period <span class='o'>=</span> <span class='m'>1988</span><span class='o'>:</span><span class='m'>2018</span>, type <span class='o'>=</span> <span class='s'>"hourly"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 0 x 7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 7 variables: Name </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span style='color: #555555;'>, Province </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span style='color: #555555;'>, StationID </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   LatitudeDD </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, LongitudeDD </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, HourlyFirstYr </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   HourlyLastYr </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

Uhh ohh! There were no stations found that have 30 years of data. This is due to station code changes. I've accounted for this in the **canadaHCDx** search function with a special argument. Let's search again with the "recodes" parameter set to `TRUE`.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/canadaHCDx/man/find_station.html'>find_station</a></span><span class='o'>(</span><span class='s'>"Toronto"</span>, period <span class='o'>=</span> <span class='m'>1988</span><span class='o'>:</span><span class='m'>2018</span>, type <span class='o'>=</span> <span class='s'>"hourly"</span>, recodes <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>

<span class='c'>#&gt; Note: In addition to the stations found, the following combinations may provide sufficient period data.</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; &gt;&gt; Combination 1 at coordinates 43.6 -79.4 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; 5085  : TORONTO ISLAND A    (1957‒2006)</span>
<span class='c'>#&gt; 5086  : TORONTO IS A (AUT)  (1994‒1994)</span>
<span class='c'>#&gt; 30247 : TORONTO CITY CENTRE (2006‒2010)</span>
<span class='c'>#&gt; 48549 : TORONTO CITY CENTRE (2009‒2020)</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; &gt;&gt; Combination 2 at coordinates 43.7 -79.6 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; 5097  : TORONTO LESTER B. PEARSON INT'L A (1953‒2013)</span>
<span class='c'>#&gt; 51459 : TORONTO INTL A                    (2013‒2020)</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; &gt;&gt; Combination 3 at coordinates 43.9 -79.4 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; 4841  : TORONTO BUTTONVILLE A (1986‒2015)</span>
<span class='c'>#&gt; 53678 : TORONTO BUTTONVILLE A (2015‒2019)</span>
<span class='c'>#&gt; 54239 : TORONTO BUTTONVILLE A (2016‒2020)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 0 x 7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 7 variables: Name </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span style='color: #555555;'>, Province </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span style='color: #555555;'>, StationID </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   LatitudeDD </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, LongitudeDD </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, HourlyFirstYr </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   HourlyLastYr </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

It looks like we have three options. I'll use the Pearson airport station for this analysis. First, I should check the years of available data.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/canadaHCDx/man/find_station.html'>find_station</a></span><span class='o'>(</span>name <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"TORONTO LESTER B. PEARSON INT'L A"</span>, <span class='s'>"TORONTO INTL A"</span><span class='o'>)</span>, type <span class='o'>=</span> <span class='s'>"hourly"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>Province</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 2 x 6</span></span>
<span class='c'>#&gt;   Name               StationID LatitudeDD LongitudeDD HourlyFirstYr HourlyLastYr</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>                  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>         </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>        </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> TORONTO INTL A         </span><span style='text-decoration: underline;'>51</span><span>459       43.7       -</span><span style='color: #BB0000;'>79.6</span><span>          </span><span style='text-decoration: underline;'>2</span><span>013         </span><span style='text-decoration: underline;'>2</span><span>020</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> TORONTO LESTER B.…      </span><span style='text-decoration: underline;'>5</span><span>097       43.7       -</span><span style='color: #BB0000;'>79.6</span><span>          </span><span style='text-decoration: underline;'>1</span><span>953         </span><span style='text-decoration: underline;'>2</span><span>013</span></span>
</code></pre>

</div>

It would seem that in 2013, The "Toronto Lester B. Pearson Int'l A" station was renamed to "Toronto Intl A" and recoded. Let's pull in data from these two stations for the summer months: June, July, and August (JJA).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_hourly.html'>hcd_hourly</a></span><span class='o'>(</span>station <span class='o'>=</span> <span class='m'>5097</span>, year <span class='o'>=</span> <span class='m'>1988</span><span class='o'>:</span><span class='m'>2013</span>, month <span class='o'>=</span> <span class='m'>6</span><span class='o'>:</span><span class='m'>8</span><span class='o'>)</span>
<span class='nv'>tpe2</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_hourly.html'>hcd_hourly</a></span><span class='o'>(</span>station <span class='o'>=</span> <span class='m'>51459</span>, year <span class='o'>=</span> <span class='m'>2013</span><span class='o'>:</span><span class='m'>2018</span>, month <span class='o'>=</span> <span class='m'>6</span><span class='o'>:</span><span class='m'>8</span><span class='o'>)</span>
</code></pre>

</div>

Let's see where we can merge these data. An important first step is to set the time zone of these data, which doesn't happen by default in **canadaHCD**. The `DateTime` column of my data is set to UTC time. If I change the timezone, R automatically converts the time to the new timezone, i.e. midnight becomes 8 pm. I don't want this. I could convert the time and then add four hours, but the [`force_tz()`](http://lubridate.tidyverse.org/reference/force_tz.html) function in the **lubridate** package makes short work of this. Note that the ECCC data is always in the local standard time, even when we're observing daylight savings time. (Thanks, Andrew for the reminder!)

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span><span class='o'>$</span><span class='nv'>DateTime</span> <span class='o'>&lt;-</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/force_tz.html'>force_tz</a></span><span class='o'>(</span><span class='nv'>tpe</span><span class='o'>$</span><span class='nv'>DateTime</span>, tzone <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>
<span class='nv'>tpe2</span><span class='o'>$</span><span class='nv'>DateTime</span> <span class='o'>&lt;-</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/force_tz.html'>force_tz</a></span><span class='o'>(</span><span class='nv'>tpe2</span><span class='o'>$</span><span class='nv'>DateTime</span>, tzone <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>
</code></pre>

</div>

Now to see about completeness. I'll search for rows with less than five missing values. My use of five here is arbitrary. There are usually a couple of missing values in a row, such as `WindChill`, which doesn't apply to the summer data, so my threshold needs to be above 2. Any day that is completely missing would have 10 missing values.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/colSums.html'>rowSums</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>tpe</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>&lt;</span> <span class='m'>5</span>,<span class='o'>]</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>DateTime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>tail</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 12</span></span>
<span class='c'>#&gt;   Station DateTime             Temp DewPointTemp RelHumidity WindDir WindSpeed</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span>              </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>        </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>08:00:00</span><span>  15.6         15.2          97       1        17</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>09:00:00</span><span>  15.9         14.8          93       1        24</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>10:00:00</span><span>  17.4         15.3          87      36        22</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>11:00:00</span><span>  17.9         15            83       1        20</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>12:00:00</span><span>  20.9         15.6          72       4        19</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span>    </span><span style='text-decoration: underline;'>5</span><span>097 2013-06-13 </span><span style='color: #555555;'>13:00:00</span><span>  21.8         14.5          63       3        15</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 5 more variables: Visibility </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Pressure </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Humidex </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   WindChill </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>, Weather </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span></span>
</code></pre>

</div>

Station 5097 collected data until June 13, 2013 at 1 pm.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe2</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/colSums.html'>rowSums</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>tpe2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>&lt;</span> <span class='m'>6</span>,<span class='o'>]</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>DateTime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 12</span></span>
<span class='c'>#&gt;   Station DateTime             Temp DewPointTemp RelHumidity WindDir WindSpeed</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span>              </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>        </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>09:00:00</span><span>  17.8         16            89      34        21</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>10:00:00</span><span>  17.4         15.9          91      34        20</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>11:00:00</span><span>  19.5         16.2          81      33        22</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>12:00:00</span><span>  19.6         15.3          76      32        28</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>13:00:00</span><span>  21           14.7          67      33        23</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2013-06-11 </span><span style='color: #555555;'>14:00:00</span><span>  21.4         14.9          66      33        25</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 5 more variables: Visibility </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Pressure </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Humidex </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   WindChill </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>, Weather </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span></span>
</code></pre>

</div>

Station 51459 collected data starting on June 11, 2013 at 9:00 am. Since we have overlapping data, we can have a quick look to see how the stations compare. I'll look at the `Temp` column for this comparison.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>val</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>datetime <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span>from <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-11 09:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>,
                             to <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-13 13:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>,
                             by <span class='o'>=</span> <span class='m'>3600</span><span class='o'>)</span>,
             tpe <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe</span>, <span class='nv'>DateTime</span> <span class='o'>%in%</span> <span class='nv'>datetime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Temp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span>,
             tpe2 <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe2</span>, <span class='nv'>DateTime</span> <span class='o'>%in%</span> <span class='nv'>datetime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Temp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>
<span class='nv'>val</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/gather.html'>gather</a></span><span class='o'>(</span>key <span class='o'>=</span> <span class='s'>"station"</span>, value <span class='o'>=</span> <span class='s'>"Temp"</span>, <span class='o'>-</span><span class='nv'>datetime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/plot_ly.html'>plot_ly</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='o'>~</span><span class='nv'>datetime</span>, y <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Temp</span>, color <span class='o'>=</span> <span class='o'>~</span><span class='nv'>station</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/add_trace.html'>add_markers</a></span><span class='o'>(</span><span class='o'>)</span>

<!--html_preserve--><div id="htmlwidget-13af1d57e29a03d58daf" style="width:700px;height:415.296px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-13af1d57e29a03d58daf">{"x":{"visdat":{"20da374c4e6f":["function () ","plotlyVisDat"]},"cur_data":"20da374c4e6f","attrs":{"20da374c4e6f":{"x":{},"y":{},"color":{},"alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","mode":"markers","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"xaxis":{"domain":[0,1],"automargin":true,"title":"datetime"},"yaxis":{"domain":[0,1],"automargin":true,"title":"Temp"},"hovermode":"closest","showlegend":true},"source":"A","config":{"showSendToCloud":false},"data":[{"x":["2013-06-11 09:00:00","2013-06-11 10:00:00","2013-06-11 11:00:00","2013-06-11 12:00:00","2013-06-11 13:00:00","2013-06-11 14:00:00","2013-06-11 15:00:00","2013-06-11 16:00:00","2013-06-11 17:00:00","2013-06-11 18:00:00","2013-06-11 19:00:00","2013-06-11 20:00:00","2013-06-11 21:00:00","2013-06-11 22:00:00","2013-06-11 23:00:00","2013-06-12 00:00:00","2013-06-12 01:00:00","2013-06-12 02:00:00","2013-06-12 03:00:00","2013-06-12 04:00:00","2013-06-12 05:00:00","2013-06-12 06:00:00","2013-06-12 07:00:00","2013-06-12 08:00:00","2013-06-12 09:00:00","2013-06-12 10:00:00","2013-06-12 11:00:00","2013-06-12 12:00:00","2013-06-12 13:00:00","2013-06-12 14:00:00","2013-06-12 15:00:00","2013-06-12 16:00:00","2013-06-12 17:00:00","2013-06-12 18:00:00","2013-06-12 19:00:00","2013-06-12 20:00:00","2013-06-12 21:00:00","2013-06-12 22:00:00","2013-06-12 23:00:00","2013-06-13 00:00:00","2013-06-13 01:00:00","2013-06-13 02:00:00","2013-06-13 03:00:00","2013-06-13 04:00:00","2013-06-13 05:00:00","2013-06-13 06:00:00","2013-06-13 07:00:00","2013-06-13 08:00:00","2013-06-13 09:00:00","2013-06-13 10:00:00","2013-06-13 11:00:00","2013-06-13 12:00:00","2013-06-13 13:00:00"],"y":[17.9,17.4,19.4,19.5,20.9,21.7,23,22.4,22,21.5,20.6,19.8,18.4,17.8,17.2,17.1,16.6,15.9,15.3,14.9,14.5,16.6,18.1,19.8,20.7,19.5,19.8,20.2,21.9,22.4,21.9,21.2,20.6,18.7,16.7,16.7,17.2,17.6,17.6,17.4,17.3,16.3,16.5,16.5,16,15.7,15.2,15.6,15.9,17.4,17.9,20.9,21.8],"type":"scatter","mode":"markers","name":"tpe","marker":{"color":"rgba(102,194,165,1)","line":{"color":"rgba(102,194,165,1)"}},"textfont":{"color":"rgba(102,194,165,1)"},"error_y":{"color":"rgba(102,194,165,1)"},"error_x":{"color":"rgba(102,194,165,1)"},"line":{"color":"rgba(102,194,165,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":["2013-06-11 09:00:00","2013-06-11 10:00:00","2013-06-11 11:00:00","2013-06-11 12:00:00","2013-06-11 13:00:00","2013-06-11 14:00:00","2013-06-11 15:00:00","2013-06-11 16:00:00","2013-06-11 17:00:00","2013-06-11 18:00:00","2013-06-11 19:00:00","2013-06-11 20:00:00","2013-06-11 21:00:00","2013-06-11 22:00:00","2013-06-11 23:00:00","2013-06-12 00:00:00","2013-06-12 01:00:00","2013-06-12 02:00:00","2013-06-12 03:00:00","2013-06-12 04:00:00","2013-06-12 05:00:00","2013-06-12 06:00:00","2013-06-12 07:00:00","2013-06-12 08:00:00","2013-06-12 09:00:00","2013-06-12 10:00:00","2013-06-12 11:00:00","2013-06-12 12:00:00","2013-06-12 13:00:00","2013-06-12 14:00:00","2013-06-12 15:00:00","2013-06-12 16:00:00","2013-06-12 17:00:00","2013-06-12 18:00:00","2013-06-12 19:00:00","2013-06-12 20:00:00","2013-06-12 21:00:00","2013-06-12 22:00:00","2013-06-12 23:00:00","2013-06-13 00:00:00","2013-06-13 01:00:00","2013-06-13 02:00:00","2013-06-13 03:00:00","2013-06-13 04:00:00","2013-06-13 05:00:00","2013-06-13 06:00:00","2013-06-13 07:00:00","2013-06-13 08:00:00","2013-06-13 09:00:00","2013-06-13 10:00:00","2013-06-13 11:00:00","2013-06-13 12:00:00","2013-06-13 13:00:00"],"y":[17.8,17.4,19.5,19.6,21,21.4,22.8,23,22.2,21.5,20.8,19.6,18.5,17.8,17,17.1,16.6,15.9,15.1,14.8,14.3,16.4,18.2,19.9,21.1,19.4,20.1,20.4,21.2,21.8,22,21.2,20.5,18.7,16.5,16.4,17,17.1,17.3,17,16.9,15.9,16.2,16.1,15.5,15.5,15.1,15.4,15.7,17.2,17.9,20.6,21.9],"type":"scatter","mode":"markers","name":"tpe2","marker":{"color":"rgba(141,160,203,1)","line":{"color":"rgba(141,160,203,1)"}},"textfont":{"color":"rgba(141,160,203,1)"},"error_y":{"color":"rgba(141,160,203,1)"},"error_x":{"color":"rgba(141,160,203,1)"},"line":{"color":"rgba(141,160,203,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"shinyEvents":["plotly_hover","plotly_click","plotly_selected","plotly_relayout","plotly_brushed","plotly_brushing","plotly_clickannotation","plotly_doubleclick","plotly_deselect","plotly_afterplot","plotly_sunburstclick"],"base_url":"https://plot.ly"},"evals":[],"jsHooks":[]}</script><!--/html_preserve--></code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/summary.html'>summary</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/MathFun.html'>abs</a></span><span class='o'>(</span><span class='nv'>val</span><span class='o'>$</span><span class='nv'>tpe</span> <span class='o'>-</span> <span class='nv'>val</span><span class='o'>$</span><span class='nv'>tpe2</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt;    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. </span>
<span class='c'>#&gt;   0.000   0.100   0.200   0.208   0.300   0.700</span>
</code></pre>

</div>

There seems to be a very minor cold bias at station 51459 overnight between June 12 and 13, but not enough to perform any significant bias correction. There is an average difference between points of just over two tenths of a degree Celsius.

What about the dew-point temperature?

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>val</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>datetime <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span>from <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-11 09:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>,
                             to <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-13 13:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span>,
                             by <span class='o'>=</span> <span class='m'>3600</span><span class='o'>)</span>,
             tpe <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe</span>, <span class='nv'>DateTime</span> <span class='o'>%in%</span> <span class='nv'>datetime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>DewPointTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span>,
             tpe2 <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe2</span>, <span class='nv'>DateTime</span> <span class='o'>%in%</span> <span class='nv'>datetime</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>DewPointTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/summary.html'>summary</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/MathFun.html'>abs</a></span><span class='o'>(</span><span class='nv'>val</span><span class='o'>$</span><span class='nv'>tpe</span> <span class='o'>-</span> <span class='nv'>val</span><span class='o'>$</span><span class='nv'>tpe2</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt;    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. </span>
<span class='c'>#&gt;    0.10    0.80    0.90    1.04    1.20    3.10</span>
</code></pre>

</div>

There is actually a larger deviation between the `DewPointTemp` variable at the two stations, with differences of up to 3.1°C and an average of 1.0°C. This could influence the humidex results by a few points, but it is hard to predict, since the metric depends on the magnitude of the dew point temperature. I will leave the data as is. After all, I don't want to end up fudging the numbers the wrong way.

For convenience, I'll join the two station data sets between June 10 and 11.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe</span>, <span class='nv'>DateTime</span> <span class='o'>&lt;</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-12 00:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span><span class='o'>)</span>,
                 <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tpe2</span>, <span class='nv'>DateTime</span> <span class='o'>&gt;=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2013-06-12 00:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>DateTime</span><span class='o'>)</span>
</code></pre>

</div>

As of the time of writing, I have fairly complete data up to July 20, 2018.

### Calculating the humidex

Now for our humidex values.

Environment Canada seems to have calculated the humidex ... *sometimes*. I can ensure that my equation is correct by performing a quick validation on their values.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>val</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter_all.html'>filter_at</a></span><span class='o'>(</span><span class='nv'>tpe</span>, <span class='nf'><a href='https://dplyr.tidyverse.org/reference/vars.html'>vars</a></span><span class='o'>(</span><span class='nv'>Temp</span>, <span class='nv'>DewPointTemp</span>, <span class='nv'>Humidex</span><span class='o'>)</span>, <span class='nf'><a href='https://dplyr.tidyverse.org/reference/all_vars.html'>all_vars</a></span><span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>hdx <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/claut/man/hdx.html'>hdx</a></span><span class='o'>(</span><span class='nv'>Temp</span>, <span class='nv'>DewPointTemp</span><span class='o'>)</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/MathFun.html'>abs</a></span><span class='o'>(</span><span class='nv'>val</span><span class='o'>$</span><span class='nv'>Humidex</span> <span class='o'>-</span> <span class='nv'>val</span><span class='o'>$</span><span class='nv'>hdx</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; [1] 0</span>
</code></pre>

</div>

Good, it seems that there is no difference between my calculations and theirs. Let's fill in the missing values.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span><span class='nv'>tpe</span>, Humidex <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/claut/man/hdx.html'>hdx</a></span><span class='o'>(</span><span class='nv'>Temp</span>, <span class='nv'>DewPointTemp</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

### Aggregating to daily

To generate daily data, Environment Canada uses 6 am UTC as the "start" of a new meteorological day. This is standard across the country. For this analysis, that means that the overnight temperatures after midnight but before 1 am tomorrow morning will still officially be "tonight". This method isn't necessarily optimal. We can take a look at an examples.

Let's first take a look at the late evening on June 28 and the early morning hours of June 29, 2018.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>DateTime</span> <span class='o'>&gt;=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2018-06-28 22:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span> <span class='o'>&amp;</span>
                 <span class='nv'>DateTime</span> <span class='o'>&lt;=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2018-06-29 08:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 11 x 12</span></span>
<span class='c'>#&gt;    Station DateTime             Temp DewPointTemp RelHumidity WindDir WindSpeed</span>
<span class='c'>#&gt;      <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span>              </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>        </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-28 </span><span style='color: #555555;'>22:00:00</span><span>  22.9         16.7          68      28        16</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-28 </span><span style='color: #555555;'>23:00:00</span><span>  22.6         17.2          71      27        16</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>00:00:00</span><span>  21.2         17.2          78      27        14</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>01:00:00</span><span>  19.9         16.8          82      25        10</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>02:00:00</span><span>  19.9         17.3          85      29        11</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>03:00:00</span><span>  20.1         17.8          86      29        14</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>04:00:00</span><span>  20.3         18            86      30        17</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>05:00:00</span><span>  19.4         16.5          83      36         2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>06:00:00</span><span>  21.1         17.2          78      30         8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>07:00:00</span><span>  23.5         17.5          69      25         5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>11</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>08:00:00</span><span>  25.9         16.8          57      27         9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 5 more variables: Visibility </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Pressure </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Humidex </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   WindChill </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>, Weather </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span></span>
</code></pre>

</div>

As we can see, above, the temperatures overnight between June 28 and June 29 fell to as low as 19.4°C at 5 am. That means that we didn't see weather that merited a heat alert.

Now on the other side, late evening on June 29 and the early morning on June 30, 2018.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>DateTime</span> <span class='o'>&gt;=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2018-06-29 22:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span> <span class='o'>&amp;</span>
                 <span class='nv'>DateTime</span> <span class='o'>&lt;=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.POSIXlt.html'>as.POSIXct</a></span><span class='o'>(</span><span class='s'>"2018-06-30 08:00:00"</span>, tz <span class='o'>=</span> <span class='s'>"Etc/GMT-5"</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 11 x 12</span></span>
<span class='c'>#&gt;    Station DateTime             Temp DewPointTemp RelHumidity WindDir WindSpeed</span>
<span class='c'>#&gt;      <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span>              </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>        </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>22:00:00</span><span>  26.1         18.1          60      21        12</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-29 </span><span style='color: #555555;'>23:00:00</span><span>  25.4         18.4          65      19        10</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>00:00:00</span><span>  24.5         18.6          69      19         7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>01:00:00</span><span>  24           18.2          70      22        17</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>02:00:00</span><span>  23.7         18.1          70      20        15</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>03:00:00</span><span>  22.4         17.8          75      25        11</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>04:00:00</span><span>  22.2         18.5          79      24         8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>05:00:00</span><span>  23.2         18.8          76      18         8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>06:00:00</span><span>  23.6         19.3          77      22        13</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>07:00:00</span><span>  24.1         19.6          76      13         9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>11</span><span>   </span><span style='text-decoration: underline;'>51</span><span>459 2018-06-30 </span><span style='color: #555555;'>08:00:00</span><span>  25.5         20.8          75      19        10</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 5 more variables: Visibility </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Pressure </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, Humidex </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   WindChill </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span style='color: #555555;'>, Weather </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span></span>
</code></pre>

</div>

Temperatures overnight from June 29 into June 30 did not fall below 22.2°C. Thus, given that it got as hot as 31°C on June 29, and the temperature didn't fall below 20°C, that day should be classed as heat warning weather. If we choose 1 am as the cutoff time, however, the cooler period overnight from June 28 to June 29 means that June 29 did not meet the heat warning criteria.

When a heat warning is issued, it is issued for *forecast* temperatures. ECCC doesn't say, "It is going to be hot tonight but since it was cool at 1 am last night, you have nothing to worry about". We, likewise, live chronologically. What do I care that it was cool in the wee hours of the previous night if I am suffering *now*? This is where the so-called "climatological observing window" (COW) becomes important. See this great article on the subject by Žaknić-Ćatović and Gough ([2018](#ref-zaknic-catovic_2018_comparison)).

Given that I am trying to detect days when the temperatures got hot, and didn't drop below 20°C *overnight*, I'm going to use the sun-based COW<sub>ND</sub> proposed by Žaknić-Ćatović and Gough ([2018](#ref-zaknic-catovic_2018_comparison)). I wrote a function for this in the **claut** package (See: [`solar_daily()`](https://gitlab.com/ConorIA/claut/blob/master/R/solar_daily.R)). COW<sub>ND</sub> defines each climatological day as one hour past sunrise until one hour past sunrise the following calendar day. This way, we can avoid double-counting late-evening cooling on both sides of 1 am, and thus, avoid having a day shaved off a heat event due to temperatures from the night before. I'll create a data set of the maximum daily temperature ($T_{max}$), minimum daily temperature ($T_{min}$), average daily temperature ($T_{mean}$), and the peak daily humidex value. I'll also add a Boolean column to flag days that met the criteria for a heat warning.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_dly</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/claut/man/solar_daily.html'>solar_daily</a></span><span class='o'>(</span>dat <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>tpe</span>, <span class='nv'>DateTime</span>, <span class='nv'>Temp</span>, <span class='nv'>Humidex</span><span class='o'>)</span>, lat <span class='o'>=</span> <span class='m'>43.68</span>, lon <span class='o'>=</span> <span class='o'>-</span><span class='m'>79.63</span>, tz <span class='o'>=</span> <span class='o'>-</span><span class='m'>5</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>MinHumidex</span>, <span class='o'>-</span><span class='nv'>MeanHumidex</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>MeanTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span>, <span class='m'>1</span><span class='o'>)</span>,
         Criteria <span class='o'>=</span> <span class='o'>(</span><span class='nv'>MaxTemp</span> <span class='o'>&gt;=</span> <span class='m'>31</span> <span class='o'>&amp;</span> <span class='nv'>MinTemp</span> <span class='o'>&gt;=</span> <span class='m'>20</span><span class='o'>)</span> <span class='o'>|</span> <span class='nv'>MaxHumidex</span> <span class='o'>&gt;=</span> <span class='m'>40</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Date</span> <span class='o'>&gt;=</span> <span class='s'>"1988-06-01"</span> <span class='o'>&amp;</span> <span class='nv'>Date</span> <span class='o'>&lt;=</span> <span class='s'>"2018-07-19"</span><span class='o'>)</span>
</code></pre>

</div>

Let's take a look at the weather since late June, for instance.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_dly</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Date</span> <span class='o'>&gt;=</span> <span class='s'>"2018-06-28"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 22 x 6</span></span>
<span class='c'>#&gt;    Date       MaxHumidex MaxTemp MeanTemp MinTemp Criteria</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>          </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span>   </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span> 2018-06-28         34    29.4     24      19.4 FALSE   </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span> 2018-06-29         37    31       27      22.2 TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span> 2018-06-30         46    34.8     28.9    24   TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span> 2018-07-01         43    34.1     29.4    24.5 TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span> 2018-07-02         43    33.1     27      19.3 TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span> 2018-07-03         34    30.2     25.7    19.7 FALSE   </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span> 2018-07-04         39    32.7     27.1    23.4 TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span> 2018-07-05         43    33.2     25.8    17.3 TRUE    </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span> 2018-07-06         24    22.8     </span><span style='color: #BB0000;'>NA</span><span>      </span><span style='color: #BB0000;'>NA</span><span>   FALSE   </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span> 2018-07-07         26    25.3     21.4    15.8 FALSE   </span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 12 more rows</span></span>
</code></pre>

</div>

Trends in these variables
-------------------------

To begin, let's take a look at how these four variables have changed in the last 30 years. I'll use a simple linear regression here, to test the relationship between the aggregate annual metrics and year. Normally there are a few assumptions to check, but for a quick analysis, I'll omit these.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>annual_stats</span> <span class='o'>&lt;-</span> <span class='nv'>tpe_dly</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Year <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/year.html'>year</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>MaxTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MinTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MinTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MeanTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MaxHumidex <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MaxHumidex</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            HeatDays <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>Criteria</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>


<span class='nv'>annual_stats</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>`HeatDays`</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/gather.html'>gather</a></span><span class='o'>(</span>key <span class='o'>=</span> <span class='nv'>Var</span>, value <span class='o'>=</span> <span class='nv'>value</span>, <span class='o'>-</span><span class='nv'>Year</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/plot_ly.html'>plot_ly</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Year</span>, y <span class='o'>=</span> <span class='o'>~</span><span class='nv'>value</span>, color <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Var</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/add_trace.html'>add_lines</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/layout.html'>layout</a></span><span class='o'>(</span>yaxis <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Temperature (°C) / Humidex"</span><span class='o'>)</span>,
         title <span class='o'>=</span> <span class='s'>"Summer temperature and humidex in Toronto, 1988-2018 (YTD)"</span><span class='o'>)</span>

<!--html_preserve--><div id="htmlwidget-58eed3ec0234c0a62d0f" style="width:700px;height:415.296px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-58eed3ec0234c0a62d0f">{"x":{"visdat":{"20da26d45a02":["function () ","plotlyVisDat"]},"cur_data":"20da26d45a02","attrs":{"20da26d45a02":{"x":{},"y":{},"color":{},"alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","mode":"lines","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"yaxis":{"domain":[0,1],"automargin":true,"title":"Temperature (°C) / Humidex"},"title":"Summer temperature and humidex in Toronto, 1988-2018 (YTD)","xaxis":{"domain":[0,1],"automargin":true,"title":"Year"},"hovermode":"closest","showlegend":true},"source":"A","config":{"showSendToCloud":false},"data":[{"x":[1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018],"y":[29.9347826086957,28.8913043478261,29.1086956521739,29.9130434782609,25.3152173913043,29.2282608695652,28.9450549450549,31,29.0120481927711,28.3956043956044,29.8021978021978,30.4111111111111,28.1758241758242,29.4891304347826,31.3695652173913,29.6304347826087,27.695652173913,33.0108695652174,30.6086956521739,30.4565217391304,28.9130434782609,27.4130434782609,31.8260869565217,31.0217391304348,31.3695652173913,29.4065934065934,28.445652173913,28.4444444444444,31.2717391304348,28.7173913043478,29.7551020408163],"type":"scatter","mode":"lines","name":"MaxHumidex","marker":{"color":"rgba(102,194,165,1)","line":{"color":"rgba(102,194,165,1)"}},"textfont":{"color":"rgba(102,194,165,1)"},"error_y":{"color":"rgba(102,194,165,1)"},"error_x":{"color":"rgba(102,194,165,1)"},"line":{"color":"rgba(102,194,165,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":[1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018],"y":[26.7510869565217,24.9619565217391,25.120652173913,26.1271739130435,22.2728260869565,25.104347826087,24.8373626373626,26.2067415730337,24.555421686747,24.9714285714286,25.721978021978,26.3355555555556,24.1593406593407,25.9978260869565,26.9684782608696,25.3554347826087,23.6130434782609,27.8608695652174,25.9597826086957,26.5739130434783,24.6836956521739,23.4282608695652,26.0630434782609,26.5597826086957,27,25.0054347826087,24.6695652173913,24.6244444444444,27.8717391304348,24.879347826087,25.9714285714286],"type":"scatter","mode":"lines","name":"MaxTemp","marker":{"color":"rgba(252,141,98,1)","line":{"color":"rgba(252,141,98,1)"}},"textfont":{"color":"rgba(252,141,98,1)"},"error_y":{"color":"rgba(252,141,98,1)"},"error_x":{"color":"rgba(252,141,98,1)"},"line":{"color":"rgba(252,141,98,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":[1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018],"y":[21.0021739130435,20.1304347826087,20.1326086956522,21.1184782608696,17.6086956521739,20.1858695652174,19.8692307692308,21.3556818181818,20.0962025316456,20.1921348314607,20.8307692307692,21.7255555555556,19.6505494505495,21.1945054945055,22.0467391304348,20.7565217391304,19.3782608695652,23.0760869565217,21.5054347826087,21.5239130434783,20.3673913043478,19.2597826086957,21.7945652173913,21.954347826087,22.2880434782609,20.7032608695652,20.1695652173913,20.3966292134831,22.7673913043478,20.4252747252747,21.5770833333333],"type":"scatter","mode":"lines","name":"MeanTemp","marker":{"color":"rgba(141,160,203,1)","line":{"color":"rgba(141,160,203,1)"}},"textfont":{"color":"rgba(141,160,203,1)"},"error_y":{"color":"rgba(141,160,203,1)"},"error_x":{"color":"rgba(141,160,203,1)"},"line":{"color":"rgba(141,160,203,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":[1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018],"y":[15.2869565217391,15.2760869565217,15.3163043478261,15.8923913043478,12.8869565217391,15.2402173913043,14.9912087912088,16.7211111111111,15.4863636363636,15.2111111111111,15.829347826087,17.2880434782609,15.554347826087,16.8131868131868,17.4695652173913,16.4108695652174,15.445652173913,18.6554347826087,17.2586956521739,16.7641304347826,16.2717391304348,15.1663043478261,17.6445652173913,17.5521739130435,17.7347826086957,16.7413043478261,16.0391304347826,16.0555555555556,17.745652173913,16.3725274725275,17.0333333333333],"type":"scatter","mode":"lines","name":"MinTemp","marker":{"color":"rgba(231,138,195,1)","line":{"color":"rgba(231,138,195,1)"}},"textfont":{"color":"rgba(231,138,195,1)"},"error_y":{"color":"rgba(231,138,195,1)"},"error_x":{"color":"rgba(231,138,195,1)"},"line":{"color":"rgba(231,138,195,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"shinyEvents":["plotly_hover","plotly_click","plotly_selected","plotly_relayout","plotly_brushed","plotly_brushing","plotly_clickannotation","plotly_doubleclick","plotly_deselect","plotly_afterplot","plotly_sunburstclick"],"base_url":"https://plot.ly"},"evals":[],"jsHooks":[]}</script><!--/html_preserve--></code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nv'>annual_stats</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Year</span> <span class='o'>!=</span> <span class='m'>2018</span><span class='o'>)</span>
<span class='nv'>splits</span> <span class='o'>&lt;-</span> <span class='nv'>dat</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/gather.html'>gather</a></span><span class='o'>(</span>key <span class='o'>=</span> <span class='nv'>var</span>, value <span class='o'>=</span> <span class='nv'>value</span>, <span class='o'>-</span><span class='nv'>Year</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/base/split.html'>split</a></span><span class='o'>(</span><span class='nv'>.</span>, <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>var</span><span class='o'>)</span><span class='o'>)</span>

<span class='nv'>splits</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://purrr.tidyverse.org/reference/map.html'>map_df</a></span><span class='o'>(</span><span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='nv'>summ</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/summary.html'>summary</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/stats/lm.html'>lm</a></span><span class='o'>(</span><span class='nv'>value</span><span class='o'>~</span><span class='nv'>Year</span>, data <span class='o'>=</span> <span class='nv'>x</span><span class='o'>)</span><span class='o'>)</span>
    <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>slope <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/coef.html'>coef</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>)</span><span class='o'>[</span><span class='m'>2</span><span class='o'>]</span>, rsq <span class='o'>=</span> <span class='nv'>summ</span><span class='o'>$</span><span class='nv'>r.squared</span>, p <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/coef.html'>coef</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>)</span><span class='o'>[</span><span class='m'>8</span><span class='o'>]</span><span class='o'>)</span>
    <span class='o'>}</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>name <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>splits</span><span class='o'>)</span>, .before <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 4</span></span>
<span class='c'>#&gt;   name        slope     rsq       p</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> HeatDays   0.054</span><span style='text-decoration: underline;'>9</span><span> 0.007</span><span style='text-decoration: underline;'>26</span><span> 0.654  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> MaxHumidex 0.036</span><span style='text-decoration: underline;'>1</span><span> 0.042</span><span style='text-decoration: underline;'>9</span><span>  0.272  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> MaxTemp    0.021</span><span style='text-decoration: underline;'>3</span><span> 0.021</span><span style='text-decoration: underline;'>8</span><span>  0.436  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> MeanTemp   0.042</span><span style='text-decoration: underline;'>8</span><span> 0.110   0.073</span><span style='text-decoration: underline;'>4</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> MinTemp    0.073</span><span style='text-decoration: underline;'>8</span><span> 0.310   0.001</span><span style='text-decoration: underline;'>41</span></span>
</code></pre>

</div>

The linear regression results do not indicate that daytime high temperatures are increasing, nor are we seeing more days, year-over-year that meet the criteria for a heat warning. I did, however, detect a significant (at the 0.01 level) warming trend in the summer evening temperatures, and a respective increase (significant at the 0.1 level) in the mean temperature, which is probably capturing the signal in $T_{min}$. This is an interesting find, and is consistent with the changes in climate that are associated with urbanization. Which by the way, I *just* published on! See Anderson, Gough, and Mohsin ([2018](#ref-anderson_2018_characterization)). This is consistent with the assertions made by Mohsin and Gough ([2012](#ref-mohsin_2012_characterization)), however I recall having heard that it was the 1970s that saw the fastest urbanization near Pearson. Certainly, over the last 20 years or so the airport has remained largely "developed". There may be a little more to this.

Analyzing heat events
---------------------

I established a methodology of analyzing short, non-uniform periods when I looked at cold snaps back in January. My methodology hasn't changed much since the winter, but I have wrapped it up in a cleaner function and shipped it in the **claut** package. See: [`describe_snaps()`](https://gitlab.com/ConorIA/claut/blob/master/R/describe_snaps.R) for the code. So, let's take a look at some summertime heat warnings.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns</span> <span class='o'>&lt;-</span> <span class='nv'>tpe_dly</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Year <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/year.html'>year</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Start <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span>, <span class='nv'>Date</span>, <span class='nv'>min</span><span class='o'>)</span>, origin <span class='o'>=</span> <span class='nv'>origin</span><span class='o'>)</span>,
         End <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span>, <span class='nv'>Date</span>, <span class='nv'>max</span><span class='o'>)</span>, origin <span class='o'>=</span> <span class='nv'>origin</span><span class='o'>)</span>,
         Length <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Year</span>, <span class='nv'>Start</span>, <span class='nv'>End</span>, <span class='nv'>Length</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>mean</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>Criteria</span>, <span class='o'>-</span><span class='nv'>Date</span><span class='o'>)</span>

<span class='nv'>tpe_warns</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 68 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [68]</span></span>
<span class='c'>#&gt;     Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988 1988-06-19 1988-06-20      2       37      32.7     25.5    18.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988 1988-07-07 1988-08-19      4       39.8    36.0     28.8    21.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988 1988-08-02 1988-08-05      4       42.5    32.5     27.6    23.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988 1988-08-12 1988-08-14      3       42      33.2     27.7    23.3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>  </span><span style='text-decoration: underline;'>1</span><span>991 1991-06-27 1991-06-28      2       38.5    32.8     27.6    22.0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>  </span><span style='text-decoration: underline;'>1</span><span>991 1991-07-17 1991-07-20      4       39.2    32.9     27.2    21.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>  </span><span style='text-decoration: underline;'>1</span><span>993 1993-07-08 1993-07-09      2       41      31.8     26.2    21.2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>  </span><span style='text-decoration: underline;'>1</span><span>993 1993-08-26 1993-08-27      2       40      32.5     26.4    20.8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>  </span><span style='text-decoration: underline;'>1</span><span>994 1994-06-16 1994-06-18      3       41      33.5     27.5    21  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>  </span><span style='text-decoration: underline;'>1</span><span>994 1994-07-05 1994-08-11      2       41      31.5     25.5    20.8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 58 more rows</span></span>
</code></pre>

</div>

Since June 1988, there have been 68 weather events that merited a heat warning in Toronto. Of those, 27 should have been "extended warnings". That leaves 41 events that brought exactly two-days of extra hot weather. The average length of heat wearning weather is 2.63 days. In the plot below, I show these heat warnings. The y-axis represents the average daily maximum humidex. The colour represents the average daily maximum temperature. The size of the points represents the length of the heat warning weather.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/plotly/man/plot_ly.html'>plot_ly</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>tpe_warns</span>, x <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Start</span>, y <span class='o'>=</span> <span class='o'>~</span><span class='nv'>MaxHumidex</span>, color <span class='o'>=</span> <span class='o'>~</span><span class='nv'>MaxTemp</span>, size <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Length</span>,
        hoverinfo <span class='o'>=</span> <span class='s'>"text"</span>, hovertext <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='s'>"Length:"</span>, <span class='nv'>tpe_warns</span><span class='o'>$</span><span class='nv'>Length</span>,
                                              <span class='s'>"days"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/add_trace.html'>add_markers</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/layout.html'>layout</a></span><span class='o'>(</span>
    title <span class='o'>=</span> <span class='s'>"Summertime heat warning weather in Toronto, 1988-2018 (YTD)"</span>,
    yaxis <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Average Daily Maximum Humidex"</span><span class='o'>)</span>,
    xaxis <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Start Date"</span>,
                      range <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>min</a></span><span class='o'>(</span><span class='nv'>tpe_warns</span><span class='o'>$</span><span class='nv'>Start</span> <span class='o'>-</span> <span class='m'>1000</span><span class='o'>)</span>, <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>tpe_warns</span><span class='o'>$</span><span class='nv'>Start</span><span class='o'>)</span> <span class='o'>+</span> <span class='m'>1000</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; Warning: `line.width` does not currently support multiple values.</span>

<!--html_preserve--><div id="htmlwidget-eb667433be956f0ca4cf" style="width:700px;height:415.296px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-eb667433be956f0ca4cf">{"x":{"visdat":{"20da35a19a23":["function () ","plotlyVisDat"]},"cur_data":"20da35a19a23","attrs":{"20da35a19a23":{"x":{},"y":{},"hoverinfo":"text","hovertext":["Length: 2 days","Length: 4 days","Length: 4 days","Length: 3 days","Length: 2 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 4 days","Length: 2 days"],"color":{},"size":{},"alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","mode":"markers","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"Summertime heat warning weather in Toronto, 1988-2018 (YTD)","yaxis":{"domain":[0,1],"automargin":true,"title":"Average Daily Maximum Humidex"},"xaxis":{"domain":[0,1],"automargin":true,"title":"Start Date","range":["1985-09-23","2021-03-30"]},"hovermode":"closest","showlegend":false,"legend":{"yanchor":"top","y":0.5}},"source":"A","config":{"showSendToCloud":false},"data":[{"x":["1988-06-19","1988-07-07","1988-08-02","1988-08-12","1991-06-27","1991-07-17","1993-07-08","1993-08-26","1994-06-16","1994-07-05","1995-06-17","1995-07-13","1995-07-31","1997-07-13","1998-07-14","1998-07-20","1998-08-23","1999-06-06","1999-07-04","1999-07-15","1999-07-22","2001-06-29","2001-07-23","2001-08-05","2002-06-30","2002-07-07","2002-07-16","2002-07-28","2002-07-31","2002-08-11","2003-06-24","2003-07-03","2003-08-14","2004-06-08","2005-06-24","2005-06-27","2005-07-10","2005-08-03","2006-06-17","2006-07-15","2006-07-31","2007-06-25","2007-07-09","2007-07-31","2008-06-08","2009-08-09","2010-07-04","2010-08-03","2010-08-30","2011-07-10","2011-07-17","2011-07-20","2011-07-31","2012-06-19","2012-07-15","2013-06-23","2013-07-16","2015-07-18","2015-07-28","2016-06-19","2016-07-06","2016-07-21","2016-08-04","2016-08-09","2017-06-11","2018-06-17","2018-06-29","2018-07-04"],"y":[37,39.75,42.5,42,38.5,39.25,41,40,41,41,38,44.3333333333333,38,39,38.5,37.5,39,41.5,43.3333333333333,39.3333333333333,36.8,37,40,41,41.75,34.5,39,43.5,40,39,39.6666666666667,37.6666666666667,38,39,43,39,39.2,40.5,39.5,39.6666666666667,45.6666666666667,39.3333333333333,42.5,39,39.5,40.5,41.6,41.5,40.5,38.5,41.5,44.5,38,41,40,40,42.75,40,37,38,37,38,38.5,39.6,38.5,40.5,42.25,41],"hoverinfo":["text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text"],"hovertext":["Length: 2 days","Length: 4 days","Length: 4 days","Length: 3 days","Length: 2 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 3 days","Length: 2 days","Length: 4 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 2 days","Length: 3 days","Length: 2 days","Length: 5 days","Length: 2 days","Length: 2 days","Length: 4 days","Length: 2 days"],"type":"scatter","mode":"markers","marker":{"colorbar":{"title":"MaxTemp","ticklen":2},"cmin":29.4,"cmax":36.4,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666668","rgba(70,19,97,1)"],["0.0833333333333332","rgba(72,32,111,1)"],["0.125","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666667","rgba(56,93,140,1)"],["0.333333333333333","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666666","rgba(42,123,142,1)"],["0.458333333333334","rgba(38,133,141,1)"],["0.5","rgba(37,144,140,1)"],["0.541666666666666","rgba(33,154,138,1)"],["0.583333333333334","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333334","rgba(79,191,110,1)"],["0.75","rgba(98,199,98,1)"],["0.791666666666666","rgba(119,207,85,1)"],["0.833333333333334","rgba(147,214,70,1)"],["0.875","rgba(172,220,52,1)"],["0.916666666666666","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":false,"color":[32.7,36.025,32.525,33.2333333333333,32.8,32.925,31.85,32.5,33.5,31.5,33.0333333333333,34.4333333333333,32.9,32.65,31.35,31.55,31.3,32.3,33.5666666666667,33.3,32.44,32.15,31.15,35.08,33.975,31.9,33.4,32.1,33.3,33.3666666666667,33.3666666666667,31.3,31,31.15,34.05,32.6,33.54,33.3,32.55,33.0666666666667,34.3666666666667,33.4,32.55,33.5666666666667,32.4,29.4,32.72,31.35,33.55,31.45,34.55,36.4,31.5,33.6666666666667,33.3333333333333,31.75,33.525,31,32.05,33.25,31.4,33.9333333333333,32.6,32.6,32,32.05,33.25,32.95],"size":[10,70,70,40,10,70,10,10,40,10,40,40,10,10,10,10,10,10,40,40,100,10,10,100,70,10,10,10,10,40,40,40,10,10,10,10,100,10,10,40,40,40,10,40,10,10,100,10,10,10,10,10,10,40,40,10,70,10,10,10,10,40,10,100,10,10,70,10],"sizemode":"area","line":{"colorbar":{"title":"","ticklen":2},"cmin":29.4,"cmax":36.4,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666668","rgba(70,19,97,1)"],["0.0833333333333332","rgba(72,32,111,1)"],["0.125","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666667","rgba(56,93,140,1)"],["0.333333333333333","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666666","rgba(42,123,142,1)"],["0.458333333333334","rgba(38,133,141,1)"],["0.5","rgba(37,144,140,1)"],["0.541666666666666","rgba(33,154,138,1)"],["0.583333333333334","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333334","rgba(79,191,110,1)"],["0.75","rgba(98,199,98,1)"],["0.791666666666666","rgba(119,207,85,1)"],["0.833333333333334","rgba(147,214,70,1)"],["0.875","rgba(172,220,52,1)"],["0.916666666666666","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":false,"color":[32.7,36.025,32.525,33.2333333333333,32.8,32.925,31.85,32.5,33.5,31.5,33.0333333333333,34.4333333333333,32.9,32.65,31.35,31.55,31.3,32.3,33.5666666666667,33.3,32.44,32.15,31.15,35.08,33.975,31.9,33.4,32.1,33.3,33.3666666666667,33.3666666666667,31.3,31,31.15,34.05,32.6,33.54,33.3,32.55,33.0666666666667,34.3666666666667,33.4,32.55,33.5666666666667,32.4,29.4,32.72,31.35,33.55,31.45,34.55,36.4,31.5,33.6666666666667,33.3333333333333,31.75,33.525,31,32.05,33.25,31.4,33.9333333333333,32.6,32.6,32,32.05,33.25,32.95]}},"textfont":{"size":[10,70,70,40,10,70,10,10,40,10,40,40,10,10,10,10,10,10,40,40,100,10,10,100,70,10,10,10,10,40,40,40,10,10,10,10,100,10,10,40,40,40,10,40,10,10,100,10,10,10,10,10,10,40,40,10,70,10,10,10,10,40,10,100,10,10,70,10]},"error_y":{"width":[]},"error_x":{"width":[]},"xaxis":"x","yaxis":"y","frame":null},{"x":[6744,17716],"y":[34.5,45.6666666666667],"type":"scatter","mode":"markers","opacity":0,"hoverinfo":"none","showlegend":false,"marker":{"colorbar":{"title":"MaxTemp","ticklen":2,"len":0.5,"lenmode":"fraction","y":1,"yanchor":"top"},"cmin":29.4,"cmax":36.4,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666668","rgba(70,19,97,1)"],["0.0833333333333332","rgba(72,32,111,1)"],["0.125","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666667","rgba(56,93,140,1)"],["0.333333333333333","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666666","rgba(42,123,142,1)"],["0.458333333333334","rgba(38,133,141,1)"],["0.5","rgba(37,144,140,1)"],["0.541666666666666","rgba(33,154,138,1)"],["0.583333333333334","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333334","rgba(79,191,110,1)"],["0.75","rgba(98,199,98,1)"],["0.791666666666666","rgba(119,207,85,1)"],["0.833333333333334","rgba(147,214,70,1)"],["0.875","rgba(172,220,52,1)"],["0.916666666666666","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":true,"color":[29.4,36.4],"line":{"color":"rgba(255,127,14,1)"}},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"shinyEvents":["plotly_hover","plotly_click","plotly_selected","plotly_relayout","plotly_brushed","plotly_brushing","plotly_clickannotation","plotly_doubleclick","plotly_deselect","plotly_afterplot","plotly_sunburstclick"],"base_url":"https://plot.ly"},"evals":[],"jsHooks":[]}</script><!--/html_preserve--></code></pre>

</div>

### Heat warning rankings

Let's look at some top five lists.

#### Longest heat events

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>5</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [5]</span></span>
<span class='c'>#&gt;    Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>  </span><span style='text-decoration: underline;'>1</span><span>999 1999-07-22 1999-07-26      5       36.8    32.4     26.1    21.2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>  </span><span style='text-decoration: underline;'>2</span><span>001 2001-08-05 2001-08-09      5       41      35.1     </span><span style='color: #BB0000;'>NA</span><span>      </span><span style='color: #BB0000;'>NA</span><span>  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>  </span><span style='text-decoration: underline;'>2</span><span>005 2005-07-10 2005-07-14      5       39.2    33.5     28.2    22.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>  </span><span style='text-decoration: underline;'>2</span><span>010 2010-07-04 2010-07-08      5       41.6    32.7     28.0    23.4</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>  </span><span style='text-decoration: underline;'>2</span><span>016 2016-08-09 2016-08-13      5       39.6    32.6     27.3    22.9</span></span>
</code></pre>

</div>

#### Hottest heat events

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>5</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [5]</span></span>
<span class='c'>#&gt;    Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>  </span><span style='text-decoration: underline;'>2</span><span>011 2011-07-20 2011-07-21      2       44.5    36.4     30.6    24.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988 1988-07-07 1988-08-19      4       39.8    36.0     28.8    21.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>  </span><span style='text-decoration: underline;'>2</span><span>001 2001-08-05 2001-08-09      5       41      35.1     </span><span style='color: #BB0000;'>NA</span><span>      </span><span style='color: #BB0000;'>NA</span><span>  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>  </span><span style='text-decoration: underline;'>2</span><span>011 2011-07-17 2011-07-18      2       41.5    34.6     28.4    23.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>  </span><span style='text-decoration: underline;'>1</span><span>995 1995-07-13 1995-08-29      3       44.3    34.4     27.7    21.7</span></span>
</code></pre>

</div>

#### Least relief

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>MinTemp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>5</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [5]</span></span>
<span class='c'>#&gt;    Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>  </span><span style='text-decoration: underline;'>2</span><span>006 2006-07-31 2006-08-02      3       45.7    34.4     29.1    25.0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>  </span><span style='text-decoration: underline;'>2</span><span>011 2011-07-20 2011-07-21      2       44.5    36.4     30.6    24.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>  </span><span style='text-decoration: underline;'>2</span><span>002 2002-07-31 2002-08-01      2       40      33.3     28.2    24.3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>  </span><span style='text-decoration: underline;'>2</span><span>010 2010-08-30 2010-08-31      2       40.5    33.6     28.6    23.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>  </span><span style='text-decoration: underline;'>2</span><span>013 2013-07-16 2013-07-19      4       42.8    33.5     28.6    23.9</span></span>
</code></pre>

</div>

#### Highest Humidex

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>MaxHumidex</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>5</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [5]</span></span>
<span class='c'>#&gt;    Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>  </span><span style='text-decoration: underline;'>2</span><span>006 2006-07-31 2006-08-02      3       45.7    34.4     29.1    25.0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>  </span><span style='text-decoration: underline;'>2</span><span>011 2011-07-20 2011-07-21      2       44.5    36.4     30.6    24.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>  </span><span style='text-decoration: underline;'>1</span><span>995 1995-07-13 1995-08-29      3       44.3    34.4     27.7    21.7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span>  </span><span style='text-decoration: underline;'>2</span><span>002 2002-07-28 2002-07-29      2       43.5    32.1     26.0    21.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span>  </span><span style='text-decoration: underline;'>1</span><span>999 1999-07-04 1999-08-11      3       43.3    33.6     28.6    23</span></span>
</code></pre>

</div>

We can also summarize these events by year. For the temperature and humidex, I'll weight the averages by the heat alert length.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>summ</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>left_join</a></span><span class='o'>(</span><span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Year</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span>,
                  <span class='nv'>tpe_warns</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Year</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
                    <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>AvgLen <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span>,
                              AvgTmax <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/weighted.mean.html'>weighted.mean</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span>, <span class='nv'>Length</span><span class='o'>)</span>,
                              AvgTmax <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/weighted.mean.html'>weighted.mean</a></span><span class='o'>(</span><span class='nv'>MinTemp</span>, <span class='nv'>Length</span><span class='o'>)</span>,
                              AvgTmean <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/weighted.mean.html'>weighted.mean</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span>, <span class='nv'>Length</span><span class='o'>)</span>,
                              AvgMaxHumidex <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/weighted.mean.html'>weighted.mean</a></span><span class='o'>(</span><span class='nv'>MaxHumidex</span>, <span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span>,
                  by <span class='o'>=</span> <span class='s'>"Year"</span><span class='o'>)</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>


<span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>summ</span>, n <span class='o'>=</span> <span class='kc'>Inf</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 25 x 6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year [25]</span></span>
<span class='c'>#&gt;     Year     n AvgLen AvgTmax AvgTmean AvgMaxHumidex</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>         </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>  </span><span style='text-decoration: underline;'>1</span><span>988     4   3.25    21.9     27.7          40.7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>  </span><span style='text-decoration: underline;'>1</span><span>991     2   3       21.8     27.3          39  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>  </span><span style='text-decoration: underline;'>1</span><span>993     2   2       21.0     26.3          40.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>  </span><span style='text-decoration: underline;'>1</span><span>994     2   2.5     20.9     26.7          41  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>  </span><span style='text-decoration: underline;'>1</span><span>995     3   2.67    21.4     26.8          40.4</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>  </span><span style='text-decoration: underline;'>1</span><span>997     1   2       21.4     26.7          39  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>  </span><span style='text-decoration: underline;'>1</span><span>998     3   2       21.3     26.1          38.3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>  </span><span style='text-decoration: underline;'>1</span><span>999     4   3.25    21.8     27.0          39.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>  </span><span style='text-decoration: underline;'>2</span><span>001     3   3       </span><span style='color: #BB0000;'>NA</span><span>       </span><span style='color: #BB0000;'>NA</span><span>            39.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>  </span><span style='text-decoration: underline;'>2</span><span>002     6   2.5     22.7     27.6          39.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>11</span><span>  </span><span style='text-decoration: underline;'>2</span><span>003     3   2.67    20.7     26.5          38.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>12</span><span>  </span><span style='text-decoration: underline;'>2</span><span>004     1   2       19.3     25.0          39  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>13</span><span>  </span><span style='text-decoration: underline;'>2</span><span>005     4   2.75    22.2     27.7          40.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>14</span><span>  </span><span style='text-decoration: underline;'>2</span><span>006     3   2.67    23.0     28.1          41.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>15</span><span>  </span><span style='text-decoration: underline;'>2</span><span>007     3   2.67    22.0     27.6          40  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>16</span><span>  </span><span style='text-decoration: underline;'>2</span><span>008     1   2       19.4     24.9          39.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>17</span><span>  </span><span style='text-decoration: underline;'>2</span><span>009     1   2       20.4     23.2          40.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>18</span><span>  </span><span style='text-decoration: underline;'>2</span><span>010     3   3       23.1     27.8          41.3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>19</span><span>  </span><span style='text-decoration: underline;'>2</span><span>011     4   2       23.0     27.9          40.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>20</span><span>  </span><span style='text-decoration: underline;'>2</span><span>012     2   3       22.2     27.4          40.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>21</span><span>  </span><span style='text-decoration: underline;'>2</span><span>013     2   3       23.3     28            41.8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>22</span><span>  </span><span style='text-decoration: underline;'>2</span><span>015     2   2       20.3     26.4          38.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>23</span><span>  </span><span style='text-decoration: underline;'>2</span><span>016     5   2.8     21.5     27.0          38.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>24</span><span>  </span><span style='text-decoration: underline;'>2</span><span>017     1   2       21.6     27.2          38.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>25</span><span>  </span><span style='text-decoration: underline;'>2</span><span>018     3   2.67    21.4     27.2          41.5</span></span>
</code></pre>

</div>

According to this summary, we have already seen heat warning weather three times in 2018, which is above the average of 2.72. If this weather keeps up, we might beat the record of 6 heat events in 2002. Let's take a look on a plot. I have added an extra variable, `n`, so I'll have to discard one of my climate variables. In the below plot, the y-axis represents the number of heat waves per year, the size of the point is the average length, and the colour is the average daily maximum humidex during heat warnings.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/plotly/man/plot_ly.html'>plot_ly</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>summ</span>, x <span class='o'>=</span> <span class='o'>~</span><span class='nv'>Year</span>, y <span class='o'>=</span> <span class='o'>~</span><span class='nv'>n</span>, color <span class='o'>=</span> <span class='o'>~</span><span class='nv'>AvgMaxHumidex</span>, size <span class='o'>=</span> <span class='o'>~</span><span class='nv'>AvgLen</span>,
        hoverinfo <span class='o'>=</span> <span class='s'>"text"</span>, hovertext <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>$</span><span class='nv'>n</span>,
                                              <span class='s'>"event(s) with mean duration of"</span>,
                                              <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>$</span><span class='nv'>AvgLen</span>, <span class='m'>1</span><span class='o'>)</span>,
                                              <span class='s'>"days"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/add_trace.html'>add_markers</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/plotly/man/layout.html'>layout</a></span><span class='o'>(</span>
    title <span class='o'>=</span> <span class='s'>"Annual summertime heat warning weather in Toronto, 1988-2018 (YTD)"</span>,
    yaxis <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Number of heat warnings"</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; Warning: `line.width` does not currently support multiple values.</span>

<!--html_preserve--><div id="htmlwidget-630c103a069051aa9e3d" style="width:700px;height:415.296px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-630c103a069051aa9e3d">{"x":{"visdat":{"20da6da76ea1":["function () ","plotlyVisDat"]},"cur_data":"20da6da76ea1","attrs":{"20da6da76ea1":{"x":{},"y":{},"hoverinfo":"text","hovertext":["4 event(s) with mean duration of 3.2 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 2 days","2 event(s) with mean duration of 2.5 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 2 days","4 event(s) with mean duration of 3.2 days","3 event(s) with mean duration of 3 days","6 event(s) with mean duration of 2.5 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","4 event(s) with mean duration of 2.8 days","3 event(s) with mean duration of 2.7 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 3 days","4 event(s) with mean duration of 2 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 2 days","5 event(s) with mean duration of 2.8 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 2.7 days"],"color":{},"size":{},"alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"scatter","mode":"markers","inherit":true}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"title":"Annual summertime heat warning weather in Toronto, 1988-2018 (YTD)","yaxis":{"domain":[0,1],"automargin":true,"title":"Number of heat warnings"},"xaxis":{"domain":[0,1],"automargin":true,"title":"Year"},"hovermode":"closest","showlegend":false,"legend":{"yanchor":"top","y":0.5}},"source":"A","config":{"showSendToCloud":false},"data":[{"x":[1988,1991,1993,1994,1995,1997,1998,1999,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2015,2016,2017,2018],"y":[4,2,2,2,3,1,3,4,3,6,3,1,4,3,3,1,1,3,4,2,2,2,5,1,3],"hoverinfo":["text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text"],"hovertext":["4 event(s) with mean duration of 3.2 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 2 days","2 event(s) with mean duration of 2.5 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 2 days","4 event(s) with mean duration of 3.2 days","3 event(s) with mean duration of 3 days","6 event(s) with mean duration of 2.5 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","4 event(s) with mean duration of 2.8 days","3 event(s) with mean duration of 2.7 days","3 event(s) with mean duration of 2.7 days","1 event(s) with mean duration of 2 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 3 days","4 event(s) with mean duration of 2 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 3 days","2 event(s) with mean duration of 2 days","5 event(s) with mean duration of 2.8 days","1 event(s) with mean duration of 2 days","3 event(s) with mean duration of 2.7 days"],"type":"scatter","mode":"markers","marker":{"colorbar":{"title":"AvgMaxHumidex","ticklen":2},"cmin":38.3333333333333,"cmax":41.875,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666662","rgba(70,19,97,1)"],["0.0833333333333325","rgba(72,32,111,1)"],["0.125000000000001","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666666","rgba(56,93,140,1)"],["0.333333333333334","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666667","rgba(42,123,142,1)"],["0.458333333333333","rgba(38,133,141,1)"],["0.500000000000001","rgba(37,144,140,1)"],["0.541666666666667","rgba(33,154,138,1)"],["0.583333333333333","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333332","rgba(79,191,110,1)"],["0.750000000000001","rgba(98,199,98,1)"],["0.791666666666667","rgba(119,207,85,1)"],["0.833333333333333","rgba(147,214,70,1)"],["0.874999999999999","rgba(172,220,52,1)"],["0.916666666666668","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":false,"color":[40.6923076923077,39,40.5,41,40.375,39,38.3333333333333,39.6153846153846,39.8888888888889,39.8666666666667,38.5,39,40.0909090909091,41.875,40,39.5,40.5,41.3333333333333,40.625,40.5,41.8333333333333,38.5,38.5,38.5,41.5],"size":[100,82,10,46,58,10,10,100,82,46,58,10,64,58,58,10,10,82,10,82,82,10,67.6,10,58],"sizemode":"area","line":{"colorbar":{"title":"","ticklen":2},"cmin":38.3333333333333,"cmax":41.875,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666662","rgba(70,19,97,1)"],["0.0833333333333325","rgba(72,32,111,1)"],["0.125000000000001","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666666","rgba(56,93,140,1)"],["0.333333333333334","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666667","rgba(42,123,142,1)"],["0.458333333333333","rgba(38,133,141,1)"],["0.500000000000001","rgba(37,144,140,1)"],["0.541666666666667","rgba(33,154,138,1)"],["0.583333333333333","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333332","rgba(79,191,110,1)"],["0.750000000000001","rgba(98,199,98,1)"],["0.791666666666667","rgba(119,207,85,1)"],["0.833333333333333","rgba(147,214,70,1)"],["0.874999999999999","rgba(172,220,52,1)"],["0.916666666666668","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":false,"color":[40.6923076923077,39,40.5,41,40.375,39,38.3333333333333,39.6153846153846,39.8888888888889,39.8666666666667,38.5,39,40.0909090909091,41.875,40,39.5,40.5,41.3333333333333,40.625,40.5,41.8333333333333,38.5,38.5,38.5,41.5]}},"textfont":{"size":[100,82,10,46,58,10,10,100,82,46,58,10,64,58,58,10,10,82,10,82,82,10,67.6,10,58]},"error_y":{"width":[]},"error_x":{"width":[]},"xaxis":"x","yaxis":"y","frame":null},{"x":[1988,2018],"y":[1,6],"type":"scatter","mode":"markers","opacity":0,"hoverinfo":"none","showlegend":false,"marker":{"colorbar":{"title":"AvgMaxHumidex","ticklen":2,"len":0.5,"lenmode":"fraction","y":1,"yanchor":"top"},"cmin":38.3333333333333,"cmax":41.875,"colorscale":[["0","rgba(68,1,84,1)"],["0.0416666666666662","rgba(70,19,97,1)"],["0.0833333333333325","rgba(72,32,111,1)"],["0.125000000000001","rgba(71,45,122,1)"],["0.166666666666667","rgba(68,58,128,1)"],["0.208333333333333","rgba(64,70,135,1)"],["0.25","rgba(60,82,138,1)"],["0.291666666666666","rgba(56,93,140,1)"],["0.333333333333334","rgba(49,104,142,1)"],["0.375","rgba(46,114,142,1)"],["0.416666666666667","rgba(42,123,142,1)"],["0.458333333333333","rgba(38,133,141,1)"],["0.500000000000001","rgba(37,144,140,1)"],["0.541666666666667","rgba(33,154,138,1)"],["0.583333333333333","rgba(39,164,133,1)"],["0.625","rgba(47,174,127,1)"],["0.666666666666666","rgba(53,183,121,1)"],["0.708333333333332","rgba(79,191,110,1)"],["0.750000000000001","rgba(98,199,98,1)"],["0.791666666666667","rgba(119,207,85,1)"],["0.833333333333333","rgba(147,214,70,1)"],["0.874999999999999","rgba(172,220,52,1)"],["0.916666666666668","rgba(199,225,42,1)"],["0.958333333333334","rgba(226,228,40,1)"],["1","rgba(253,231,37,1)"]],"showscale":true,"color":[38.3333333333333,41.875],"line":{"color":"rgba(255,127,14,1)"}},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"shinyEvents":["plotly_hover","plotly_click","plotly_selected","plotly_relayout","plotly_brushed","plotly_brushing","plotly_clickannotation","plotly_doubleclick","plotly_deselect","plotly_afterplot","plotly_sunburstclick"],"base_url":"https://plot.ly"},"evals":[],"jsHooks":[]}</script><!--/html_preserve--></code></pre>

</div>

Heat warning trends
-------------------

To perform a fair trend detection, I need to filter off 2018, since it is not a complete year. In this regression analysis, I'll weight the climate metrics by the number of events, so that the years with more events are weighted more strongly.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dat</span> <span class='o'>&lt;-</span> <span class='nv'>summ</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Year</span> <span class='o'>!=</span> <span class='m'>2018</span><span class='o'>)</span>
<span class='nv'>splits</span> <span class='o'>&lt;-</span> <span class='nv'>dat</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/gather.html'>gather</a></span><span class='o'>(</span>key <span class='o'>=</span> <span class='nv'>var</span>, value <span class='o'>=</span> <span class='nv'>value</span>, <span class='o'>-</span><span class='nv'>Year</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/base/split.html'>split</a></span><span class='o'>(</span><span class='nv'>.</span>, <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>var</span><span class='o'>)</span><span class='o'>)</span>

<span class='nv'>splits</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://purrr.tidyverse.org/reference/map.html'>map_df</a></span><span class='o'>(</span><span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='nv'>fit</span> <span class='o'>&lt;-</span> <span class='kr'>if</span> <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='m'>2</span>,<span class='m'>1</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>!=</span> <span class='s'>"n"</span><span class='o'>)</span> <span class='o'>{</span>
      <span class='nf'><a href='https://rdrr.io/r/stats/lm.html'>lm</a></span><span class='o'>(</span><span class='nv'>value</span><span class='o'>~</span><span class='nv'>Year</span>, data <span class='o'>=</span> <span class='nv'>x</span>, weights <span class='o'>=</span> <span class='nv'>dat</span><span class='o'>$</span><span class='nv'>n</span><span class='o'>)</span>
    <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
      <span class='nf'><a href='https://rdrr.io/r/stats/lm.html'>lm</a></span><span class='o'>(</span><span class='nv'>value</span><span class='o'>~</span><span class='nv'>Year</span>, data <span class='o'>=</span> <span class='nv'>x</span><span class='o'>)</span>
    <span class='o'>}</span>
    <span class='nv'>summ</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/summary.html'>summary</a></span><span class='o'>(</span><span class='nv'>fit</span><span class='o'>)</span>
    <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>slope <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/coef.html'>coef</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>)</span><span class='o'>[</span><span class='m'>2</span><span class='o'>]</span>, rsq <span class='o'>=</span> <span class='nv'>summ</span><span class='o'>$</span><span class='nv'>r.squared</span>, p <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/coef.html'>coef</a></span><span class='o'>(</span><span class='nv'>summ</span><span class='o'>)</span><span class='o'>[</span><span class='m'>8</span><span class='o'>]</span><span class='o'>)</span>
    <span class='o'>}</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>name <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>splits</span><span class='o'>)</span>, .before <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 4</span></span>
<span class='c'>#&gt;   name             slope     rsq     p</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>            </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> AvgLen        -</span><span style='color: #BB0000;'>0.011</span><span style='color: #BB0000;text-decoration: underline;'>4</span><span>  0.047</span><span style='text-decoration: underline;'>0</span><span>  0.309</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> AvgMaxHumidex -</span><span style='color: #BB0000;'>0.013</span><span style='color: #BB0000;text-decoration: underline;'>7</span><span>  0.011</span><span style='text-decoration: underline;'>6</span><span>  0.616</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> AvgTmax        0.017</span><span style='text-decoration: underline;'>3</span><span>  0.023</span><span style='text-decoration: underline;'>6</span><span>  0.484</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> AvgTmean       0.007</span><span style='text-decoration: underline;'>68</span><span> 0.005</span><span style='text-decoration: underline;'>43</span><span> 0.738</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> n              0.005</span><span style='text-decoration: underline;'>40</span><span> 0.001</span><span style='text-decoration: underline;'>06</span><span> 0.880</span></span>
</code></pre>

</div>

The linear regression did not detect any significant trends in the aggregate summertime heat warning weather. There are, a few negative slopes in the chart, but none of them are even close to being statistically significant. The $R^2$ values are also very low, suggesting that a linear model does not explain the variance in these numbers very well. Indeed, examining the charts above, it seems that we have a lot of variation in the number, duration, and magnitude of heat-events from one year to the next. They are common, but there seems to be more than a simple year-to-year change. It is also interesting to note that, somewhat by chance, I started my analysis on a period of intense heat in much of North America. Indeed, in 1988, we saw the second hottest period of heat warning weather that we've seen in the past 30 years. In addition, we saw some very strong events towards the middle of the series, in particular the mid 2000s. In the interim between the strong events, we have a lot of rather insignificant years. Summer 2017, for instance, was cool and damp, and saw widespread flooding, right after 2016, which saw the second highest number of heat events in the past 30 years. I am interested to follow this topic into the future, as we certainly seem to be seeing *something* since around the year 2000, but the signal hasn't come out through the noise in this particular data set.

What about 2018?
----------------

If we aren't seeing increasing trends in the last 30 years, and the warm events so far this year have been relatively short, what was all the fuss about this month? Was it just a case of our memory biasing more recent events? Do we Canadians just *love* to complain about the weather.[^1] Not exactly. I think we found the heatwave in July to be particularly unbearable because of the lack of relief. The first ECCC weather alert (which someone has, thankfully, archived [here](https://web.archive.org/web/20180627002238/https://weather.gc.ca/warnings/report_e.html?on61)) came out on June 26, forecasting extreme weather from June 28 or June 29 through to July 2. Their forecast was spot on. On July 3, [another alert](https://web.archive.org/web/20180704025646/https://weather.gc.ca/warnings/report_e.html?on61) was issued for extreme weather through to Thursday July 5. ECCC called it "the most significant heat event in the past few years". Indeed, two of the three heat events so far this year were June 29 to July 2, and July 4 to July 5. July 3 saw temperatures above 30 degrees, but missed the $T_{max}$ threshold by 0.8°C and the $T_{min}$ threshold by just three tenths of a degree Celsius. We can tweak our criteria a little. Let's say, "if the two days before were super hot, and the two days after were super hot, then it was damn hot!"

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_dly</span> <span class='o'>&lt;-</span> <span class='nv'>tpe_dly</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Criteria <span class='o'>=</span> <span class='nv'>Criteria</span> <span class='o'>|</span> 
           <span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lead</a></span><span class='o'>(</span><span class='nv'>Criteria</span>, <span class='m'>2</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lead</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>Criteria</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>

<span class='nv'>tpe_warns_lax</span> <span class='o'>&lt;-</span> <span class='nv'>tpe_dly</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Year <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/year.html'>year</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Start <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span>, <span class='nv'>Date</span>, <span class='nv'>min</span><span class='o'>)</span>,
                         origin <span class='o'>=</span> <span class='nv'>origin</span><span class='o'>)</span>,
         End <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span>, <span class='nv'>Date</span>, <span class='nv'>max</span><span class='o'>)</span>,
                         origin <span class='o'>=</span> <span class='nv'>origin</span><span class='o'>)</span>,
         Length <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/claut/man/describe_snaps.html'>describe_snaps</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Year</span>, <span class='nv'>Start</span>, <span class='nv'>End</span>, <span class='nv'>Length</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>mean</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>Criteria</span>, <span class='o'>-</span><span class='nv'>Date</span><span class='o'>)</span>
</code></pre>

</div>

Now, let's take a look at the length of the top 3 events.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_warns_lax</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>3</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 3 x 8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Year, Start, End [3]</span></span>
<span class='c'>#&gt;    Year Start      End        Length MaxHumidex MaxTemp MeanTemp MinTemp</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span>  </span><span style='text-decoration: underline;'>2</span><span>018 2018-06-29 2018-07-05      7       40.7    32.7     27.3    21.5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span>  </span><span style='text-decoration: underline;'>1</span><span>999 1999-07-22 1999-07-26      5       36.8    32.4     26.1    21.2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span>  </span><span style='text-decoration: underline;'>2</span><span>001 2001-08-05 2001-08-09      5       41      35.1     </span><span style='color: #BB0000;'>NA</span><span>      </span><span style='color: #BB0000;'>NA</span></span>
</code></pre>

</div>

There you have it! The Canada Day 2018 heat event *was* the longest in the past 30 years, if we leave room for one brief moment of "relief".

Origin of air during heat events
--------------------------------

So, where does this hot weather come from? I think this goes without saying, but it certainly isn't polar air in the city when we're in the middle of a heat wave. Indeed, ECCC's heat warnings often contain language like "a passing warm front" or, like the June 26 warning linked to above, "hot, humid air from the Gulf of Mexico". So, is it always warm, tropical air that causes us to feel the heat? Let's look at the SSC2 air masses by Sheridan ([2002](#ref-Sheridan_2002_redevelopment)) that I've used in my previous posts.

I will bin these values, as I have done in the past, but I will split the tropical air into dry and moist bins, which were less important for winter weather, but are useful here.

1.  'cool': SSC codes 2 (dry polar) and 5 (moist polar)
2.  'mod': SSC codes 1 (dry moderate) and 4 (moist moderate)
3.  'dt': SSC code 3 (dry tropical)
4.  'mt': SSC codes 6 (moist tropical), 66 (moist tropical plus), and 67 (moist tropical double plus)
5.  'trans': SCC code 7 (transition)

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>f</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/tempfile.html'>tempfile</a></span><span class='o'>(</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/download.file.html'>download.file</a></span><span class='o'>(</span><span class='s'>"http://sheridan.geog.kent.edu/ssc/files/YYZ.dbdmt"</span>, <span class='nv'>f</span>, quiet <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
<span class='nv'>air</span> <span class='o'>&lt;-</span> <span class='nf'>read_table</span><span class='o'>(</span><span class='nv'>f</span>, col_names <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Station"</span>, <span class='s'>"Date"</span>, <span class='s'>"Air"</span><span class='o'>)</span>, col_types <span class='o'>=</span> <span class='nf'>cols</span><span class='o'>(</span><span class='nf'>col_character</span><span class='o'>(</span><span class='o'>)</span>, <span class='nf'>col_date</span><span class='o'>(</span>format <span class='o'>=</span> <span class='s'>"%Y%m%d"</span><span class='o'>)</span>, <span class='nf'>col_factor</span><span class='o'>(</span>levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>4</span>, <span class='m'>5</span>, <span class='m'>6</span>, <span class='m'>7</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/levels.html'>levels</a></span><span class='o'>(</span><span class='nv'>air</span><span class='o'>$</span><span class='nv'>Air</span><span class='o'>)</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>cool <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>5</span><span class='o'>)</span>, mod <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>4</span><span class='o'>)</span>, dt <span class='o'>=</span> <span class='m'>3</span>, mt <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>6</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span>, trans <span class='o'>=</span> <span class='s'>"7"</span><span class='o'>)</span>
</code></pre>

</div>

I will add these air mass codes to the daily data set, and determine the proportion of each air mass type during heat warning weather.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tpe_air</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>left_join</a></span><span class='o'>(</span><span class='nv'>tpe_dly</span>, <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>air</span>, <span class='o'>-</span><span class='nv'>Station</span><span class='o'>)</span>, by <span class='o'>=</span> <span class='s'>"Date"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Criteria</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Air</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>prop <span class='o'>=</span> <span class='nv'>n</span><span class='o'>/</span><span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>tpe_dly</span><span class='o'>$</span><span class='nv'>Criteria</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>n</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nv'>ungroup</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/spread.html'>spread</a></span><span class='o'>(</span><span class='nv'>Air</span>, <span class='nv'>prop</span>, fill <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span>

<span class='nv'>tpe_air</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 1 x 5</span></span>
<span class='c'>#&gt;      mod    dt    mt  trans  `&lt;NA&gt;`</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 0.012</span><span style='text-decoration: underline;'>0</span><span> 0.474 0.458 0.047</span><span style='text-decoration: underline;'>8</span><span> 0.007</span><span style='text-decoration: underline;'>97</span></span>
</code></pre>

</div>

It's true! When we experience super hot weather in the city, it is due to the presence of a warm front of hot, tropical air over the city. Of all the summer days where the criteria for a heat warning were met, none showed air of polar origin, and only 1.2% showed continental, "moderate" air. Indeed, a full 93.2% of days that merited a heat warning showed air of tropical origin (47.4% dry tropical and 45.8% moist tropical). That's right. Toronto, at 43°N latitude, is affected by air from the tropics! No need to visit the Caribbean to get that *clima caribeño*!

Outro
-----

So, that's that. A brief exploration of the kind of weather that provokes heat warnings in Toronto. So remember, next time you are sweating buckets and wishing the heat would just go away, head over to Toronto Island and pretend you're in some tropical paradise. If that doesn't work, wait a minute. It'll be winter in no time!

------------------------------------------------------------------------

*This post was compiled on 2020-10-09 12:01:39. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package      * version    date       lib source                             </span>
<span class='c'>#&gt;  assertthat     0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  canadaHCD    * 0.0-2      2020-10-01 [1] Github (ConorIA/canadaHCD@e2f4d2b) </span>
<span class='c'>#&gt;  canadaHCDx   * 0.0.8      2020-10-01 [1] gitlab (ConorIA/canadaHCDx@0f99419)</span>
<span class='c'>#&gt;  claut        * 0.1.10     2020-10-08 [1] local                              </span>
<span class='c'>#&gt;  cli            2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  codetools      0.2-16     2018-12-24 [3] CRAN (R 4.0.2)                     </span>
<span class='c'>#&gt;  colorspace     1.4-1      2019-03-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  crayon         1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  crosstalk      1.1.0.1    2020-03-13 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  curl           4.3        2019-12-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  data.table     1.13.0     2020-07-24 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  digest         0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  doParallel     1.0.15     2019-08-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  downlit        0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  dplyr        * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  ellipsis       0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  evaluate       0.14       2019-05-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fansi          0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  farver         2.0.3      2020-01-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  foreach        1.5.0      2020-03-30 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fs             1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  gdata          2.18.0     2017-06-06 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  generics       0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  geosphere      1.5-10     2019-05-26 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  ggplot2      * 3.3.2      2020-06-19 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  glue           1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  gtable         0.3.0      2019-03-25 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  gtools         3.8.2      2020-03-31 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  hms            0.5.3      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  htmltools      0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  htmlwidgets    1.5.1      2019-10-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  httr           1.4.2      2020-07-20 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  hugodown       0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)    </span>
<span class='c'>#&gt;  iterators      1.0.12     2019-07-26 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  jsonlite       1.7.1      2020-09-07 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  knitr          1.30       2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  lattice        0.20-41    2020-04-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lazyeval       0.2.2      2019-03-15 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lifecycle      0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lpSolve        5.6.15     2020-01-24 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lubridate    * 1.7.9      2020-06-08 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  magrittr       1.5        2014-11-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  munsell        0.5.0      2018-06-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  pillar         1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  pkgconfig      2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  plotly       * 4.9.2.1    2020-04-04 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  plyr           1.8.6      2020-03-03 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  purrr        * 0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  R6             2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  rappdirs       0.3.1      2016-03-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  RColorBrewer   1.1-2      2014-12-07 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  Rcpp           1.0.5      2020-07-06 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  readr          1.3.1      2018-12-21 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  reshape2       1.4.4      2020-04-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rlang          0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rmarkdown      2.3        2020-06-18 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  scales         1.1.1      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sessioninfo    1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sp             1.4-2      2020-05-20 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  storr          1.2.1      2018-10-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  stringi        1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  stringr        1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  tibble       * 3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyr        * 1.1.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyselect     1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  utf8           1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  vctrs          0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  viridisLite    0.3.0      2018-02-01 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  withr          2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  xfun           0.18       2020-09-29 [2] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  yaml           2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  zoo            1.8-8      2020-05-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

References
----------

<div id="refs" class="references">

<div id="ref-Anderson_2017_Evolution">

Anderson, Conor I., and William A. Gough. 2017. "Evolution of Winter Temperature in Toronto, Ontario, Canada: A Case Study of Winters 2013/14 and 2014/15." *Journal of Climate* 30 (14): 5361--76. <https://doi.org/10.1175/JCLI-D-16-0562.1>.

</div>

<div id="ref-anderson_2018_characterization">

Anderson, Conor I., William A. Gough, and Tanzina Mohsin. 2018. "Characterization of the Urban Heat Island at Toronto: Revisiting the Choice of Rural Sites Using a Measure of Day-to-Day Variation." *Urban Climate* 25 (September): 187--95. <https://doi.org/10.1016/J.UCLIM.2018.07.002>.

</div>

<div id="ref-mohsin_2012_characterization">

Mohsin, Tanzina, and William A. Gough. 2012. "Characterization and Estimation of Urban Heat Island at Toronto: Impact of the Choice of Rural Sites." *Theoretical and Applied Climatology* 108 (April): 105--17. <https://doi.org/10.1007/s00704-011-0516-7>.

</div>

<div id="ref-Sheridan_2002_redevelopment">

Sheridan, Scott C. 2002. "The Redevelopment of a Weather-Type Classification Scheme for North America." *International Journal of Climatology* 22 (1): 51--68. <https://doi.org/10.1002/joc.709>.

</div>

<div id="ref-zaknic-catovic_2018_comparison">

Žaknić-Ćatović, Ana, and William A. Gough. 2018. "A Comparison of Climatological Observing Windows and Their Impact on Detecting Daily Temperature Extrema." *Theoretical and Applied Climatology* 132 (1-2): 41--54. <https://doi.org/10.1007/S00704-017-2068-Y>.

</div>

</div>

[^1]: Yes! Of course!

