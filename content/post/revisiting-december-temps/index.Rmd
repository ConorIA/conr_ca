---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Revisiting December Temperatures at Toronto"
subtitle: ""
slug: "revisiting-december-temps"
summary: "A brief, superficial analysis of winter temperatures during the coldsnap of December 2017 and January 2018."
authors: []
categories:
  - R
tags:
  - cold
  - temperature
  - Toronto
date: 2018-01-11
lastmod: 2018-01-12
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse = TRUE)
options('digits' = 3)
options(knitr.digits.signif = TRUE)
load("../../../static/files/RData/2018-01-11_revisiting_december_temps.RData")
library(canadaHCDx)
library(dplyr)
library(ggplot2)
library(readr)
library(tibble)
library(tidyr)
library(zoo)
```

## Introduction

In @Anderson_2017_Evolution, Bill Gough and I examined winter temperatures in Toronto, with a special focus on winters 2013/14 and 2014/15. With the recent cold snap, I thought it would be interesting to re-examine the subject. This will all be done in R, with the full code included in this blog post.

First, I will load the libraries that I will use.

```{r libraries, eval = FALSE}
library(canadaHCDx)
library(dplyr)
library(ggplot2)
library(readr)
library(tibble)
library(tidyr)
library(zoo)
```

Now I get the Toronto temperature data:
  
```{r, eval = FALSE}
# Toronto changed station code afer June 2003, so we'll merge it with the new one. 
tor <- hcd_daily(5051, 1840:2003)
tmp <- hcd_daily(31688, 2003:2018)
tor <- bind_rows(tor[1:which(tor$Date == "2003-06-30"),], tmp[which(tmp$Date == "2003-07-01"):which(tmp$Date == "2018-01-31"),])
```

## Plots

Let's first see what the temperature looked like. First, I prepare some "plot data", extracting December 01 to January 09 (last day of available data so far in 2018). 

```{r}
plot_data <- tor %>% select(Date, MaxTemp, MinTemp, MeanTemp) %>% filter(Date >= "2017-12-01" & Date <= "2018-01-09")
```

Let's see what that looks like:

```{r}
ggplot(data = plot_data) + geom_line(aes(Date, MaxTemp, col = "Tmax")) + geom_line(aes(Date, MinTemp, col = "Tmin")) + geom_hline(aes(yintercept = 0)) + labs(x = "Date", y = "Temperature (°C)", color = "Variable") + ggtitle("Temperature at Toronto (St. George Campus) 2017-12-01 to 2018-01-09") + theme_light()
```

We can see that the beginning of December was fairly mild, with an additional mild period around the 18th to 20th before plummeting in the last days of 2017 and beginning of 2018. 

## Comparison to daily "normals"

We can't say for sure that weather is anomalous, unless we compare it to normalized data. Let's see how December and January look, by plotting the "normal" temperature, i.e. the 30-year average for the same period for each day from 1981&ndash;2010.

First, I will generate daily normals by averaging each calendar date over the past 30 years. Note that I am not sure whether this is sloppy methodology, but thought I'd give it a shot. 

```{r warning=FALSE}
tor_norm <- tor %>% select(Date, MaxTemp, MinTemp, MeanTemp) %>% filter(format(Date, format = "%Y") >= 1981 & format(Date, format = "%Y") <= 2010)
tor_norm <- tor_norm %>% group_by(Day = format(Date, format = "%m-%d")) %>% summarize_all(mean, na.rm = TRUE) %>% select(-Date)
```

Now that I have the 30-year daily average temperature for each day of the year, I'll extract just December 1 to January 9. I'll plot these with a loess curve and use the values from that curve to represent my "typical" temperature.

```{r message=FALSE}
tor_typical <- bind_rows(tor_norm[tor_norm$Day >= "12-01",], tor_norm[tor_norm$Day <= "01-09",]) %>% add_column(Index = 1:nrow(.)) %>% add_column(MaxSmooth = predict(loess(MaxTemp~Index, .), .$Index), MinSmooth = predict(loess(MinTemp~Index, .), .$Index)) 

ggplot(data = tor_typical) + geom_line(aes(1:40, MaxTemp, col = "Tmax")) + geom_smooth(aes(1:40, MaxTemp, col = "Tmax")) + geom_line(aes(1:40, MinTemp, col = "Tmin")) + geom_smooth(aes(1:40, MinTemp, col = "Tmin")) + geom_hline(aes(yintercept = 0)) + labs(x = "Day", y = "Temperature (°C)", color = "Variable") + ggtitle("\"Typical\" Temperature at Toronto (St. George Campus) 12-01 to 01-09", subtitle = "Baseline period from 1981 to 2010; smoothed with loess curve") + theme_light()
```

Now let's compare our temperature with the "typical". 

```{r}
plot_data <- plot_data %>% add_column(MaxTempAnom = (.$MaxTemp - tor_typical$MaxSmooth), MinTempAnom = (.$MinTemp - tor_typical$MinSmooth))

ggplot(data = plot_data) + geom_line(aes(Date, MaxTempAnom, col = "Tmax")) + geom_line(aes(Date, MinTempAnom, col = "Tmin")) + geom_hline(aes(yintercept = 0))+ labs(x = "Day", y = "Temperature Anomaly (°C)", color = "Variable") + ggtitle("Temperature Anomaly at Toronto 2017-12-01 to 2018-01-09", subtitle = "Relative to smoothed 1981-2010 baseline") + theme_light()
```

Indeed, temperatures were cold, getting to be as much as 16 °C colder than the smoothed average daily temperature from 1981&ndash;2010. 

## How does December rank?

Now let's prepare some December-only data series at the daily and monthly timescales. 

```{r}
tor_dec <- tor %>% select(Date, MaxTemp, MinTemp, MeanTemp) %>% filter(format(Date, format = "%m") == 12)
tor_dec_mon <- tor_dec %>% group_by(Yearmon = format(Date, format = "%Y-%m")) %>% summarize_all(mean, na.rm = TRUE)
```

Note, I have become largely familiar with the Toronto data series, and I know that missing data is not an issue in this time series. Generally, one would need to do a quick check of the data, such as: 

```{r}
quick_audit(tor_dec, c("MaxTemp", "MinTemp", "MeanTemp"), by = "month")
```

OK, let's look at some monthly ranks! 

```{r}
tor_dec_mon <- tor_dec_mon %>% add_column(MaxTempRank = min_rank(.$MaxTemp), MinTempRank = min_rank(.$MinTemp), MeanTempRank = min_rank(.$MeanTemp))

tail(tor_dec_mon)
```

Toronto's temperature in the month of December 2017 was ranked `r tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]` for $T_{max}$ (`r tor_dec_mon$MaxTemp[nrow(tor_dec_mon)]` °C with a return interval (RI) of `r (nrow(tor_dec_mon) + 1) / tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]`) , `r tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]` for $T_{min}$ (`r tor_dec_mon$MinTemp[nrow(tor_dec_mon)]` °C with RI of `r (nrow(tor_dec_mon) + 1) / tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]`), and `r tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]` for $T_{mean}$ (`r tor_dec_mon$MeanTemp[nrow(tor_dec_mon)]` °C with RI of `r (nrow(tor_dec_mon) + 1) / tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]`). All three variables are ranked out of 178 total Decembers in the set.

The last time that we saw a December this cold was in `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$Yearmon[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -1]`. The temperature that year was:

  - $T_{max}$: `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTemp[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -1]` °C, rank `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTempRank[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -1]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTempRank[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -1]`
  - $T_{min}$: `r (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTemp[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -1]` °C, rank `r (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTempRank[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -1]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTempRank[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -1]`
  - $T_{mean}$: `r (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTemp[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -1]` °C, rank `r (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTempRank[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -1]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTempRank[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -1]`

The second-last December that was as cold or colder than December 2017 was `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$Yearmon[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -2]`. That year the temperature reached: 

  - $T_{max}$: `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTemp[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -2]` °C, rank `r (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTempRank[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -2]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)]))$MaxTempRank[nrow(tor_dec_mon %>% filter(MaxTempRank <= tor_dec_mon$MaxTempRank[nrow(tor_dec_mon)])) -2]`
  - $T_{min}$: `r (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTemp[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -2]` °C, rank `r (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTempRank[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -2]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)]))$MinTempRank[nrow(tor_dec_mon %>% filter(MinTempRank <= tor_dec_mon$MinTempRank[nrow(tor_dec_mon)])) -2]`
  - $T_{mean}$: `r (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTemp[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -2]` °C, rank `r (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTempRank[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -2]`, with RI of `r (nrow(tor_dec_mon) + 1) / (tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)]))$MeanTempRank[nrow(tor_dec_mon %>% filter(MeanTempRank <= tor_dec_mon$MeanTempRank[nrow(tor_dec_mon)])) -2]`


## Air Mass Analysis

One of the things that we looked at in our 2017 paper was the position of the jet stream. During December 2017, we know that the jet stream was to the south of Toronto during the cold snap. Let's look at this, using data from Sheridan's [Spatial Synoptic Classification (SSC)](http://sheridan.geog.kent.edu/ssc.html) [@Sheridan_2002_redevelopment].

First, I'll get the data and bin it into four bins, as per @Anderson_2017_Evolution: 

1. 'cool': SSC codes 2 (dry polar) and 5 (moist polar)
2. 'moderate': SSC codes 1 (dry moderate) and 4 (moist moderate)
3. 'warm': SSC codes 3 (dry tropical), 6 (moist tropical), 66 (moist tropical plus), and 67 (moist tropical double plus)
7. 'transition': SCC code 7 (transition)

```{r, eval = FALSE}
f <- tempfile()
download.file("http://sheridan.geog.kent.edu/ssc/files/YYZ.dbdmt", f, quiet = TRUE)
air <- read_table(f, col_names = c("Station", "Date", "Air"), col_types = cols(col_character(), col_date(format = "%Y%m%d"), col_factor(levels = c(1, 2, 3, 4, 5, 6, 7, 66, 67)))) # Note, I am ignoring code 8 because that is the SSC2 "NA" value, which is turned to NA by ignoring.
levels(air$Air) <- list(cool = c(2, 5), mod = c(1, 4), warm = c(3, 6, 66, 67), trans = "7")
```

Now I will rearrange so that I get the proportions of each. 

```{r}
# Some hints from https://stackoverflow.com/questions/25811756/summarizing-counts-of-a-factor-with-dplyr

air_dec <- air %>% filter(format(Date, format = "%m") == 12)
air_tally <- air_dec %>% select(-Station) %>% group_by(Yearmon = as.yearmon(Date), Air) %>% tally %>% spread(Air, n, fill = 0)
air_tally <- air_tally %>% filter(`<NA>` < 31) # Remove empty year-months
air_prop <- as_tibble(prop.table(as.matrix(air_tally[,2:ncol(air_tally)]), 1))
names(air_prop) <- paste(names(air_prop), "prop", sep = "_")
air_prop <- bind_cols(air_tally, air_prop)
```

The last few Decembers, for instance: 

```{r}
tail(air_prop) %>% select(-2:-6)
```

So, was it just the origin of the air that made December so cold? Not exactly. on average `r mean(air_prop$cool_prop) * 100`% of daily December air masses are of polar origin. We had `r air_prop$cool_prop[air_prop$Yearmon == "Dec 2017"] *100`% air of polar origin in December 2017.

Let's take a look at the continuity of the polar air. Here I will look for the longest continuous series of 'cool' or 'trans' air for each December, omitting any missing values. Therefore, for this analysis, a cold snap is the longest period of two or more consecutive days of polar or transitional air during each December. 

```{r}
# Took some code from https://stackoverflow.com/questions/41254014/consecutive-count-by-groups

fx <- function(x) {
  x <- na.omit(x)
  if (length(x) > 0) {
    with(rle(x == "cool" | x == "trans"), max(lengths[values]))
  } else {
    NA
  }
}


air_consec <- air_dec %>%
   group_by(Year = format(Date, format = "%Y")) %>%
      summarise(ConsecutivePolar = fx(Air))

tail(air_consec)
```


So, was it the length of the cold snap that made December so cold?: So far, no! The cold snap (up to December 31) was only `r air_consec$ConsecutivePolar[nrow(air_consec)]` days long. Since 1950, `r sum(air_consec$ConsecutivePolar > air_consec$ConsecutivePolar[air_consec$Year == 2017], na.rm = TRUE)` Decembers showed cold snaps that were longer than the 2017 one and `r sum(air_consec$ConsecutivePolar <= air_consec$ConsecutivePolar[air_consec$Year == 2017], na.rm = TRUE)` Decembers showed maximum cold snaps that were as long or shorter. It looks like the length of our snap is pretty near the middle.^[Note, this only considers December (no SSC data for January yet), so this is definitely subject to change, since the cold snap was limited to the end of December and start of January. At this point, one conclusion to draw from this is that long cold snaps typically start earlier in December than the 2017/18 one.] This is interesting to compare to the Decembers with the longest cold snaps (which also happen to include the most recent colder Decembers: 

1. Year `r air_consec$Year[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar))])]`, length of cold snap: `r air_consec$ConsecutivePolar[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar))])]`
2. Year `r air_consec$Year[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar)) - 1])]`, length of cold snap: `r air_consec$ConsecutivePolar[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar)) - 1])]` 
3. Year `r air_consec$Year[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar)) - 2])]`, length of cold snap: `r air_consec$ConsecutivePolar[which(air_consec$ConsecutivePolar == sort(air_consec$ConsecutivePolar)[sum(!is.na(air_consec$ConsecutivePolar)) - 2])]`

December 2017 was ranked highly in terms of how cold it got, even with a relatively short period of arctic air in the month. 

## Conclusions

From what I can tell, it wasn't a case of the origin or length of the December cold snap that was anomalous.^[Again, I was just looking at December air masses here, this is likely to change when we consider the January days; there are also other ways to think about the length of the cold snap e.g. number of consecutive days that are more than $x$ degrees below the baseline, that I have not considered here.] Instead, we just got some very, very cold temperatures that were between 6 and 16 °C colder than the temperatures that we would typically expect for the period from, December 21 to January 9, with the strongest anomalies from December 26 to around January 6th. Note, however, that other scientists have reached the [opposite conclusion](https://phys.org/news/2018-01-science-big-chill-warmer-world.html) (that the length was more remarkable than the temperature). [Another group](http://www.cbc.ca/news/technology/cold-climate-change-1.4482497) directly contradicts that previous article. The debate rages on!

---
_This post was compiled on `r Sys.time()`. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below._

<details>
  <summary>Packages Used in this post</summary>
```{r}
sessioninfo::package_info(dependencies = "Depends")
```
</details>

## References
