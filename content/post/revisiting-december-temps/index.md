---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Revisiting December Temperatures at Toronto"
subtitle: ""
slug: "revisiting-december-temps"
summary: "A brief, superficial analysis of winter temperatures during the coldsnap of December 2017 and January 2018."
authors: []
categories:
  - R
tags:
  - cold
  - temperature
  - Toronto
date: 2018-01-11
lastmod: 2018-01-12
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
rmd_hash: 8d4a3d104fe81ef4

---

Introduction
------------

In Anderson and Gough ([2017](#ref-Anderson_2017_Evolution)), Bill Gough and I examined winter temperatures in Toronto, with a special focus on winters 2013/14 and 2014/15. With the recent cold snap, I thought it would be interesting to re-examine the subject. This will all be done in R, with the full code included in this blog post.

First, I will load the libraries that I will use.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/ConorIA/canadaHCDx/'>canadaHCDx</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://ggplot2.tidyverse.org'>ggplot2</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://readr.tidyverse.org'>readr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tibble.tidyverse.org/'>tibble</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tidyr.tidyverse.org'>tidyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://zoo.R-Forge.R-project.org/'>zoo</a></span><span class='o'>)</span>
</code></pre>

</div>

Now I get the Toronto temperature data:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Toronto changed station code afer June 2003, so we'll merge it with the new one. </span>
<span class='nv'>tor</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_daily.html'>hcd_daily</a></span><span class='o'>(</span><span class='m'>5051</span>, <span class='m'>1840</span><span class='o'>:</span><span class='m'>2003</span><span class='o'>)</span>
<span class='nv'>tmp</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_daily.html'>hcd_daily</a></span><span class='o'>(</span><span class='m'>31688</span>, <span class='m'>2003</span><span class='o'>:</span><span class='m'>2018</span><span class='o'>)</span>
<span class='nv'>tor</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nv'>tor</span><span class='o'>[</span><span class='m'>1</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tor</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2003-06-30"</span><span class='o'>)</span>,<span class='o'>]</span>, <span class='nv'>tmp</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tmp</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2003-07-01"</span><span class='o'>)</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tmp</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2018-01-31"</span><span class='o'>)</span>,<span class='o'>]</span><span class='o'>)</span>
</code></pre>

</div>

Plots
-----

Let's first see what the temperature looked like. First, I prepare some "plot data", extracting December 01 to January 09 (last day of available data so far in 2018).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>plot_data</span> <span class='o'>&lt;-</span> <span class='nv'>tor</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Date</span> <span class='o'>&gt;=</span> <span class='s'>"2017-12-01"</span> <span class='o'>&amp;</span> <span class='nv'>Date</span> <span class='o'>&lt;=</span> <span class='s'>"2018-01-09"</span><span class='o'>)</span>
</code></pre>

</div>

Let's see what that looks like:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>plot_data</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmax"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MinTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmin"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_hline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>yintercept <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>labs</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>"Date"</span>, y <span class='o'>=</span> <span class='s'>"Temperature (°C)"</span>, color <span class='o'>=</span> <span class='s'>"Variable"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Temperature at Toronto (St. George Campus) 2017-12-01 to 2018-01-09"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggtheme.html'>theme_light</a></span><span class='o'>(</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-3-1.png" width="700px" style="display: block; margin: auto;" />

</div>

We can see that the beginning of December was fairly mild, with an additional mild period around the 18th to 20th before plummeting in the last days of 2017 and beginning of 2018.

Comparison to daily "normals"
-----------------------------

We can't say for sure that weather is anomalous, unless we compare it to normalized data. Let's see how December and January look, by plotting the "normal" temperature, i.e. the 30-year average for the same period for each day from 1981--2010.

First, I will generate daily normals by averaging each calendar date over the past 30 years. Note that I am not sure whether this is sloppy methodology, but thought I'd give it a shot.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_norm</span> <span class='o'>&lt;-</span> <span class='nv'>tor</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%Y"</span><span class='o'>)</span> <span class='o'>&gt;=</span> <span class='m'>1981</span> <span class='o'>&amp;</span> <span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%Y"</span><span class='o'>)</span> <span class='o'>&lt;=</span> <span class='m'>2010</span><span class='o'>)</span>
<span class='nv'>tor_norm</span> <span class='o'>&lt;-</span> <span class='nv'>tor_norm</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Day <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%m-%d"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>mean</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>Date</span><span class='o'>)</span>
</code></pre>

</div>

Now that I have the 30-year daily average temperature for each day of the year, I'll extract just December 1 to January 9. I'll plot these with a loess curve and use the values from that curve to represent my "typical" temperature.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_typical</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nv'>tor_norm</span><span class='o'>[</span><span class='nv'>tor_norm</span><span class='o'>$</span><span class='nv'>Day</span> <span class='o'>&gt;=</span> <span class='s'>"12-01"</span>,<span class='o'>]</span>, <span class='nv'>tor_norm</span><span class='o'>[</span><span class='nv'>tor_norm</span><span class='o'>$</span><span class='nv'>Day</span> <span class='o'>&lt;=</span> <span class='s'>"01-09"</span>,<span class='o'>]</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>Index <span class='o'>=</span> <span class='m'>1</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>MaxSmooth <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/predict.html'>predict</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/stats/loess.html'>loess</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span><span class='o'>~</span><span class='nv'>Index</span>, <span class='nv'>.</span><span class='o'>)</span>, <span class='nv'>.</span><span class='o'>$</span><span class='nv'>Index</span><span class='o'>)</span>, MinSmooth <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/predict.html'>predict</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/stats/loess.html'>loess</a></span><span class='o'>(</span><span class='nv'>MinTemp</span><span class='o'>~</span><span class='nv'>Index</span>, <span class='nv'>.</span><span class='o'>)</span>, <span class='nv'>.</span><span class='o'>$</span><span class='nv'>Index</span><span class='o'>)</span><span class='o'>)</span> 

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>tor_typical</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>40</span>, <span class='nv'>MaxTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmax"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_smooth.html'>geom_smooth</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>40</span>, <span class='nv'>MaxTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmax"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>40</span>, <span class='nv'>MinTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmin"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_smooth.html'>geom_smooth</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>40</span>, <span class='nv'>MinTemp</span>, col <span class='o'>=</span> <span class='s'>"Tmin"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_hline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>yintercept <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>labs</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>"Day"</span>, y <span class='o'>=</span> <span class='s'>"Temperature (°C)"</span>, color <span class='o'>=</span> <span class='s'>"Variable"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"\"Typical\" Temperature at Toronto (St. George Campus) 12-01 to 01-09"</span>, subtitle <span class='o'>=</span> <span class='s'>"Baseline period from 1981 to 2010; smoothed with loess curve"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggtheme.html'>theme_light</a></span><span class='o'>(</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-5-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Now let's compare our temperature with the "typical".

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>plot_data</span> <span class='o'>&lt;-</span> <span class='nv'>plot_data</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>MaxTempAnom <span class='o'>=</span> <span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>MaxTemp</span> <span class='o'>-</span> <span class='nv'>tor_typical</span><span class='o'>$</span><span class='nv'>MaxSmooth</span><span class='o'>)</span>, MinTempAnom <span class='o'>=</span> <span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>MinTemp</span> <span class='o'>-</span> <span class='nv'>tor_typical</span><span class='o'>$</span><span class='nv'>MinSmooth</span><span class='o'>)</span><span class='o'>)</span>

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>plot_data</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTempAnom</span>, col <span class='o'>=</span> <span class='s'>"Tmax"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MinTempAnom</span>, col <span class='o'>=</span> <span class='s'>"Tmin"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_hline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>yintercept <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span><span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>labs</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>"Day"</span>, y <span class='o'>=</span> <span class='s'>"Temperature Anomaly (°C)"</span>, color <span class='o'>=</span> <span class='s'>"Variable"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Temperature Anomaly at Toronto 2017-12-01 to 2018-01-09"</span>, subtitle <span class='o'>=</span> <span class='s'>"Relative to smoothed 1981-2010 baseline"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggtheme.html'>theme_light</a></span><span class='o'>(</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-6-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Indeed, temperatures were cold, getting to be as much as 16 °C colder than the smoothed average daily temperature from 1981--2010.

How does December rank?
-----------------------

Now let's prepare some December-only data series at the daily and monthly timescales.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_dec</span> <span class='o'>&lt;-</span> <span class='nv'>tor</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%m"</span><span class='o'>)</span> <span class='o'>==</span> <span class='m'>12</span><span class='o'>)</span>
<span class='nv'>tor_dec_mon</span> <span class='o'>&lt;-</span> <span class='nv'>tor_dec</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Yearmon <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%Y-%m"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>mean</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
</code></pre>

</div>

Note, I have become largely familiar with the Toronto data series, and I know that missing data is not an issue in this time series. Generally, one would need to do a quick check of the data, such as:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/canadaHCDx/man/quick_audit.html'>quick_audit</a></span><span class='o'>(</span><span class='nv'>tor_dec</span>, <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"MaxTemp"</span>, <span class='s'>"MinTemp"</span>, <span class='s'>"MeanTemp"</span><span class='o'>)</span>, by <span class='o'>=</span> <span class='s'>"month"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 178 x 6</span></span>
<span class='c'>#&gt;    Year  Month `Year-month` `MaxTemp pct NA` `MinTemp pct NA` `MeanTemp pct NA`</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;yearmon&gt;</span><span>               </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>            </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>             </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span> 1840  12    Dec 1840                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span> 1841  12    Dec 1841                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span> 1842  12    Dec 1842                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span> 1843  12    Dec 1843                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span> 1844  12    Dec 1844                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span> 1845  12    Dec 1845                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span> 1846  12    Dec 1846                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span> 1847  12    Dec 1847                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span> 1848  12    Dec 1848                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span> 1849  12    Dec 1849                    0                0                 0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 168 more rows</span></span>
</code></pre>

</div>

OK, let's look at some monthly ranks!

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_dec_mon</span> <span class='o'>&lt;-</span> <span class='nv'>tor_dec_mon</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>MaxTempRank <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/ranking.html'>min_rank</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>MaxTemp</span><span class='o'>)</span>, MinTempRank <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/ranking.html'>min_rank</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>MinTemp</span><span class='o'>)</span>, MeanTempRank <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/ranking.html'>min_rank</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>$</span><span class='nv'>MeanTemp</span><span class='o'>)</span><span class='o'>)</span>

<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>tail</a></span><span class='o'>(</span><span class='nv'>tor_dec_mon</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 8</span></span>
<span class='c'>#&gt;   Yearmon Date       MaxTemp MinTemp MeanTemp MaxTempRank MinTempRank</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2012-12 2012-12-16   4.76   -</span><span style='color: #BB0000;'>0.827</span><span>    1.94          171         173</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2013-12 2013-12-16  -</span><span style='color: #BB0000;'>0.567</span><span>  -</span><span style='color: #BB0000;'>5.60</span><span>    -</span><span style='color: #BB0000;'>3.24</span><span>           45          81</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2014-12 2014-12-16   3.25   -</span><span style='color: #BB0000;'>1.57</span><span>     0.857         148         163</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2015-12 2015-12-16   7.57    2.52     5.06          178         178</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2016-12 2016-12-16   2.23   -</span><span style='color: #BB0000;'>2.97</span><span>    -</span><span style='color: #BB0000;'>0.387</span><span>         125         139</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2017-12 2017-12-16  -</span><span style='color: #BB0000;'>0.845</span><span>  -</span><span style='color: #BB0000;'>6.94</span><span>    -</span><span style='color: #BB0000;'>3.91</span><span>           36          46</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 1 more variable: MeanTempRank </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
</code></pre>

</div>

Toronto's temperature in the month of December 2017 was ranked 36 for $T_{max}$ (-0.845 °C with a return interval (RI) of 4.97) , 46 for $T_{min}$ (-6.94 °C with RI of 3.89), and 40 for $T_{mean}$ (-3.91 °C with RI of 4.47). All three variables are ranked out of 178 total Decembers in the set.

The last time that we saw a December this cold was in 2000-12. The temperature that year was:

-   $T_{max}$: -2.11 °C, rank 12, with RI of 14.9
-   $T_{min}$: -7.73 °C, rank 34, with RI of 5.26
-   $T_{mean}$: -4.94 °C, rank 24, with RI of 7.46

The second-last December that was as cold or colder than December 2017 was 1989-12. That year the temperature reached:

-   $T_{max}$: -4.63 °C, rank 1, with RI of 179
-   $T_{min}$: -10.9 °C, rank 3, with RI of 59.7
-   $T_{mean}$: -7.79 °C, rank 3, with RI of 59.7

Air Mass Analysis
-----------------

One of the things that we looked at in our 2017 paper was the position of the jet stream. During December 2017, we know that the jet stream was to the south of Toronto during the cold snap. Let's look at this, using data from Sheridan's [Spatial Synoptic Classification (SSC)](http://sheridan.geog.kent.edu/ssc.html) (Sheridan [2002](#ref-Sheridan_2002_redevelopment)).

First, I'll get the data and bin it into four bins, as per Anderson and Gough ([2017](#ref-Anderson_2017_Evolution)):

1.  'cool': SSC codes 2 (dry polar) and 5 (moist polar)
2.  'moderate': SSC codes 1 (dry moderate) and 4 (moist moderate)
3.  'warm': SSC codes 3 (dry tropical), 6 (moist tropical), 66 (moist tropical plus), and 67 (moist tropical double plus)
4.  'transition': SCC code 7 (transition)

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>f</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/tempfile.html'>tempfile</a></span><span class='o'>(</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/download.file.html'>download.file</a></span><span class='o'>(</span><span class='s'>"http://sheridan.geog.kent.edu/ssc/files/YYZ.dbdmt"</span>, <span class='nv'>f</span>, quiet <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
<span class='nv'>air</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://readr.tidyverse.org/reference/read_table.html'>read_table</a></span><span class='o'>(</span><span class='nv'>f</span>, col_names <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Station"</span>, <span class='s'>"Date"</span>, <span class='s'>"Air"</span><span class='o'>)</span>, col_types <span class='o'>=</span> <span class='nf'><a href='https://readr.tidyverse.org/reference/cols.html'>cols</a></span><span class='o'>(</span><span class='nf'><a href='https://readr.tidyverse.org/reference/parse_atomic.html'>col_character</a></span><span class='o'>(</span><span class='o'>)</span>, <span class='nf'><a href='https://readr.tidyverse.org/reference/parse_datetime.html'>col_date</a></span><span class='o'>(</span>format <span class='o'>=</span> <span class='s'>"%Y%m%d"</span><span class='o'>)</span>, <span class='nf'><a href='https://readr.tidyverse.org/reference/parse_factor.html'>col_factor</a></span><span class='o'>(</span>levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>4</span>, <span class='m'>5</span>, <span class='m'>6</span>, <span class='m'>7</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='c'># Note, I am ignoring code 8 because that is the SSC2 "NA" value, which is turned to NA by ignoring.</span>
<span class='nf'><a href='https://rdrr.io/r/base/levels.html'>levels</a></span><span class='o'>(</span><span class='nv'>air</span><span class='o'>$</span><span class='nv'>Air</span><span class='o'>)</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>cool <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>5</span><span class='o'>)</span>, mod <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>4</span><span class='o'>)</span>, warm <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>3</span>, <span class='m'>6</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span>, trans <span class='o'>=</span> <span class='s'>"7"</span><span class='o'>)</span>
</code></pre>

</div>

Now I will rearrange so that I get the proportions of each.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Some hints from https://stackoverflow.com/questions/25811756/summarizing-counts-of-a-factor-with-dplyr</span>

<span class='nv'>air_dec</span> <span class='o'>&lt;-</span> <span class='nv'>air</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%m"</span><span class='o'>)</span> <span class='o'>==</span> <span class='m'>12</span><span class='o'>)</span>
<span class='nv'>air_tally</span> <span class='o'>&lt;-</span> <span class='nv'>air_dec</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>Station</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Yearmon <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span>, <span class='nv'>Air</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nv'>tally</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/spread.html'>spread</a></span><span class='o'>(</span><span class='nv'>Air</span>, <span class='nv'>n</span>, fill <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span>
<span class='nv'>air_tally</span> <span class='o'>&lt;-</span> <span class='nv'>air_tally</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>`&lt;NA&gt;`</span> <span class='o'>&lt;</span> <span class='m'>31</span><span class='o'>)</span> <span class='c'># Remove empty year-months</span>
<span class='nv'>air_prop</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/as_tibble.html'>as_tibble</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/proportions.html'>prop.table</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/matrix.html'>as.matrix</a></span><span class='o'>(</span><span class='nv'>air_tally</span><span class='o'>[</span>,<span class='m'>2</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>ncol</a></span><span class='o'>(</span><span class='nv'>air_tally</span><span class='o'>)</span><span class='o'>]</span><span class='o'>)</span>, <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>air_prop</span><span class='o'>)</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>air_prop</span><span class='o'>)</span>, <span class='s'>"prop"</span>, sep <span class='o'>=</span> <span class='s'>"_"</span><span class='o'>)</span>
<span class='nv'>air_prop</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_cols</a></span><span class='o'>(</span><span class='nv'>air_tally</span>, <span class='nv'>air_prop</span><span class='o'>)</span>
</code></pre>

</div>

The last few Decembers, for instance:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/head.html'>tail</a></span><span class='o'>(</span><span class='nv'>air_prop</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='m'>2</span><span class='o'>:</span><span class='o'>-</span><span class='m'>6</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Yearmon [6]</span></span>
<span class='c'>#&gt;   Yearmon   cool_prop mod_prop warm_prop trans_prop `&lt;NA&gt;_prop`</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;yearmon&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> Dec 2012      0.323    0.581    0.032</span><span style='text-decoration: underline;'>3</span><span>     0.064</span><span style='text-decoration: underline;'>5</span><span>      0     </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> Dec 2013      0.613    0.290    0          0.096</span><span style='text-decoration: underline;'>8</span><span>      0     </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> Dec 2014      0.484    0.355    0.064</span><span style='text-decoration: underline;'>5</span><span>     0.096</span><span style='text-decoration: underline;'>8</span><span>      0     </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> Dec 2015      0.161    0.613    0.129      0.096</span><span style='text-decoration: underline;'>8</span><span>      0     </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> Dec 2016      0.548    0.323    0          0.096</span><span style='text-decoration: underline;'>8</span><span>      0.032</span><span style='text-decoration: underline;'>3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> Dec 2017      0.548    0.323    0          0.129       0</span></span>
</code></pre>

</div>

So, was it just the origin of the air that made December so cold? Not exactly. on average 57.1% of daily December air masses are of polar origin. We had 54.8% air of polar origin in December 2017.

Let's take a look at the continuity of the polar air. Here I will look for the longest continuous series of 'cool' or 'trans' air for each December, omitting any missing values. Therefore, for this analysis, a cold snap is the longest period of two or more consecutive days of polar or transitional air during each December.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Took some code from https://stackoverflow.com/questions/41254014/consecutive-count-by-groups</span>

<span class='nv'>fx</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nv'>x</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/stats/na.fail.html'>na.omit</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span>
  <span class='kr'>if</span> <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>&gt;</span> <span class='m'>0</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='nf'><a href='https://rdrr.io/r/base/with.html'>with</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>x</span> <span class='o'>==</span> <span class='s'>"cool"</span> <span class='o'>|</span> <span class='nv'>x</span> <span class='o'>==</span> <span class='s'>"trans"</span><span class='o'>)</span>, <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>values</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
  <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
    <span class='kc'>NA</span>
  <span class='o'>}</span>
<span class='o'>}</span>


<span class='nv'>air_consec</span> <span class='o'>&lt;-</span> <span class='nv'>air_dec</span> <span class='o'>%&gt;%</span>
   <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Year <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/format.html'>format</a></span><span class='o'>(</span><span class='nv'>Date</span>, format <span class='o'>=</span> <span class='s'>"%Y"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
      <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarise</a></span><span class='o'>(</span>ConsecutivePolar <span class='o'>=</span> <span class='nf'>fx</span><span class='o'>(</span><span class='nv'>Air</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>


<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>tail</a></span><span class='o'>(</span><span class='nv'>air_consec</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 2</span></span>
<span class='c'>#&gt;   Year  ConsecutivePolar</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>            </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2012                 5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2013                13</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2014                 5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2015                 2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2016                12</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2017                11</span></span>
</code></pre>

</div>

So, was it the length of the cold snap that made December so cold?: So far, no! The cold snap (up to December 31) was only 11 days long. Since 1950, 25 Decembers showed cold snaps that were longer than the 2017 one and 39 Decembers showed maximum cold snaps that were as long or shorter. It looks like the length of our snap is pretty near the middle.[^1] This is interesting to compare to the Decembers with the longest cold snaps (which also happen to include the most recent colder Decembers:

1.  Year 1989, length of cold snap: 30
2.  Year 1950, length of cold snap: 28
3.  Year 2000, length of cold snap: 27

December 2017 was ranked highly in terms of how cold it got, even with a relatively short period of arctic air in the month.

Conclusions
-----------

From what I can tell, it wasn't a case of the origin or length of the December cold snap that was anomalous.[^2] Instead, we just got some very, very cold temperatures that were between 6 and 16 °C colder than the temperatures that we would typically expect for the period from, December 21 to January 9, with the strongest anomalies from December 26 to around January 6th. Note, however, that other scientists have reached the [opposite conclusion](https://phys.org/news/2018-01-science-big-chill-warmer-world.html) (that the length was more remarkable than the temperature). [Another group](http://www.cbc.ca/news/technology/cold-climate-change-1.4482497) directly contradicts that previous article. The debate rages on!

------------------------------------------------------------------------

*This post was compiled on 2020-10-09 11:29:27. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package     * version    date       lib source                             </span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  canadaHCD   * 0.0-2      2020-10-01 [1] Github (ConorIA/canadaHCD@e2f4d2b) </span>
<span class='c'>#&gt;  canadaHCDx  * 0.0.8      2020-10-01 [1] gitlab (ConorIA/canadaHCDx@0f99419)</span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  downlit       0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  dplyr       * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fs            1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  geosphere     1.5-10     2019-05-26 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  glue          1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)    </span>
<span class='c'>#&gt;  knitr         1.30       2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  labeling      0.3        2014-08-23 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  Matrix        1.2-18     2019-11-27 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  mgcv          1.8-33     2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  nlme          3.1-149    2020-08-23 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  pillar        1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  purrr         0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  rappdirs      0.3.1      2016-03-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  Rcpp          1.0.5      2020-07-06 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  readr       * 1.3.1      2018-12-21 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rlang         0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rmarkdown     2.3        2020-06-18 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sp            1.4-2      2020-05-20 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  storr         1.2.1      2018-10-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  stringi       1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  stringr       1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  tibble      * 3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyr       * 1.1.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  vctrs         0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  withr         2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  xfun          0.18       2020-09-29 [2] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  zoo         * 1.8-8      2020-05-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

References
----------

<div id="refs" class="references">

<div id="ref-Anderson_2017_Evolution">

Anderson, Conor I., and William A. Gough. 2017. "Evolution of Winter Temperature in Toronto, Ontario, Canada: A Case Study of Winters 2013/14 and 2014/15." *Journal of Climate* 30 (14): 5361--76. <https://doi.org/10.1175/JCLI-D-16-0562.1>.

</div>

<div id="ref-Sheridan_2002_redevelopment">

Sheridan, Scott C. 2002. "The Redevelopment of a Weather-Type Classification Scheme for North America." *International Journal of Climatology* 22 (1): 51--68. <https://doi.org/10.1002/joc.709>.

</div>

</div>

[^1]: Note, this only considers December (no SSC data for January yet), so this is definitely subject to change, since the cold snap was limited to the end of December and start of January. At this point, one conclusion to draw from this is that long cold snaps typically start earlier in December than the 2017/18 one.

[^2]: Again, I was just looking at December air masses here, this is likely to change when we consider the January days; there are also other ways to think about the length of the cold snap e.g. number of consecutive days that are more than $x$ degrees below the baseline, that I have not considered here.

