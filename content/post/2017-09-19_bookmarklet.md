---
date: 2017-09-19
lastmod: 2018-01-04
tags:
title: Bookmarklet for the UofT Library Get it! Service
summary: An easy bookmarket to get you to a UofT login on a publisher's page. 
---

If you are anything like me, you might click onto an interesting article from ResearchGate or a Google Scholar Alert when you are at home, only to find yourself at a publisher paywall, with no easy way to login to the UofT [MyAccess (a.k.a. "Get it! UTL")](https://onesearch.library.utoronto.ca/faq/what-myaccess) service. Instead of returning to the library webpage or to Google Scholar to tweak your libraries preferences, you can easily get the paper by sticking the URL into the MyAccess proxy service^[Note that the proxy will often return a 404 error if you are not on a known publisher page.]. To make this easier, just drag the bookmarklet, below, to your bookmarks toolbar. Then you will be able to click the bookmarklet to instantly be taken to the MyAccess login page.

<center>
<a href="javascript:(function(){var%20url=document.URL;url=url.split(&quot;/&quot;);var%20path=&quot;&quot;;for(i=3;i&lt;url.length;i++){path+=&quot;/&quot;+url[i];}target=&quot;http://&quot;+url[2]+&quot;.myaccess.library.utoronto.ca&quot;+path;window.open(target,&quot;_self&quot;);})();">Get it! UTL</a>
</center>

If you can't drag and drop the bookmarklet, create a new bookmark, and use the following as the URL:

```javascript
javascript:(function(){var%20url=document.URL;url=url.split("/");var%20path="";for(i=3;i<url.length;i++){path+="/"+url[i];}target="http://"+url[2]+".myaccess.library.utoronto.ca"+path;window.open(target,"_self");})();
``` 

---

__UPDATE:__ As luck would have it, the fine folks at the UofT library [beat me to the punch](https://guides.library.utoronto.ca/proxy) a long time ago.

<center>
<a href="javascript:void(location.href=%22http://myaccess.library.utoronto.ca/login?url=%22+location.href);">Get it! UTL (official)</a>
</center>

The "official" bookmarklet code is as simple as: 

```javascript
javascript:void(location.href=%22http://myaccess.library.utoronto.ca/login?url=%22+location.href)
```

As it turns out, all of the URL splitting and re-arranging in my bookmarket is handled by the proxy server itself!
