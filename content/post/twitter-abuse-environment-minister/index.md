---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Twitter abuse of Canada's Environment Minister, Catherine McKenna
subtitle: ""
slug: twitter-abuse-environment-minister
summary: "An analysis of hate and anti-female slurs against Catherine McKenna"
authors: []
categories:
  - R
tags:
  - sentiment analysis
  - Twitter
  - politics
date: 2019-10-25
lastmod: 2019-10-29
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
rmd_hash: 02f4f4d2f7e65795

---

On September 8 it was [reported](https://www.nationalobserver.com/2019/09/08/news/threats-abuse-move-online-real-world-mckenna-now-requires-security) that Catherine McKenna now requires IRL security after enduring years of escalating abuse on Twitter. I, of course, was perturbed by the vitriol that I had seen directed at the minister, but I had largely dismissed it as part and parcel of Twitter. Twitter is a strange microcosm of some of the worst elements of politics---a sort of strange other world where earnest human tweeters, bot networks, and other shadowy figures all share "thoughts"; sometimes it is hard to tell who is sincere, who is a troll, and who is nothing more than a couple of lines of code on a server somewhere. As a climate scientist, I am always interested to see how social media platforms amplify climate change denial, or push regressive climate (in)action policy positions. I thought that Catherine McKenna's Twitter world would be a great case study, and I wanted to see if there was any specific milestone that marked a significant shift in the tone of Tweets that are directed at her.

Usually when I create a write-up like this, I share the full methodology, but today I am going to omit the specific lines of code that were used to obtain the Tweet data that I analyzed. Twitter has an official API, which allows free access to the last 9 days of history. Paid access will allow access to the full Twitter archive. There are myriad tutorials online to help use the API. An alternative option is to script interactions between a web browser and Twitter. Programming a browser to visit Twitter automatically is probably [ethical](https://perrystephenson.me/2018/08/11/is-it-okay-to-scrape-twitter/), but very likely in contravention of Twitter ToS, so be cautious if you want to undertake an analysis of that sort.

For this analysis, I will be using an object called `tweets` that contains five columns: the timestamp the tweet was sent (`timstamp`); the Status ID of the top tweet in the thread (`reply_to`), the Status ID of each individual Tweet (`twid`), the username of the tweeter (`user`), and the Tweet text (`tweet`).

Let's start by loading the libraries that we will use through this analysis.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://ggplot2.tidyverse.org'>ggplot2</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://readr.tidyverse.org'>readr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://stringr.tidyverse.org'>stringr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tibble.tidyverse.org/'>tibble</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tidyr.tidyverse.org'>tidyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/juliasilge/tidytext'>tidytext</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/lchiffon/wordcloud2'>wordcloud2</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://zoo.R-Forge.R-project.org/'>zoo</a></span><span class='o'>)</span>
</code></pre>

</div>

Now I will clean up some duplicate entries in my data and filter to a fixed range of dates.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/distinct.html'>distinct</a></span><span class='o'>(</span><span class='nv'>twid</span>, .keep_all <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span> <span class='o'>&gt;=</span> <span class='s'>"2015-11-04"</span> <span class='o'>&amp;</span> 
         <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span> <span class='o'>&lt;=</span> <span class='s'>"2019-09-08"</span><span class='o'>)</span><span class='o'>-&gt;</span> <span class='nv'>tweets</span>
</code></pre>

</div>

I will only look at Tweets between November 4, 2015 (the day that McKenna assumed office as Environment Minister), and September 8, 2019 (the day it was reported that she required security). The dataset I used contained 247226 Tweets in 12989 threads. Catherine McKenna wrote 13308 of the Tweets, and was, of course, the first tweeter in each thread. I should note that this analysis only includes *replies*, and does not include unthreaded Tweets direct to @cathmckenna or any direct messages she may receive.

Let's first look at the raw breakdown of Tweet volume by day.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>==</span> <span class='s'>"cathmckenna"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Day <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Day</span> <span class='o'>&gt;=</span> <span class='s'>"2015-11-04"</span> <span class='o'>&amp;</span> <span class='nv'>Day</span> <span class='o'>&lt;=</span> <span class='s'>"2019-09-08"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>Day</span>, y <span class='o'>=</span> <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"#34495E"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Tweets authored by @cathmckenna, by day"</span>,
          subtitle <span class='o'>=</span> <span class='s'>"Nov. 4, 2015 to Sep. 8, 2019"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-3-1.png" width="700px" style="display: block; margin: auto;" />

</div>

McKenna is a heavy Twitter user, and we can see some spikes on which she sent a very high volume of Tweets. Her biggest day was on June 5, 2018, World Environment Day, followed by February 13, 2019, when McKenna released a series of Tweets defending the federal carbon tax (to coincide with Saskatchewan's court challenge to the tax's constitutionality). Her third biggest day was from the 72nd UN General Assembly on September 19, 2019.

Now let's see the reply volume by day.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>!=</span> <span class='s'>"cathmckenna"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Day <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Day</span> <span class='o'>&gt;=</span> <span class='s'>"2015-11-04"</span> <span class='o'>&amp;</span> <span class='nv'>Day</span> <span class='o'>&lt;=</span> <span class='s'>"2019-09-08"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>Day</span>, y <span class='o'>=</span> <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span>xintercept <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='s'>"2019-04-01"</span><span class='o'>)</span>, colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"#34495E"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"@cathmckenna reply volume, by day"</span>,
          subtitle <span class='o'>=</span> <span class='s'>"Nov. 4, 2015 to Sep. 8, 2019"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-4-1.png" width="700px" style="display: block; margin: auto;" />

</div>

By far the highest volume of Tweets directed to McKenna was sent on April 1, 2019 (marked with a dash red line). April 1 was the day the federal carbon tax took effect in Ontario, Saskatchewan, Manitoba and New Brunswick. Other high-volume days were April 25, April 15, May 16, and September 8. I didn't find any standout issue on April 15, 25 or May 16. September 8 seems to have been a mix of McKenna's defenders ("can't believe you have to go through this") and abusers ("playing the victim card") reacting to her announcement that she required a security detail. Later in this notebook, I will show the Tweets from McKenna that garnered the most replies.

Let's first take a look a who the most prolific tweeters are on Cath McKenna's statuses.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>user</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>!=</span> <span class='s'>"cathmckenna"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>tweet_volume</span>
<span class='nv'>tweet_volume</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 28,453 x 2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   user [28,453]</span></span>
<span class='c'>#&gt;    user                n</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>           </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span> kgaider          </span><span style='text-decoration: underline;'>2</span><span>364</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span> FriendsOScience  </span><span style='text-decoration: underline;'>1</span><span>091</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span> erwingerrits      927</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span> bo_canut          867</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span> Darrylrides       826</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span> isma_fan          779</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span> FraserMacLeod5    772</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span> NecktopP          671</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span> OldWood007        650</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span> stan15537715      632</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 28,443 more rows</span></span>
</code></pre>

</div>

Kathy Gaider (@kgaider) is the most prolific tweeter by far. At the time of writing, Gaider's account was suspended, but the snippet of her bio from a [DuckDuckGo search](https://duckduckgo.com/?q=%40kgaider+site%3Atwitter.com) reveals a strong anti-Liberal bent and an assertion of holding "three real science degrees". So take that for what its worth. Twitter users informed me that Kathy was (or claimed to be) an ex-Environment Canada employee who unleashed a series of ad-hominen attacks on Trudeau and McKenna. Gaider's supporters claim that her account was suspended after she revealed data manipulation by Environment Canada. I reached out to a contact at ECCC to see if he had heard about this, but he was on vacation when I got in touch. At any rate, I have found no evidence (nor do I believe) that Environment Canada engaged in any manipulation of data. Data is often corrected to account for measuring errors or other systematic quality control issues. Snopes even has a write up on [the topic](https://www.snopes.com/fact-check/global-warming-data-faked/).

Number two on the list are the mind-numbingly oxymoronic climate change denial crusaders behind the "Friends of Science" moniker. FoS are known climate denial trolls and are wilfully ignorant of modern climate science. The members of the group remain anonymous, but they have been [linked](https://www.desmogblog.com/friends-of-science) to fossil fuel interests.

User number 3, @, is another anti-Liberal Twitter activist, judging from his profile photo at the time of writing: a Liberal "L" logo embedded in the word "Liars". I don't want to explore all of the above users just now, but I am sure that many more on the list may similar anti-Liberal biases. I think it is a safe assumption to say that Catherine McKenna isn't inundated with loving Tweets.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweet_volume</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>user</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>5</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>top_tweeters</span>

<span class='nv'>tweets</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>%in%</span> <span class='nv'>top_tweeters</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>user</span>, month <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>month</span>, y <span class='o'>=</span> <span class='nv'>n</span>, fill <span class='o'>=</span> <span class='nv'>user</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_bar.html'>geom_col</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Top five most prolific tweeters in replies directed to @cathmckenna"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-6-1.png" width="700px" style="display: block; margin: auto;" />

</div>

We can see a pattern of concentrated effort from 2018 to present. It seems Gaider was either suspended just before April of this year. This is an interesting point, because that means that Gaider didn't contribute to the explosion of Tweets on April 1, 2019, unless it was under another account name.

Tokens
------

What can we learn about the content of these Tweets? Let's perform some quick text mining. I'll start by tokenizing the Tweets. First, I will remove "rt", usernames, punctuation, URLs, and tabs, and then trim whitespace. Finally, I'll remove common stop words in both English and French (McKenna tweets in both languages).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/data.html'>data</a></span><span class='o'>(</span><span class='nv'>stop_words</span><span class='o'>)</span>
<span class='c'># Clean up tweet text</span>
<span class='nv'>tweets</span><span class='o'>$</span><span class='nv'>tweet</span> <span class='o'>%&gt;%</span> <span class='nv'>tolower</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://stringr.tidyverse.org/reference/str_remove.html'>str_remove_all</a></span><span class='o'>(</span>pattern <span class='o'>=</span> <span class='s'>"^rt|@[^\\s]+|https?[^\\s]+|pic\\.twitter\\.com[^\\s]+|[[:punct:]]"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nv'>str_trim</span> <span class='o'>-&gt;</span> <span class='nv'>tweets</span><span class='o'>$</span><span class='nv'>tweet</span>

<span class='c'># unnest tokens and remove stop words</span>
<span class='nv'>tweets</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/tidytext/man/unnest_tokens.html'>unnest_tokens</a></span><span class='o'>(</span><span class='nv'>word</span>, <span class='nv'>tweet</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter-joins.html'>anti_join</a></span><span class='o'>(</span><span class='nv'>stop_words</span>, by <span class='o'>=</span> <span class='s'>"word"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter-joins.html'>anti_join</a></span><span class='o'>(</span>
    <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>
      word <span class='o'>=</span> <span class='nf'>jsonlite</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/jsonlite/man/fromJSON.html'>fromJSON</a></span><span class='o'>(</span>
        <span class='s'>"https://raw.githubusercontent.com/stopwords-iso/stopwords-iso/master/stopwords-iso.json"</span><span class='o'>)</span><span class='o'>$</span><span class='nv'>fr</span><span class='o'>)</span>,
    by <span class='o'>=</span> <span class='s'>"word"</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>tokens</span>
</code></pre>

</div>

Let's see a wordcloud of McKenna's Tweets.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tokens</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>==</span> <span class='s'>"cathmckenna"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='nv'>word</span>, sort <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/wordcloud2/man/wordcloud2.html'>wordcloud2</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<img src="wc1.png" width="700px" style="display: block; margin: auto;" />

</div>

The content here makes sense for a person in her position. We can see words that appeal to her riding, her portfolio, and to Canadians in general.

How about the replies?

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tokens</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>!=</span> <span class='s'>"cathmckenna"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='nv'>word</span>, sort <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/wordcloud2/man/wordcloud2.html'>wordcloud2</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<img src="wc2.png" width="700px" style="display: block; margin: auto;" />

</div>

The content of the replies contains words that focus on the carbon tax, and terms that seem be personal and party references. A number of insults and slurs have a fairly high precedence.

Let's look at the "sentiment"--whether they are positive or negative--of all of the words in the replies. I'll use a sentiment lexicon developed by Nielsen ([2011](#ref-nielsen_2011_new)). This lexicon rates words on a scale of -5 to 5, scored for valence. Not all words will be captured by this lexicon, so this analysis won't capture everything, but will rather look for key words in Tweets. Sentiment analysis is not a perfect science, since it scores words independent of their context, but it is a good way to get a sense of the general tone of the corpus of Tweets.

You can access the AFINN 111 word list in R using [`get_sentiments("afinn")`](https://rdrr.io/pkg/tidytext/man/get_sentiments.html) from the *tidytext* package, but that can't be done non-interactively, so won't work on this blog. I will use JSON to grab AFINN 165, which contains 905 more words than 111.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>afinn</span> <span class='o'>&lt;-</span> <span class='nf'>jsonlite</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/jsonlite/man/fromJSON.html'>fromJSON</a></span><span class='o'>(</span><span class='s'>"https://raw.githubusercontent.com/words/afinn-165/master/index.json"</span><span class='o'>)</span>
<span class='nv'>afinn</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>word <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>afinn</span><span class='o'>)</span>, value <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='nv'>afinn</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>token_sentiment</span> <span class='o'>&lt;-</span> <span class='nv'>tokens</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>full_join</a></span><span class='o'>(</span><span class='nv'>afinn</span>, by <span class='o'>=</span> <span class='s'>"word"</span><span class='o'>)</span>
</code></pre>

</div>

These are the top-10 positive words in the comments:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>token_sentiment</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>value</span> <span class='o'>&gt;</span> <span class='m'>0</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='nv'>word</span>, sort <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>10</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>word</span>, <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_bar.html'>geom_col</a></span><span class='o'>(</span>fill <span class='o'>=</span> <span class='s'>"#2ECC71"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/coord_flip.html'>coord_flip</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Top-10 most frequent positive words"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-14-1.png" width="700px" style="display: block; margin: auto;" />

</div>

These are the top-10 negative words in the comments:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>token_sentiment</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>value</span> <span class='o'>&lt;</span> <span class='m'>0</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='nv'>word</span>, sort <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='m'>10</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>word</span>, <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_bar.html'>geom_col</a></span><span class='o'>(</span>fill <span class='o'>=</span> <span class='s'>"#E74C3C"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/coord_flip.html'>coord_flip</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Top-10 most frequent negative words"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-15-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Tweet sentiment
---------------

Later we can look at word frequencies, but let's go back to using Tweets as a unit. We can get a rough idea of the sentiment of a Tweet by averaging the sentiment of the constituent words in the Tweet.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>token_sentiment</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by_all.html'>group_by_at</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/vars.html'>vars</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>word</span>, <span class='o'>-</span><span class='nv'>value</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>sentiment <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>value</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>right_join</a></span><span class='o'>(</span><span class='nv'>tweets</span>, by <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"timestamp"</span>, <span class='s'>"reply_to"</span>, <span class='s'>"twid"</span>, <span class='s'>"user"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>ungroup</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>tweet_sentiment</span>

<span class='c'>#&gt; `summarise()` regrouping output by 'timestamp', 'reply_to', 'twid' (override with `.groups` argument)</span>
</code></pre>

</div>

Let's see how the average sentiment of Tweets in the Catherine McKenna orbit have changed over time. Unsurprisingly, Cath McKenna is the most positive voice on her own twitter, so let's look at just the replies to her Tweets.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweet_sentiment</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>!=</span> <span class='s'>"cathmckenna"</span> <span class='o'>&amp;</span> <span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>month <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>month</span>, <span class='nv'>user</span>, <span class='nv'>sentiment</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>month</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>positive <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>sentiment</span> <span class='o'>&gt;</span> <span class='m'>0</span><span class='o'>)</span>, negative <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>sentiment</span> <span class='o'>&lt;</span><span class='m'>0</span><span class='o'>)</span>, sentiment <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>monthly_sentiment</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>
</code></pre>

</div>

We can look at the count value of positive and negative Tweets:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>monthly_sentiment</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>month</span> <span class='o'>&gt;</span> <span class='s'>"2015-11"</span> <span class='o'>&amp;</span> <span class='nv'>month</span> <span class='o'>&lt;</span> <span class='s'>"2019-09"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>sentiment</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/pivot_longer.html'>pivot_longer</a></span><span class='o'>(</span>cols <span class='o'>=</span> <span class='m'>2</span><span class='o'>:</span><span class='m'>3</span>, names_to <span class='o'>=</span> <span class='s'>"sentiment"</span>, values_to <span class='o'>=</span> <span class='s'>"n"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>month</span>, <span class='nv'>n</span>, fill <span class='o'>=</span> <span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_ribbon.html'>geom_area</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span>xintercept <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='s'>"2019-04"</span><span class='o'>)</span>, colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Proportion of +/- Tweets directed at @cathmckenna over time"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-18-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Changes in the proportion of positive and negative Tweets:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>monthly_sentiment</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>month</span> <span class='o'>&gt;</span> <span class='s'>"2015-11"</span> <span class='o'>&amp;</span> <span class='nv'>month</span> <span class='o'>&lt;</span> <span class='s'>"2019-09"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>total <span class='o'>=</span> <span class='nv'>positive</span> <span class='o'>+</span> <span class='nv'>negative</span>,
         positive <span class='o'>=</span> <span class='nv'>positive</span> <span class='o'>/</span> <span class='nv'>total</span>,
         negative <span class='o'>=</span> <span class='nv'>negative</span> <span class='o'>/</span> <span class='nv'>total</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>sentiment</span>, <span class='o'>-</span><span class='nv'>total</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/pivot_longer.html'>pivot_longer</a></span><span class='o'>(</span>cols <span class='o'>=</span> <span class='m'>2</span><span class='o'>:</span><span class='m'>3</span>, names_to <span class='o'>=</span> <span class='s'>"sentiment"</span>, values_to <span class='o'>=</span> <span class='s'>"n"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>month</span>, <span class='nv'>n</span>, colour <span class='o'>=</span> <span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Proportion of positive and negative Tweets directed at @cathmckenna over time"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-19-1.png" width="700px" style="display: block; margin: auto;" />

</div>

It is interesting to note that March 2019 was actually the month with the lowest average sentiment. November 2018, the month after the IPCC SR15 report, and the month before COP24, ranked second. April 2019 was the third most negative month.

We can also look at changes in the average sentiment of all replies in a month:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>monthly_sentiment</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>month</span> <span class='o'>&gt;</span> <span class='s'>"2015-11"</span> <span class='o'>&amp;</span> <span class='nv'>month</span> <span class='o'>&lt;</span> <span class='s'>"2019-09"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span><span class='nv'>month</span>, <span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>show.legend <span class='o'>=</span> <span class='kc'>FALSE</span>, color <span class='o'>=</span> <span class='s'>"#E67E22"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_hline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>yintercept <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span>xintercept <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='s'>"2019-04"</span><span class='o'>)</span>, colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Sentiment of replies to @cathmckenna Tweets over time"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-20-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Sentiment of top tweeters
-------------------------

How much do you wager that our top-five high volume tweeters were postive forces for good in the world? They were not.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweet_sentiment</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>user</span> <span class='o'>%in%</span> <span class='nv'>top_tweeters</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>user</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>sentiment <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>sentiment</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 2</span></span>
<span class='c'>#&gt;   user            sentiment</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>               </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> bo_canut           -</span><span style='color: #BB0000;'>0.779</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> Darrylrides        -</span><span style='color: #BB0000;'>0.820</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> erwingerrits       -</span><span style='color: #BB0000;'>0.179</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> FriendsOScience    -</span><span style='color: #BB0000;'>0.565</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> kgaider            -</span><span style='color: #BB0000;'>1.10</span></span>
</code></pre>

</div>

Most replies to a Tweet
-----------------------

How about the Tweets with most buzz?

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweet_sentiment</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>twid</span> <span class='o'>!=</span> <span class='nv'>reply_to</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='c'>#remove the original tweets</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>reply_to</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>replies <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/context.html'>n</a></span><span class='o'>(</span><span class='o'>)</span>, sentiment <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>sentiment</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>replies</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>tweet_replies</span>

</code></pre>
\#\> [`summarise()`](https://dplyr.tidyverse.org/reference/summarise.html) ungrouping output (override with `.groups` argument)
<pre class='chroma'><code class='language-r' data-lang='r'>
<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>tweet_replies</span>, <span class='m'>5</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>left_join</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>tweet_sentiment</span>, <span class='o'>-</span><span class='nv'>reply_to</span>, <span class='o'>-</span><span class='nv'>sentiment</span><span class='o'>)</span>, by <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"reply_to"</span> <span class='o'>=</span> <span class='s'>"twid"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>timestamp</span>, <span class='nv'>reply_to</span>, <span class='nv'>replies</span>, <span class='nv'>tweet</span>, <span class='nv'>sentiment</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/with.html'>with</a></span><span class='o'>(</span><span class='nv'>.</span>, <span class='nf'><a href='https://rdrr.io/r/base/cat.html'>cat</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/sprintf.html'>sprintf</a></span><span class='o'>(</span><span class='s'>"- Tweet [%s](https://twitter.com/cathmckenna/status/%s) sent on %s received %s replies with a mean sentiment of %f\n"</span>, <span class='nv'>reply_to</span>, <span class='nv'>reply_to</span>, <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span>, <span class='nv'>replies</span>, <span class='nv'>sentiment</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>

</code></pre>

-   Tweet [1080144362321760256](https://twitter.com/cathmckenna/status/1080144362321760256) sent on 2019-01-01 received 355 replies with a mean sentiment of -0.581574
-   Tweet [1078367588697161728](https://twitter.com/cathmckenna/status/1078367588697161728) sent on 2018-12-27 received 320 replies with a mean sentiment of -0.328307
-   Tweet [1170338716184928257](https://twitter.com/cathmckenna/status/1170338716184928257) sent on 2019-09-07 received 312 replies with a mean sentiment of -0.706285
-   Tweet [1128796337292677121](https://twitter.com/cathmckenna/status/1128796337292677121) sent on 2019-05-15 received 296 replies with a mean sentiment of -0.846227
-   Tweet [1078285036158361600](https://twitter.com/cathmckenna/status/1078285036158361600) sent on 2018-12-27 received 290 replies with a mean sentiment of -0.594874

</div>

The biggest volume day seems to have come on the same day that the backstop carbon tax was applied in Ontario. In a nasty, if not unexpected turn of events, it seems that the third-most replied-to-Tweet was one from September 7, 2019, in which Catherine McKenna denounced Twitter attacks.

Word frequencies of slurs
-------------------------

Indeed, the fact the Minister McKenna is a female likely has a lot to do with the content of her replies. Let's examine how often anti-female slurs are hurled her way.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># My code here uses an unnecessary regex string to avoid having search engines pick up the words. I am sure that readers who like puzzles will have no trouble figuring out what words I was looking at. </span>

<span class='nv'>token_sentiment</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>value</span> <span class='o'>==</span> <span class='o'>-</span><span class='m'>5</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>word</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unlist.html'>unlist</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://stringr.tidyverse.org/reference/str_extract.html'>str_extract</a></span><span class='o'>(</span><span class='s'>"(bit|sl|c|tw)(u|a|c)(t|nt|h)(es)?"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nv'>.</span><span class='o'>[</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>)</span><span class='o'>]</span> <span class='o'>-&gt;</span> <span class='nv'>slurs</span>

<span class='nv'>tweets</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://stringr.tidyverse.org/reference/str_detect.html'>str_detect</a></span><span class='o'>(</span><span class='nv'>tweet</span>, <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='nv'>slurs</span>, collapse <span class='o'>=</span> <span class='s'>"|"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>month <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>month</span> <span class='o'>&gt;</span> <span class='s'>"2015-11"</span> <span class='o'>&amp;</span> <span class='nv'>month</span> <span class='o'>&lt;</span> <span class='s'>"2019-09"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>month</span>, y <span class='o'>=</span> <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"#8E44AD"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span>xintercept <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='s'>"2019-04"</span><span class='o'>)</span>, colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Tweets containing anti-female slurs, by month"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-23-1.png" width="700px" style="display: block; margin: auto;" />

</div>

In late 2017, Tory MP Gerry Ritz used the "Climate Barbie" slur. He eventually [apologized](https://www.cbc.ca/news/canada/ottawa/gerry-ritz-catherine-mckenna-climate-barbie-1.4298005), but it appears to have brought the insult into the mainstream:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://stringr.tidyverse.org/reference/str_detect.html'>str_detect</a></span><span class='o'>(</span><span class='nv'>tweet</span>, <span class='s'>"barbie"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>month <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='nv'>timestamp</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>month</span> <span class='o'>&gt;</span> <span class='s'>"2015-11"</span> <span class='o'>&amp;</span> <span class='nv'>month</span> <span class='o'>&lt;</span> <span class='s'>"2019-09"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>month</span>, y <span class='o'>=</span> <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"#F1948A"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span>xintercept <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.yearmon</a></span><span class='o'>(</span><span class='s'>"2019-04"</span><span class='o'>)</span>, colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_continuous.html'>scale_x_continuous</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Tweets containing the word \"barbie\", by month"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-24-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Indeed, McKenna has had to put up with many slurs in the last four years. The total counts for these slur over all replies directed to McKenna are:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>censor</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='kr'>for</span> <span class='o'>(</span><span class='nv'>i</span> <span class='kr'>in</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq_along</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='kr'>if</span> <span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span> <span class='o'>==</span> <span class='s'>"barbie"</span><span class='o'>)</span> <span class='kr'>next</span>
    <span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste0</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/substr.html'>substr</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, start <span class='o'>=</span> <span class='m'>1</span>, stop <span class='o'>=</span> <span class='m'>2</span><span class='o'>)</span>,
                   <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste0</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='s'>"*"</span>, <span class='nf'><a href='https://rdrr.io/r/base/nchar.html'>nchar</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>-</span> <span class='m'>3</span><span class='o'>)</span>, collapse <span class='o'>=</span> <span class='s'>""</span><span class='o'>)</span>,
                   <span class='nf'><a href='https://rdrr.io/r/base/substr.html'>substr</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, start <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/nchar.html'>nchar</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span>, stop <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/nchar.html'>nchar</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
  <span class='o'>}</span>
  <span class='nv'>x</span>
<span class='o'>}</span>

<span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://stringr.tidyverse.org/reference/str_detect.html'>str_detect</a></span><span class='o'>(</span><span class='nv'>tweet</span>, <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>slurs</span>, <span class='s'>"barbie"</span><span class='o'>)</span>, collapse <span class='o'>=</span> <span class='s'>"|"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>slur <span class='o'>=</span> <span class='nf'><a href='https://stringr.tidyverse.org/reference/str_extract.html'>str_extract_all</a></span><span class='o'>(</span><span class='nv'>tweet</span>, <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>slurs</span>, <span class='s'>"barbie"</span><span class='o'>)</span>, collapse <span class='o'>=</span> <span class='s'>"|"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>slur</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/nest.html'>unnest</a></span><span class='o'>(</span>cols <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>slur</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/count.html'>count</a></span><span class='o'>(</span><span class='nv'>slur</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>slur <span class='o'>=</span> <span class='nf'>censor</span><span class='o'>(</span><span class='nv'>slur</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 5 x 2</span></span>
<span class='c'>#&gt;   slur       n</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> barbie  </span><span style='text-decoration: underline;'>6</span><span>961</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> bi**h    158</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> cu*t      32</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> sl*t       6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> tw*t     107</span></span>
</code></pre>

</div>

Summary
-------

No one should have to put up with abuse, no matter their position or their politics. I applaud Minister McKenna for her perseverance and dogged dedication to her portfolio. Since approximately the middle of 2017, she has put up with constant abuse, and yet remains accessible to Canadians via Twitter. We can see from the evidence in this post that Climate Change denial and misogyny seem to go hand-in-hand. This is definitely something I will look into more.

------------------------------------------------------------------------

*This post was compiled on 2020-10-09 11:36:43. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package     * version    date       lib source                         </span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  downlit       0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  dplyr       * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fs            1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  glue          1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  htmlwidgets   1.5.1      2019-10-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)</span>
<span class='c'>#&gt;  janeaustenr   0.1.5      2017-06-10 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  jsonlite    * 1.7.1      2020-09-07 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  knitr         1.30       2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  labeling      0.3        2014-08-23 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  Matrix        1.2-18     2019-11-27 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  pillar        1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  purrr         0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  Rcpp          1.0.5      2020-07-06 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  readr       * 1.3.1      2018-12-21 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  rlang         0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  rmarkdown     2.3        2020-06-18 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  SnowballC     0.7.0      2020-04-01 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  stringi       1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  stringr     * 1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  tibble      * 3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tidyr       * 1.1.2      2020-08-27 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  tidytext    * 0.2.6      2020-09-20 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tokenizers    0.2.1      2018-03-29 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  vctrs         0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  withr         2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  wordcloud2  * 0.2.1      2018-01-03 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  xfun          0.18       2020-09-29 [2] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  zoo         * 1.8-8      2020-05-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

References
----------

<div id="refs" class="references">

<div id="ref-nielsen_2011_new">

Nielsen, Finn Årup. 2011. "A New ANEW: Evaluation of a Word List for Sentiment Analysis in Microblogs." In *Proceedings of the Eswc2011 Workshop on 'Making Sense of Microposts': Big Things Come in Small Packages*, edited by Matthew Rowe, Milan Stankovic, Aba-Sah Dadzie, and Mariann Hardey, 718:93--98. CEUR Workshop Proceedings. <http://ceur-ws.org/Vol-718/paper_16.pdf>.

</div>

</div>

