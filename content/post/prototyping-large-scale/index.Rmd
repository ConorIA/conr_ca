---
output: hugodown::md_document
title: "Prototyping a large-scale analysis of \"weather events\" in Canada"
subtitle: ""
slug: "prototyping-large-scale"
summary: "Code for moving from a single-station to a larger spatial scope"
authors: []
categories:
  - R
tags:
  - cold
  - heat
  - Toronto
date: 2020-06-12
lastmod: 2020-10-08
featured: false
draft: false
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
---

```{r setup, include=FALSE}
knitr::opts_knit$set(warning = FALSE)
```

In past blog posts I have looked at weather "events" of or "blocks" of similar "extreme" weather (specifically cold snaps and heat waves), and how these weather events are described by the SSC2 [@Sheridan_2002_redevelopment] weather types. My posts have focused on Toronto, but my research interest is broader. So I have developed some code to help automate some of the analysis that I would perform at the station level. I originally developed some of this code as a Shiny app, but that app needs an update, so this post is as much about debugging the code as it is about writing it! 

First, we load the libraries that we will need for this analysis. 

```{r, message=FALSE}
library(canadaHCDx)
library(claut)
library(dplyr)
library(lubridate)
library(plotly)
library(purrr)
library(readr)
library(stringr)
library(tibble)
library(tidyr)
library(XML)
```

## Getting information about the available stations

Let's first grab the list of SSC2 stations. We want to look at both local weather conditions and the weather type, so I will use the SSC2 stations as my "data points", and find ECCC data from the same, or nearby stations. 

First I will download the webpage that shows the available stations, pull the table from the HTML using the **XML** package, and then filter so that my list only contains the stations in Canada. 

```{r}
f <- tempfile(fileext = ".html")
url <- "http://sheridan.geog.kent.edu/ssc/stations.html"
download.file(url, f, quiet = TRUE)

readHTMLTable(f,stringsAsFactors = FALSE)[[1]] %>% 
  as_tibble() %>% 
  filter(grepl("Canada", Station)) %>% 
  mutate(Station = sub("\\s*, Canada", "", Station)) -> stns

stns
```

We want to know the years of available data, but the formatted list of ranges is not very easy to automate. Let's get the minimum and maximum years as "start" and "end". 

```{r}
str_extract_all(stns$Years, "[0-9]{4}") %>% 
  map_df(~tibble(Start = min(as.integer(.)),
                 End = max(as.integer(.)))) %>% 
  bind_cols(stns, .) -> stns

stns
```

The "Years" column is now _almost_ redundant, but it still contains an extra point of interest that we have not captured in "Start" and "End"&mdash;the periods of missing data are apparent as the gaps between the last year of each range and the first year of the next range. Let's use this logic to pull this information out, explicitly. 

```{r}
str_extract_all(stns$Years, "-[0-9]{4} [0-9]{4}-") %>% 
  map_df(~list(Gaps = paste(as.integer(str_sub(., 2, 5)) + 1L,
                            as.integer(str_sub(., 6, 10)) -1L,
                            sep = "-", collapse = ", "))) %>% 
  mutate(Gaps = str_replace_all(Gaps, "([0-9]{4})-\\1", "\\1")) %>% 
  bind_cols(select(stns, -Years), .) -> stns

stns
```

Now we have information on the temporal availability of data, but we are missing the spatial information. Let's pull the information out of the [Google Map](https://www.google.com/maps/d/viewer?mid=1wHXUzwNjNvmnz2JW9XFr8AhqntWEaeFQ&ll=44.74122602342316%2C-17.754999999999995&z=2) at http://sheridan.geog.kent.edu/ssc.html 

```{r}
f <- tempfile(fileext = ".kml")
download.file("https://www.google.com/maps/d/kml?mid=1wHXUzwNjNvmnz2JW9XFr8AhqntWEaeFQ&lid=PlhtYW-UWmA&forcekml=1", f, quiet = TRUE)
coords <- rgdal::readOGR(f)
```

Let's see what we're working with:
```{r}
coords %>% head()
```

Extract the information that we are interested in. 
```{r}
coords <- bind_cols(
  attr(coords, "data") %>% mutate(ID = str_sub(description, 6, 8)) %>% select(ID),
  attr(coords, "coords") %>% as_tibble %>% select(Latitude = coords.x2, Longitude = coords.x1)
)

coords %>% head()
```

Join to our `stns` table. 

```{r}
left_join(stns, coords, by = "ID") %>% 
  select(Station, ID, Latitude, Longitude, Start:Gaps) -> stns

stns
```

## Getting station data

Let's look at how we would analyze data for a given station. True to form, I'll stick to Toronto for the first portion of this blog post. This is the station called "`r stns$Station[stns$ID == "YYZ"]`" in our table above.

First, we want to get the relevant data from our `stns` object for the Station. 

```{r}
get_stn_info <- function(station_in, stns) {
  filter(stns, Station == station_in)
}

stn_info <- get_stn_info("Toronto, Ontario", stns)

stn_info
```

Now we need to find the ECCC stations that are co-located (or nearby) to `r stn_info$Latitude`&deg;N, `r -stn_info$Longitude`&deg;W. 

```{r}
get_eccc_data <- function(stn_info) {
  # Look for nearby stations using canadaHCDx
  eccc_stns <- filter(find_station(),
                      LatitudeDD == stn_info$Latitude & LongitudeDD == stn_info$Longitude)
  # If no station is co-located, find another within (an arbitrary) 2 km
  if (nrow(eccc_stns) == 0) {
      eccc_stns <- find_station(target = c(stn_info$latitude, stn_info$longitude), dist = 0:2)
  }
  
  # Return data using the CanadaHCD package.
  # Cache requires fork - remotes::install_github("ConorIA/canadaHCD@cache_external")
  hcd_hourly(eccc_stns$StationID,
             year = stn_info$Start:stn_info$End,
             month = c(1, 2, 6, 7, 8, 12), cache = TRUE, progress = FALSE)
}
```

The function above will return ECCC data for one _or more_ nearby stations (co-located or within 2 km).

```{r, warning=FALSE}
dat <- get_eccc_data(stn_info)

dat
```

Now we need the corresponding weather types data, binned as I have done in previously: "warm" (DT, MT, MT+, MT++), "moderate" (DM, MM), and "cool" (DP, MP), plus "transitional". 

```{r}
get_ssc_data <- function(stn_info) {
  f <- tempfile(fileext = ".dbdmt")
  url <- sprintf("http://sheridan.geog.kent.edu/ssc/files/%s.dbdmt", stn_info$ID)
  download.file(url, f, quiet = TRUE)
  air <- read_table(f, col_names = c("Station", "Date", "Air"),
                    col_types = cols(col_character(),
                                     col_date(format = "%Y%m%d"),
                                     col_factor(levels = c(1, 2, 3, 4, 5, 6, 7, 66, 67))))
  levels(air$Air) <- list(warm = c(3, 6, 66, 67), mod = c(1, 4), cool = c(2, 5), trans = 7)
  air %>% mutate(Air = forcats::fct_explicit_na(air$Air, na_level = "(none)"))
}
```

```{r}
air <- get_ssc_data(stn_info)

air
```

### Making daily data

As explained in a [previous post](../toronto-heat-warning-weather-yyz), the use of 6AM UTC for the "cutoff" point for the dermatological day is not ideal. I will once again use the sun-based COW<sub>ND</sub> proposed by @zaknic-catovic_2018_comparison.

I need a few helper functions to get me from A to B in this case. 

A function to find the timezone of each station is implemented in the **claut** package, which in turn uses the **lutz** package.

And functions to calculate windchill (`wcl`) and humidex (`hdx`).

```{r}
wcl <- function(tair, wspeed) {
  wcl = 13.12 + 0.6215 * tair - 11.37 * wspeed^0.16 + 0.3965 * tair * wspeed^0.16
  wcl[tair >= 10 | wspeed < 4.8] <- NA
  round(wcl)
}

hdx <- function(tair, tdew) {
  tdew = tdew + 273.15
  e = 6.11 * exp(1)^(5417.7530  * ((1 / 273.16) - (1 / tdew)))
  h = 0.5555 * (e - 10)
  round(tair + h)
}
```

Now let's create the function to create our data set(s).

I am going to include an additional note

```{r}
make_eccc_daily <- function(stn_info, dat) {
  
  tz <- get_tz(stn_info$Latitude, stn_info$Longitude)
  
  dat %>% group_by(DateTime) %>%
    summarize(StationID = paste(StationID[!is.na(Temp)], collapse = ", "),
              Temp = mean(Temp, na.rm = TRUE),
              DewPointTemp = mean(DewPointTemp, na.rm = TRUE),
              WindSpeed = mean(WindSpeed, na.rm = TRUE),
              .groups = "drop") -> dat
  
  filter(dat, month(DateTime) %in% c(1, 2, 12)) %>% 
    mutate(WindChill = wcl(Temp, WindSpeed)) %>% 
    select(DateTime, Temp, WindChill) %>% 
    solar_daily(., stn_info$Latitude, stn_info$Longitude,
                tz, na.rm = TRUE) -> win_dly
  
  filter(dat, month(DateTime) %in% 6:8) %>% 
    mutate(Humidex = hdx(Temp, DewPointTemp)) %>% 
    select(DateTime, Temp, Humidex) %>% 
    solar_daily(., stn_info$Latitude, stn_info$Longitude,
                tz, na.rm = TRUE) -> sum_dly
  
    list(win_dly = win_dly, sum_dly = sum_dly)
}
```

```{r, warning=FALSE}
daily_dat <- make_eccc_daily(stn_info, dat)
```

## Identifying "events"

Now we can identify our events. Here we will look at heatwaves and cold snaps. I will be working with the following definitions: 

- A cold snap is any two or more days with minimum temperatures below -15&deg;C or windchill below -20&deg;C.
- A heat wave is any two or more days with maximum temperatures above 30&deg;C and minimum temperatures above 20&deg;C, or days when the humidex exceeds 40. 

```{r}
win_expression <- "MinTemp <= -15 | MinWindChill <= -20"
win_event_length <- 2

sum_expression <- "(MaxTemp >= 31 & MinTemp >= 20) | MaxHumidex >= 40"
sum_event_length <- 2
```

```{r, warning=FALSE}
make_eccc_warns <- function(stn_info, daily_dat,
                            win_expression, win_event_length,
                            sum_expression, sum_event_length) {
  
  sum_warns <- daily_dat$sum_dly %>%
    mutate(Criteria = eval(parse(text = sum_expression))) %>% 
    group_by(Year = year(Date)) %>%
    mutate(Start = describe_snaps(rle(Criteria), sum_event_length, Date, min),
           End = describe_snaps(rle(Criteria), sum_event_length, Date, max),
           Length = describe_snaps(rle(Criteria), sum_event_length)) %>%
    filter(!is.na(Length)) %>%
    group_by(Year, Start, End, Length) %>%
    summarize_all(mean) %>%
    select(-Criteria, -Date)
  
  win_warns <- daily_dat$win_dly %>%
    mutate(Criteria = eval(parse(text = win_expression))) %>% 
    mutate(Year = case_when(month(Date) == 12 ~ year(Date) + 1,
                            TRUE ~ year(Date))) %>%
    filter(Year != stn_info$Start) %>%
    group_by(Year) %>%
    mutate(Start = describe_snaps(rle(Criteria), win_event_length, Date, min),
           End = describe_snaps(rle(Criteria), win_event_length, Date, max),
           Length = describe_snaps(rle(Criteria), win_event_length)) %>%
    filter(!is.na(Length)) %>%
    group_by(Year, Start, End, Length) %>%
    summarize_all(mean) %>%
    select(-Criteria, -Date)
  
  list(win_warns = win_warns, sum_warns = sum_warns)
}
```

```{r, warning=FALSE}
warns <- make_eccc_warns(stn_info, daily_dat,
                         win_expression, win_event_length,
                         sum_expression, sum_event_length)
```

## Creating seasonal summaries

Let's look at some seasonal summaries as well. Summarize the number of days with a particular air mass, by season.

```{r}
make_air_sets <- function(stn_info, air, daily_dat) {
  
  sum_air <- left_join(daily_dat$sum_dly, select(air, -Station), by = "Date") %>%
    group_by(Year = year(Date), Air) %>%
    count() %>%
    ungroup() %>%
    pivot_wider(names_from = "Air", values_from = "n", values_fill = list(n = 0L))
  
  win_air <- left_join(daily_dat$win_dly, select(air, -Station), by = "Date") %>%
    mutate(Year = case_when(month(Date) == 12 ~ year(Date) + 1,
                           TRUE ~ year(Date))) %>%
    filter(Year != stn_info$Start) %>%
    group_by(Year, Air) %>%
    count() %>%
    ungroup() %>%
    pivot_wider(names_from = "Air", values_from = "n", values_fill = list(n = 0L))
  
  list(win_air = win_air, sum_air = sum_air)
}
```

```{r}
airs <- make_air_sets(stn_info, air, daily_dat)

airs$sum_air
```

Summarize the characteristics of "events" during each season.


```{r}
make_eccc_warns_seasonal <- function(warns) {

  summ_sum <- left_join(warns$sum_warns %>% group_by(Year) %>% count(),
                        warns$sum_warns %>% group_by(Year) %>%
                            summarize(AvgLen = mean(Length),
                                      AvgTmax = weighted.mean(MaxTemp, Length),
                                      AvgTmax = weighted.mean(MinTemp, Length),
                                      AvgTmean = weighted.mean(MeanTemp, Length),
                                      AvgMaxHumidex = weighted.mean(MaxHumidex, Length)),
                        by = "Year")
  
  summ_win <- left_join(warns$win_warns %>% group_by(Year) %>% count(),
                        warns$win_warns %>% group_by(Year) %>%
                          summarize(AvgLen = mean(Length),
                                    AvgTmax = weighted.mean(MaxTemp, Length),
                                    AvgTmax = weighted.mean(MinTemp, Length),
                                    AvgTmean = weighted.mean(MeanTemp, Length),
                                    AvgMinWindChill = weighted.mean(MinWindChill, Length)),
                        by = "Year")
  
  list(summ_win = summ_win, summ_sum = summ_sum)
}
```
  
```{r, message=FALSE}
warns_summary <- make_eccc_warns_seasonal(warns)

warns_summary$summ_win
```

And finally a seasonal weather data set.
   
```{r}
make_eccc_seasonal <- function(stn_info, daily_dat,
                               sum_expression, win_expression) {

  win_seas <- daily_dat$win_dly %>%
    mutate(Year = case_when(month(Date) == 12 ~ year(Date) + 1,
                            TRUE ~ year(Date)),
           Criteria = eval(parse(text = win_expression))) %>%
    filter(Year != stn_info$Start) %>%
    group_by(Year) %>%
    summarize(MaxTemp = mean(MaxTemp, na.rm = TRUE),
              MinTemp = mean(MinTemp, na.rm = TRUE),
              MeanTemp = mean(MeanTemp, na.rm = TRUE),
              MinWindChill = mean(MinWindChill, na.rm = TRUE),
              ColdDays = sum(Criteria, na.rm = TRUE)) %>%
    mutate_if(is.numeric, ~ifelse(abs(.) == Inf,NA,.)) %>% 
    mutate(ColdDays = ifelse(is.na(MaxTemp) & is.na(MinTemp) & is.na(MeanTemp),
                             NA, ColdDays)) #FIXME: Need a more elegant solution to this
  
  sum_seas <- daily_dat$sum_dly %>%
    mutate(Criteria = eval(parse(text = sum_expression))) %>% 
    group_by(Year = year(Date)) %>%
    summarize(MaxTemp = mean(MaxTemp, na.rm = TRUE),
              MinTemp = mean(MinTemp, na.rm = TRUE),
              MeanTemp = mean(MeanTemp, na.rm = TRUE),
              MaxHumidex = mean(MaxHumidex, na.rm = TRUE),
              HeatDays = sum(Criteria, na.rm = TRUE)) %>% 
    mutate_if(is.numeric, ~ifelse(abs(.) == Inf,NA,.)) %>% 
    mutate(HeatDays = ifelse(is.na(MaxTemp) & is.na(MinTemp) & is.na(MeanTemp),
                             NA, HeatDays)) #FIXME: Need a more elegant solution to this
  
  list(win_seas = win_seas, sum_seas = sum_seas)
}
```

```{r, message=FALSE}
seas_dat <- make_eccc_seasonal(stn_info, daily_dat,
                               sum_expression, win_expression)

seas_dat$sum_seas
```

## Exploring the initial results

Now we can plot the initial results. Some of these plots will need to be tweaked, for instance, to remove months with too many missing values. 

### Winter "cold snaps"

#### Winter SSC classes

```{r, warning=FALSE}
airs$win_air %>%
  pivot_longer(-Year, names_to = "air", values_to = "n") %>%
  plot_ly(x = ~Year, y = ~n, color = ~air) %>%
  add_lines() %>%
  layout(title = "Seasonal wintertime air masses",
         yaxis = list(title = "Number of instances of air mass type"))
```

#### Winter weather events
   
```{r, warning=FALSE}
warns$win_warns %>% 
  plot_ly(x = ~Start, y = ~MinWindChill, color = ~MinTemp, size = ~Length,
          hoverinfo = "text", 
          hovertext = paste0(warns$win_warns$Start, ": ",
                             warns$win_warns$Length, "days")) %>%
  plotly::add_markers() %>%
  layout(title = "Wintertime cold alerts",
         xaxis = list(title = "Start Date",
                      range = c(min(warns$win_warns$Start - 1000),
                                max(warns$win_warns$Start) + 1000)))
```
   
#### Seasonal winter weather event summary
    
```{r, warning=FALSE}
seas_dat$win_seas %>% 
  select(-`ColdDays`) %>%
  pivot_longer(-Year, names_to = "Var", values_to = "value") %>%
  plot_ly(x = ~Year, y = ~value, color = ~Var) %>%
  add_lines() %>%
  layout(yaxis = list(title = "Temperature (°C) / WindChill"),
         title = "Winter temperature and windchill")
```

```{r, warning=FALSE}
warns_summary$summ_win %>% 
  plot_ly(x = ~Year, y = ~n, 
          color = ~AvgMinWindChill, size = ~AvgLen,
          hoverinfo = "text",
          hovertext = paste0(warns_summary$summ_win$Year, ": ",
                             warns_summary$summ_win$n,
                             " event(s) with mean duration of ",
                             round(warns_summary$summ_win$AvgLen, 1),
                             " days")) %>% plotly::add_markers() %>%
  layout(title = "Seasonal wintertime cold alert weather",
         yaxis = list(title = "Number of cold alerts"))
```    

We can also perform a quick linear regression (though these data are likely to be junk until I've done a better job of dealing with missing values and adding in the "zero-event" years.)

**Seasonal characteristics**

```{r}
fx <- function(x) {
  if (all(is.na(x$value))) {
    tibble(min = NA, max = NA, mean = NA)
  } else {
    summ <- summary(lm(value~Year, data = x))
    tibble(min = min(x$value, na.rm = TRUE),
           max = max(x$value, na.rm = TRUE),
           mean = mean(x$value, na.rm = TRUE),
           slope = coef(summ)[2], rsq = summ$r.squared, p = coef(summ)[8])
  }
}

seas_dat$win_seas %>% 
  pivot_longer(-Year, names_to = "var", values_to = "value") %>% 
  group_by(var) %>% 
  nest() %>% 
  mutate(model = map(data, ~fx(.))) %>% 
  select(-data) %>% 
  unnest(model) %>% 
  knitr::kable("markdown", digits = 3, escape = FALSE)
```

**Characteristics of cold snaps**

```{r}
warns_summary$summ_win %>%
  filter(Year != year(Sys.Date())) %>%
  pivot_longer(-Year, names_to = "var", values_to = "value") %>%
  group_by(var) %>% 
  nest() %>% 
  mutate(model = map(data, ~fx(.))) %>% 
  select(-data) %>% 
  unnest(model) %>% 
  knitr::kable("markdown", digits = 3, escape = FALSE)
```

### Summer "heat waves"

#### Summer SSC classes

```{r, warning=FALSE}
airs$sum_air %>%
  pivot_longer(-Year, names_to = "air", values_to = "n") %>%
  plot_ly(x = ~Year, y = ~n, color = ~air) %>%
  add_lines() %>%
  layout(title = "Seasonal summertime air masses",
         yaxis = list(title = "Number of instances of air mass type"))
``` 

#### Summer weather events
   
```{r, warning=FALSE}
warns$sum_warns %>% 
  plot_ly(x = ~Start, y = ~MaxHumidex, color = ~MaxTemp, size = ~Length,
          hoverinfo = "text", 
          hovertext = paste0(warns$sum_warns$Start, ": ",
                             warns$sum_warns$Length, "days")) %>%
  plotly::add_markers() %>%
  layout(title = "Summertime heat warnings",
         xaxis = list(title = "Start Date",
                      range = c(min(warns$sum_warns$Start - 1000),
                                max(warns$sum_warns$Start) + 1000)))
```
   
#### Seasonal summer weather event summary
    
```{r, warning=FALSE}
seas_dat$sum_seas %>% 
  select(-`HeatDays`) %>%
  pivot_longer(-Year, names_to = "Var", values_to = "value") %>%
  plot_ly(x = ~Year, y = ~value, color = ~Var) %>%
  add_lines() %>%
  layout(yaxis = list(title = "Temperature (°C) / Humidex"),
         title = "Summertime temperature and humidex")
```

```{r, warning=FALSE}
warns_summary$summ_sum %>% 
  plot_ly(x = ~Year, y = ~n,
          color = ~AvgMaxHumidex, size = ~AvgLen,
          hoverinfo = "text",
          hovertext = paste0(warns_summary$summ_sum$Year, ": ",
                             warns_summary$summ_sum$n,
                             " event(s) with mean duration of ",
                             round(warns_summary$summ_sum$AvgLen, 1),
                             " days")) %>% plotly::add_markers() %>%
  layout(title = "Seasonal summertime heat alert weather",
         yaxis = list(title = "Number of heat warnings"))
```    

Once again, we can perform a junky linear regression. 

**Seasonal characteristics**

```{r}
seas_dat$sum_seas %>% 
  pivot_longer(-Year, names_to = "var", values_to = "value") %>% 
  group_by(var) %>% 
  nest() %>% 
  mutate(model = map(data, ~fx(.))) %>% 
  select(-data) %>% 
  unnest(model) %>% 
  knitr::kable("markdown", digits = 3, escape = FALSE)
```

**Characteristics of heat waves**

```{r}
warns_summary$summ_sum %>%
  filter(Year != year(Sys.Date())) %>%
  pivot_longer(-Year, names_to = "var", values_to = "value") %>%
  group_by(var) %>% 
  nest() %>% 
  mutate(model = map(data, ~fx(.))) %>% 
  select(-data) %>% 
  unnest(model) %>% 
  knitr::kable("markdown", digits = 3, escape = FALSE)
```

## Fin

The code included here is just the start of a method to perform an analysis of weather events across space and time. Future blog posts will look at other regions in Canada, and I will continue to iterate upon and improve this code. 

---

_This post was compiled on `r Sys.time()`. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below._

<details>
  <summary>Packages Used in this post</summary>
```{r}
sessioninfo::package_info(dependencies = "Depends")
```
</details>
