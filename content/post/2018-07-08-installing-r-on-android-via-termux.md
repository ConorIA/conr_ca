---
title: Installing R on Android via Termux
author: ''
date: '2018-07-08'
lastmod: '2020-06-03'
slug: installing-r-on-android-via-termux
categories:
  - R
tags:
  - Android
summary: "Get a full IDE experience in R on Android." 
---

**Update 2020-06-03: This post continues to be the main driver of traffic to my website, so I figured it was a good idea to give it a bit of TLC after nearly two years since I first posted. I have made a few modifications to bring the code up to date for 2020.**

---

Now and then I'll be away from home on the subway or something when I think about a quick function call I want to test in R, or a line of code I want to tweak. I have sometimes logged in to my RStudio Server instance, but the interface is next to impossible to use on a cell phone. Instead of accessing R _from_ my phone, the solution was to access R _on_ my phone. The instructions here will help you get set up with an IDE-like R experience on Android. 

To give credit where credit is due, some commands below were suggested in [this issue](https://github.com/termux/termux-packages/issues/250) on the Termux packages GitHub repo. 

## Step 1: Installing Termux

Termux forms the basis of this little project. It is a terminal emulator for Android, with a Linux environment and package management tools. Get it on [Google Play here](https://play.google.com/store/apps/details?id=com.termux).

## Step 2: Add the pointless package repository

GitHub user [_its-pointless_](https://github.com/its-pointless) maintains a repository of packages for Termux, including the ones we'll need to install R. Enter the commands below to add their repository to your Termux install.

```bash
pkg install curl gnupg
mkdir -p "$PREFIX/etc/apt/sources.list.d/"
echo "deb https://its-pointless.github.io/files/24 termux extras" > "$PREFIX/etc/apt/sources.list.d/pointless.list"
curl "https://its-pointless.github.io/pointless.gpg" | apt-key add
```

## Step 3: Install required `.deb` packages

The following series of packages will give you a fully-functioning R environment, including the ability to install R packages from CRAN.

```bash
pkg install r-base \
            make \
            clang \
            gcc-9 \
            libgfortran5 \
            openssl \
            libcurl \
            libicu \
            libxml2
```

You will need to configure your compiler. _its-pointless_ provides a handy script for this. Just run `setupclang-gfort-9`.

## Step 4: Install R packages

Now you're ready to install your packages. First launch R by typing an uppercase R: `R`. 

From within R, use the `install.packages()` function to install the packages of your choice. For instance, if you are a **tidyverse** user, you can run:

```r
install.packages("tidyverse")
```

Note, the above install takes a _long_ time. You may prefer to install individual packages instead of the whole **tidyverse** bundle ... really, how many **knitr** documents do you plan to write on your phone?

## Step 5: Adding an IDE

If you are happy to use R from the R console, then you can skip this step. If you, like me, prefer to have a bit more control to run lines and see the objects in your environment, then you can get something akin to the RStudio experience (console-based of course) using [Neovim](https://neovim.io/) and the [Nvim-R plugin](https://github.com/jalvesaq/Nvim-R).

To get started, install Neovim. 

```bash
pkg install neovim
```

You can find the latest version of **Nvim-R** [here](http://www.vim.org/scripts/script.php?script_id=2628). Take a note of the URL for the latest release. You can download this directly to your Termux environment via cURL as follows: 

```bash
curl -L "https://www.vim.org/scripts/download_script.php?src_id=27185" > NvimR.vmb
```

In an effort to future-proof this post, you can download the newest version automatically with this (rough) grep hack (YMMV): 

```bash
latestver=$(curl -sNL "https://www.vim.org/scripts/script.php?script_id=2628" | grep -m1 "download_script.php" | grep -Eo "[0-9]+")
curl -L "https://www.vim.org/scripts/download_script.php?src_id=$latestver" > NvimR.vmb
```
You have now saved the **Nvim-R** plugin in a file in the current directory, but it is not installed yet. To install, launch Neovim and open the `NvimR.vmb` file. 

```bash
nvim NvimR.vmb
```

In `nvim`, type the following two commands:

```
:packadd vimball
:so %
```

You will see a list of files installed. Hit <kbd>space</kbd> until you reach the end of the output. You can now quit via `:q!`.

## Step 6: Profit

There is a pretty steep learning curve to using Neovim and I'll admit that I'm still just getting started. The following will give you a few tips to get your feet wet. For more detailed instructions, see the [Nvim-R documentation](https://raw.githubusercontent.com/jalvesaq/Nvim-R/master/doc/Nvim-R.txt). 

You can open a new R file in Neovim by typing the following:

```bash
nvim test.R
```

You should now find yourself within Neovim. Fire up an R instance by typing `\rf` and wait for it to initialize. 

You can open the object browser via `\ro`. This will give you a layout that looks a little like RStudio. 

You can tap between the console and the edit area. To insert text, type <kbd>i</kbd>. When you are done typing, hit <kbd>Esc</kbd>. Run a line by `\l`. 

_Note: To insert an underscore, type <kbd>&#95;</kbd> twice. One is turned to `<-`_

For inspiration: 

![Nvim-R on Google Pixel](../../img/2018-07-08-installing-r-on-android-via-termux/20180708-135043.png)

And there you have it. Good luck coding in R from your phone!

### Alternative means:

An alternative means of using R on Android is to use a full Linux distro on top of Termux, and then install R the way you would on that distro. [AnLinux](https://play.google.com/store/apps/details?id=exa.lnx.a) makes this easy to do.
