---
output: hugodown::md_document
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Examining Wintertime Polar Events at Toronto"
subtitle: ""
summary: "A look at some wintertime polar events in Toronto since 1950." 
authors: []
categories:
  - R
tags:
  - cold
  - temperature
  - Toronto
date: 2018-01-26
lastmod: 2018-01-26
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
rmd_hash: 9d2cee239aa154b3

---

Introduction
------------

In my [last post](https://conr.ca/post/2018-01-11_revisiting_december_temps/), I looked at December temperatures in Toronto in reponse to the cold snap that we experienced between December and January of this winter. I wanted to delve deeper and contextualize that cold snap among other prolonged periods of polar air in Toronto, which I'll refer to as "polar events" throughout this post.

First, I will load the libraries that I will use.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/ConorIA/canadaHCDx/'>canadaHCDx</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://ggplot2.tidyverse.org'>ggplot2</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://lubridate.tidyverse.org'>lubridate</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://readr.tidyverse.org'>readr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/hadley/reshape'>reshape2</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tibble.tidyverse.org/'>tibble</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tidyr.tidyverse.org'>tidyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://zoo.R-Forge.R-project.org/'>zoo</a></span><span class='o'>)</span>
</code></pre>

</div>

We'll get Toronto temperature data up to January 9, 2018.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Toronto changed station code afer June 2003, so we'll merge it with the new one. </span>
<span class='nv'>tor</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_daily.html'>hcd_daily</a></span><span class='o'>(</span><span class='m'>5051</span>, <span class='m'>1950</span><span class='o'>:</span><span class='m'>2003</span><span class='o'>)</span>
<span class='nv'>tmp</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/canadaHCD/man/hcd_daily.html'>hcd_daily</a></span><span class='o'>(</span><span class='m'>31688</span>, <span class='m'>2003</span><span class='o'>:</span><span class='m'>2018</span><span class='o'>)</span>
<span class='nv'>tor</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nv'>tor</span><span class='o'>[</span><span class='m'>1</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tor</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2003-06-30"</span><span class='o'>)</span>,<span class='o'>]</span>, <span class='nv'>tmp</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tmp</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2003-07-01"</span><span class='o'>)</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='nv'>tmp</span><span class='o'>$</span><span class='nv'>Date</span> <span class='o'>==</span> <span class='s'>"2018-01-09"</span><span class='o'>)</span>,<span class='o'>]</span><span class='o'>)</span>
</code></pre>

</div>

Instead of using temperature, I'm going to classify events by air masses. I'll start with data from Sheridan's [Spatial Synoptic Classification (SSC)](http://sheridan.geog.kent.edu/ssc.html) (Sheridan [2002](#ref-Sheridan_2002_redevelopment)).

Just like my last post, I'll use four bins, as per Anderson and Gough ([2017](#ref-Anderson_2017_Evolution)):

1.  'cool': SSC codes 2 (dry polar) and 5 (moist polar)
2.  'moderate': SSC codes 1 (dry moderate) and 4 (moist moderate)
3.  'warm': SSC codes 3 (dry tropical), 6 (moist tropical), 66 (moist tropical plus), and 67 (moist tropical double plus)
4.  'transition': SCC code 7 (transition)

I'm going to manually add some air classifications at the end of the SSC series to account for the rest of the polar event (through January 7). Those dates haven't been officially classified under the to SSC yet. Note that I might have some of these days wrong. I used [archived jet stream maps](http://squall.sfsu.edu/crws/archive/jet_arch.html) for reference.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>f</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/tempfile.html'>tempfile</a></span><span class='o'>(</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/download.file.html'>download.file</a></span><span class='o'>(</span><span class='s'>"http://sheridan.geog.kent.edu/ssc/files/YYZ.dbdmt"</span>, <span class='nv'>f</span>, quiet <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
<span class='nv'>air</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://readr.tidyverse.org/reference/read_table.html'>read_table</a></span><span class='o'>(</span><span class='nv'>f</span>, col_names <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Station"</span>, <span class='s'>"Date"</span>, <span class='s'>"Air"</span><span class='o'>)</span>, col_types <span class='o'>=</span> <span class='nf'><a href='https://readr.tidyverse.org/reference/cols.html'>cols</a></span><span class='o'>(</span><span class='nf'><a href='https://readr.tidyverse.org/reference/parse_atomic.html'>col_character</a></span><span class='o'>(</span><span class='o'>)</span>, <span class='nf'><a href='https://readr.tidyverse.org/reference/parse_datetime.html'>col_date</a></span><span class='o'>(</span>format <span class='o'>=</span> <span class='s'>"%Y%m%d"</span><span class='o'>)</span>, <span class='nf'><a href='https://readr.tidyverse.org/reference/parse_factor.html'>col_factor</a></span><span class='o'>(</span>levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>4</span>, <span class='m'>5</span>, <span class='m'>6</span>, <span class='m'>7</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='c'># Note, I am ignoring code 8 because that is the SSC2 "NA" value, which is turned to NA by ignoring.</span>
<span class='nf'><a href='https://rdrr.io/r/base/levels.html'>levels</a></span><span class='o'>(</span><span class='nv'>air</span><span class='o'>$</span><span class='nv'>Air</span><span class='o'>)</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>cool <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>5</span><span class='o'>)</span>, mod <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>4</span><span class='o'>)</span>, warm <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>3</span>, <span class='m'>6</span>, <span class='m'>66</span>, <span class='m'>67</span><span class='o'>)</span>, trans <span class='o'>=</span> <span class='s'>"7"</span><span class='o'>)</span>

<span class='nv'>air</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/bind.html'>bind_rows</a></span><span class='o'>(</span><span class='nv'>air</span>, <span class='nf'><a href='https://tibble.tidyverse.org/reference/tibble.html'>tibble</a></span><span class='o'>(</span>Date <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span><span class='nf'><a href='http://lubridate.tidyverse.org/reference/ymd.html'>ymd</a></span><span class='o'>(</span><span class='s'>"2018-01-01"</span><span class='o'>)</span>, <span class='nf'><a href='http://lubridate.tidyverse.org/reference/ymd.html'>ymd</a></span><span class='o'>(</span><span class='s'>"2018-01-09"</span><span class='o'>)</span>, by <span class='o'>=</span> <span class='s'>'day'</span><span class='o'>)</span>, Air <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/factor.html'>factor</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='s'>'cool'</span>, <span class='m'>7</span><span class='o'>)</span>, <span class='s'>'mod'</span>, <span class='s'>'mod'</span><span class='o'>)</span>, levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/levels.html'>levels</a></span><span class='o'>(</span><span class='nv'>air</span><span class='o'>$</span><span class='nv'>Air</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Now, let's create a table that includes just temperature and air mass data.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor</span> <span class='o'>&lt;-</span> <span class='nv'>tor</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Date</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tibble.tidyverse.org/reference/add_column.html'>add_column</a></span><span class='o'>(</span>Air <span class='o'>=</span> <span class='nv'>air</span><span class='o'>$</span><span class='nv'>Air</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>tail</a></span><span class='o'>(</span><span class='nv'>tor</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 6 x 5</span></span>
<span class='c'>#&gt;   Date       MaxTemp MinTemp MeanTemp Air  </span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;fct&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> 2018-01-04    -</span><span style='color: #BB0000;'>7.7</span><span>   -</span><span style='color: #BB0000;'>19.7</span><span>    -</span><span style='color: #BB0000;'>13.7</span><span> cool </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> 2018-01-05   -</span><span style='color: #BB0000;'>14.7</span><span>   -</span><span style='color: #BB0000;'>20.6</span><span>    -</span><span style='color: #BB0000;'>17.7</span><span> cool </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> 2018-01-06   -</span><span style='color: #BB0000;'>15.4</span><span>   -</span><span style='color: #BB0000;'>22.3</span><span>    -</span><span style='color: #BB0000;'>18.9</span><span> cool </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>4</span><span> 2018-01-07    -</span><span style='color: #BB0000;'>1</span><span>     -</span><span style='color: #BB0000;'>17.5</span><span>     -</span><span style='color: #BB0000;'>9.3</span><span> cool </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>5</span><span> 2018-01-08     3      -</span><span style='color: #BB0000;'>1.7</span><span>      0.7 mod  </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>6</span><span> 2018-01-09     1.6    -</span><span style='color: #BB0000;'>0.6</span><span>      0.5 mod</span></span>
</code></pre>

</div>

I'm going to isolate the winter months (December, January, and February). Note that I will be using the year of January and February to refer to a given winter, so winter 2014 is December 2013 through February 2014. In the text, I will (rightfully) call this winter 2013/14.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_winter</span> <span class='o'>&lt;-</span> <span class='nv'>tor</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='http://lubridate.tidyverse.org/reference/month.html'>month</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span> <span class='o'>%in%</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>12</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span>Year <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/year.html'>year</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span>, Month <span class='o'>=</span> <span class='nf'><a href='http://lubridate.tidyverse.org/reference/month.html'>month</a></span><span class='o'>(</span><span class='nv'>Date</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Winter <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/case_when.html'>case_when</a></span><span class='o'>(</span><span class='nv'>Month</span> <span class='o'>%in%</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>11</span><span class='o'>)</span> <span class='o'>~</span> <span class='nv'>Year</span>, <span class='nv'>Month</span> <span class='o'>==</span> <span class='m'>12</span> <span class='o'>~</span> <span class='nv'>Year</span> <span class='o'>+</span> <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nv'>ungroup</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>`Year`</span>, <span class='o'>-</span><span class='nv'>`Month`</span><span class='o'>)</span>
</code></pre>

</div>

Since I'm interested in the length of polar events, I'll look for lengths of continuous cool air using R's [`rle()`](https://rdrr.io/r/base/rle.html) function. The code below is probably overly-complicated, but it does the trick. I will count polar events as continuous periods of cool air. Days with transitional air will count if they are preceeded *and* followed by cool air; same goes for missing values.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>fx</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span>, <span class='nv'>y</span>, <span class='nv'>comm</span> <span class='o'>=</span> <span class='kc'>NULL</span>, <span class='nv'>...</span><span class='o'>)</span> <span class='o'>{</span>
  
  <span class='nv'>event</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/rle.html'>rle</a></span><span class='o'>(</span><span class='nv'>x</span> <span class='o'>==</span> <span class='s'>"cool"</span> <span class='o'>|</span> <span class='o'>(</span>
    <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>)</span> <span class='o'>|</span> <span class='nv'>x</span> <span class='o'>==</span> <span class='s'>"trans"</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>x</span>, <span class='m'>1</span><span class='o'>)</span> <span class='o'>==</span> <span class='s'>"cool"</span> <span class='o'>&amp;</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lead</a></span><span class='o'>(</span><span class='nv'>x</span>, <span class='m'>1</span><span class='o'>)</span> <span class='o'>==</span> <span class='s'>"cool"</span><span class='o'>)</span>
  <span class='o'>)</span><span class='o'>)</span>
    
  <span class='nv'>event</span><span class='o'>$</span><span class='nv'>values</span><span class='o'>[</span><span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span> <span class='o'>&lt;</span> <span class='m'>3</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='kc'>FALSE</span>
  
  <span class='kr'>if</span> <span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/class.html'>inherits</a></span><span class='o'>(</span><span class='nv'>comm</span>, <span class='s'>"function"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='c'># This will return the lengths of the polar events</span>
    <span class='nv'>lens</span> <span class='o'>&lt;-</span> <span class='kc'>NULL</span>
    <span class='kr'>for</span> <span class='o'>(</span><span class='nv'>i</span> <span class='kr'>in</span> <span class='m'>1</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>{</span>
      <span class='kr'>if</span> <span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>$</span><span class='nv'>values</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='nv'>event</span><span class='o'>$</span><span class='nv'>values</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>{</span>
        <span class='nv'>lens</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>lens</span>, <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, <span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
      <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
        <span class='nv'>lens</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>lens</span>, <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='kc'>NA</span>, <span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
      <span class='o'>}</span>
    <span class='o'>}</span>
    <span class='nv'>lens</span>
  <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
    <span class='c'># This will return summary variables of the polar events</span>
    <span class='nv'>vals</span> <span class='o'>&lt;-</span> <span class='kc'>NULL</span>
    <span class='kr'>for</span> <span class='o'>(</span><span class='nv'>i</span> <span class='kr'>in</span> <span class='m'>1</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>{</span>
      <span class='kr'>if</span> <span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>$</span><span class='nv'>values</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>&amp;</span> <span class='nv'>event</span><span class='o'>$</span><span class='nv'>values</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>{</span>
        <span class='nv'>pull</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='kc'>FALSE</span>, <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='m'>1</span><span class='o'>:</span><span class='o'>(</span><span class='nv'>i</span> <span class='o'>-</span> <span class='m'>1</span><span class='o'>)</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>,
                  <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='kc'>TRUE</span>, <span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
        <span class='nv'>pull</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>pull</span>, <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='kc'>FALSE</span>, <span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nv'>y</span><span class='o'>)</span> <span class='o'>-</span> <span class='nf'><a href='https://rdrr.io/r/base/length.html'>length</a></span><span class='o'>(</span><span class='nv'>pull</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
        <span class='nv'>vals</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>vals</span>, <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='nf'>comm</span><span class='o'>(</span><span class='nv'>y</span><span class='o'>[</span><span class='nv'>pull</span><span class='o'>]</span>, <span class='nv'>...</span><span class='o'>)</span>, <span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
      <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
        <span class='nv'>vals</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nv'>vals</span>, <span class='nf'><a href='https://rdrr.io/r/base/rep.html'>rep</a></span><span class='o'>(</span><span class='kc'>NA</span>, <span class='nv'>event</span><span class='o'>$</span><span class='nv'>lengths</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span><span class='o'>)</span><span class='o'>)</span>
      <span class='o'>}</span>
    <span class='o'>}</span>
    <span class='nv'>vals</span>
  <span class='o'>}</span>
<span class='o'>}</span>
</code></pre>

</div>

Using the above function, I'll "annotate" each day that is part of a polar event with the start date, end date, and length of the cold event. I will also calculate the temperature swing as the change between each day and the previous, i.e. $t_i - t_{i-1}$.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_winter</span> <span class='o'>&lt;-</span> <span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Winter</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Start <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nf'>fx</span><span class='o'>(</span><span class='nv'>Air</span>, <span class='nv'>Date</span>, <span class='nv'>min</span><span class='o'>)</span><span class='o'>)</span>,
         End <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/zoo/man/yearmon.html'>as.Date</a></span><span class='o'>(</span><span class='nf'>fx</span><span class='o'>(</span><span class='nv'>Air</span>, <span class='nv'>Date</span>, <span class='nv'>max</span><span class='o'>)</span><span class='o'>)</span>,
         Length <span class='o'>=</span> <span class='nf'>fx</span><span class='o'>(</span><span class='nv'>Air</span><span class='o'>)</span>,
         MaxSwing <span class='o'>=</span> <span class='nv'>MaxTemp</span> <span class='o'>-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span>, <span class='m'>1</span><span class='o'>)</span>,
         MinSwing <span class='o'>=</span> <span class='nv'>MinTemp</span> <span class='o'>-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>MinTemp</span>, <span class='m'>1</span><span class='o'>)</span>,
         MeanSwing <span class='o'>=</span> <span class='nv'>MeanTemp</span> <span class='o'>-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/lead-lag.html'>lag</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span>, <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

As an example, we can look at what this looks like for the recent cold snap.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Start</span> <span class='o'>==</span> <span class='s'>"2017-12-21"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 18 x 12</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># Groups:   Winter [1]</span></span>
<span class='c'>#&gt;    Date       MaxTemp MinTemp MeanTemp Air   Winter Start      End        Length</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>       </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;fct&gt;</span><span>  </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span> 2017-12-21    -</span><span style='color: #BB0000;'>1.8</span><span>    -</span><span style='color: #BB0000;'>4.7</span><span>     -</span><span style='color: #BB0000;'>3.3</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span> 2017-12-22    -</span><span style='color: #BB0000;'>1.8</span><span>    -</span><span style='color: #BB0000;'>5.7</span><span>     -</span><span style='color: #BB0000;'>3.8</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span> 2017-12-23     0.6    -</span><span style='color: #BB0000;'>4.8</span><span>     -</span><span style='color: #BB0000;'>2.1</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span> 2017-12-24    -</span><span style='color: #BB0000;'>1.1</span><span>    -</span><span style='color: #BB0000;'>4.8</span><span>     -</span><span style='color: #BB0000;'>3</span><span>   cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span> 2017-12-25    -</span><span style='color: #BB0000;'>3</span><span>     -</span><span style='color: #BB0000;'>10.5</span><span>     -</span><span style='color: #BB0000;'>6.8</span><span> trans   </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span> 2017-12-26   -</span><span style='color: #BB0000;'>10.2</span><span>   -</span><span style='color: #BB0000;'>14.4</span><span>    -</span><span style='color: #BB0000;'>12.3</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span> 2017-12-27    -</span><span style='color: #BB0000;'>9.9</span><span>   -</span><span style='color: #BB0000;'>17.9</span><span>    -</span><span style='color: #BB0000;'>13.9</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span> 2017-12-28   -</span><span style='color: #BB0000;'>11.8</span><span>   -</span><span style='color: #BB0000;'>19.6</span><span>    -</span><span style='color: #BB0000;'>15.7</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span> 2017-12-29    -</span><span style='color: #BB0000;'>7.3</span><span>   -</span><span style='color: #BB0000;'>14.3</span><span>    -</span><span style='color: #BB0000;'>10.8</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span> 2017-12-30    -</span><span style='color: #BB0000;'>6.8</span><span>   -</span><span style='color: #BB0000;'>18.1</span><span>    -</span><span style='color: #BB0000;'>12.5</span><span> trans   </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>11</span><span> 2017-12-31   -</span><span style='color: #BB0000;'>13.7</span><span>   -</span><span style='color: #BB0000;'>20.2</span><span>    -</span><span style='color: #BB0000;'>17</span><span>   cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>12</span><span> 2018-01-01    -</span><span style='color: #BB0000;'>7.9</span><span>   -</span><span style='color: #BB0000;'>18.6</span><span>    -</span><span style='color: #BB0000;'>13.3</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>13</span><span> 2018-01-02    -</span><span style='color: #BB0000;'>7.1</span><span>   -</span><span style='color: #BB0000;'>12.5</span><span>     -</span><span style='color: #BB0000;'>9.8</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>14</span><span> 2018-01-03    -</span><span style='color: #BB0000;'>5.3</span><span>   -</span><span style='color: #BB0000;'>11.2</span><span>     -</span><span style='color: #BB0000;'>8.3</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>15</span><span> 2018-01-04    -</span><span style='color: #BB0000;'>7.7</span><span>   -</span><span style='color: #BB0000;'>19.7</span><span>    -</span><span style='color: #BB0000;'>13.7</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>16</span><span> 2018-01-05   -</span><span style='color: #BB0000;'>14.7</span><span>   -</span><span style='color: #BB0000;'>20.6</span><span>    -</span><span style='color: #BB0000;'>17.7</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>17</span><span> 2018-01-06   -</span><span style='color: #BB0000;'>15.4</span><span>   -</span><span style='color: #BB0000;'>22.3</span><span>    -</span><span style='color: #BB0000;'>18.9</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>18</span><span> 2018-01-07    -</span><span style='color: #BB0000;'>1</span><span>     -</span><span style='color: #BB0000;'>17.5</span><span>     -</span><span style='color: #BB0000;'>9.3</span><span> cool    </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 3 more variables: MaxSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, MinSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, MeanSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

I'll wipe out any years with more than 10 missing values (i.e. 1967--1971, which are missing more than a month's worth of data each).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_winter</span><span class='o'>[</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_data.html'>group_indices</a></span><span class='o'>(</span><span class='nv'>tor_winter</span><span class='o'>)</span> <span class='o'>%in%</span> <span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='o'>(</span><span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Winter</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>NAs <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>Air</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span><span class='o'>$</span><span class='nv'>NAs</span> <span class='o'>&gt;</span> <span class='m'>10</span><span class='o'>)</span>, <span class='m'>3</span><span class='o'>:</span><span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>ncol</a></span><span class='o'>(</span><span class='nv'>tor_winter</span><span class='o'>)</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='kc'>NA</span>

<span class='c'>#&gt; `summarise()` ungrouping output (override with `.groups` argument)</span>
</code></pre>

</div>

Let's also make a traditional "seasonal" winter data set.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_winter_means</span> <span class='o'>&lt;-</span> <span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Winter</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Winter</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise_all.html'>summarize_all</a></span><span class='o'>(</span><span class='nv'>mean</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
</code></pre>

</div>

Finally, we can create a summary table detailing the key characteristics of each polar event.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tor_polar_events</span> <span class='o'>&lt;-</span> <span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Winter</span>, <span class='nv'>Start</span>, <span class='nv'>End</span>, <span class='nv'>Length</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>ColdestDay <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>min</a></span><span class='o'>(</span><span class='nv'>MinTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            WarmestDay <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>max</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MaxTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MaxTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MinTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MinTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MeanTemp <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MaxSwing <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>min</a></span><span class='o'>(</span><span class='nv'>MaxSwing</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
            MinSwing <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Extremes.html'>min</a></span><span class='o'>(</span><span class='nv'>MinSwing</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; `summarise()` regrouping output by 'Winter', 'Start', 'End' (override with `.groups` argument)</span>


<span class='c'># Let's also keep track of the proportion of air masses (especially NA)</span>
<span class='nv'>tor_polar_events</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate-joins.html'>left_join</a></span><span class='o'>(</span><span class='nv'>tor_polar_events</span>, <span class='nv'>tor_winter</span> <span class='o'>%&gt;%</span>
                                <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
                                <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>Winter</span>, <span class='nv'>Start</span>, <span class='nv'>End</span>, <span class='nv'>Length</span>, <span class='nv'>Air</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
                                <span class='nv'>tally</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/spread.html'>spread</a></span><span class='o'>(</span><span class='nv'>Air</span>, <span class='nv'>n</span>, fill <span class='o'>=</span> <span class='m'>0</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; Joining, by = c("Winter", "Start", "End", "Length")</span>


<span class='c'># Ungroup these data frames for reshaping</span>
<span class='nv'>tor_polar_events</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>ungroup</a></span><span class='o'>(</span><span class='nv'>tor_polar_events</span><span class='o'>)</span>
</code></pre>

</div>

Great! Now let's see what we can come up with. Let's plot the "big" events in the last 30 years. We'll filter our data for polar events that are 10 days or longer, and that have a mean temperature below 0 °C.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>gg</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/reshape2/man/melt.html'>melt</a></span><span class='o'>(</span><span class='nv'>tor_polar_events</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>Winter</span> <span class='o'>&gt;</span> <span class='m'>1987</span> , <span class='nv'>Length</span> <span class='o'>&gt;=</span> <span class='m'>10</span>, <span class='nv'>MeanTemp</span> <span class='o'>&lt;=</span> <span class='m'>0</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
             <span class='nf'><a href='https://dplyr.tidyverse.org/reference/mutate.html'>mutate</a></span><span class='o'>(</span>Period <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste0</a></span><span class='o'>(</span><span class='nv'>Start</span>, <span class='s'>"-"</span>, <span class='nv'>End</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
             <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nv'>Period</span>, <span class='nv'>Length</span>, <span class='nv'>WarmestDay</span>, <span class='nv'>ColdestDay</span>, <span class='nv'>MaxTemp</span>, <span class='nv'>MinTemp</span>, 
                    <span class='nv'>MeanTemp</span><span class='o'>)</span>, id <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Period"</span>, <span class='s'>"Length"</span><span class='o'>)</span><span class='o'>)</span>

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nv'>gg</span>, <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>Period</span>, y <span class='o'>=</span> <span class='nv'>value</span>, colour <span class='o'>=</span> <span class='nv'>variable</span>, group <span class='o'>=</span> <span class='nv'>Period</span>, lwd <span class='o'>=</span> <span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_path.html'>geom_line</a></span><span class='o'>(</span>alpha <span class='o'>=</span> <span class='m'>.6</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/theme.html'>theme</a></span><span class='o'>(</span>axis.text.x <span class='o'>=</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/element.html'>element_text</a></span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>7</span>, angle <span class='o'>=</span> <span class='m'>90</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ylab</a></span><span class='o'>(</span><span class='s'>"Temperature (°C)"</span><span class='o'>)</span> <span class='o'>+</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>labs</a></span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"Variable"</span>, lwd <span class='o'>=</span> <span class='s'>"Snap Length"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"\"Big\" Polar Events in Toronto since 1988"</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-11-1.png" width="700px" style="display: block; margin: auto;" />

</div>

In the above graph, line width represents polar event length. The bars show the range between the extremes, and averaged $T_{max}$ and $T_{min}$; $T_{mean}$ is where the green and blue bars meet. We can already see that the December 2017 / January 2018 cold snap was among the coldest events in terms of mean temperature, and was relatively long. Let's take a closer look and rank the polar events of the past 30 years.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>last_30_cold</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tor_polar_events</span>, <span class='nv'>Winter</span> <span class='o'>&gt;</span> <span class='m'>1988</span><span class='o'>)</span>
<span class='nv'>last_30_tor</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>tor_winter_means</span>, <span class='nv'>Winter</span> <span class='o'>&gt;</span> <span class='m'>1988</span><span class='o'>)</span>
</code></pre>

</div>

These are the top 10 coldest winters (in terms of $T_{mean}$). This year is ranked 4, but we can't take that number at face value because the series is just over one third of the length of other winters and was dominated by cold temperatures. The last few days have probably done a great deal to undo that, but any data after January 9th are omitted from this analysis!

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>last_30_tor</span>, <span class='nv'>MeanTemp</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 30 x 4</span></span>
<span class='c'>#&gt;    Winter MaxTemp MinTemp MeanTemp</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994  -</span><span style='color: #BB0000;'>2.50</span><span>    -</span><span style='color: #BB0000;'>8.50</span><span>    -</span><span style='color: #BB0000;'>5.51</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014  -</span><span style='color: #BB0000;'>2.25</span><span>    -</span><span style='color: #BB0000;'>8.56</span><span>    -</span><span style='color: #BB0000;'>5.49</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>2</span><span>015  -</span><span style='color: #BB0000;'>1.83</span><span>    -</span><span style='color: #BB0000;'>8.77</span><span>    -</span><span style='color: #BB0000;'>5.35</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>2</span><span>018  -</span><span style='color: #BB0000;'>2.02</span><span>    -</span><span style='color: #BB0000;'>8.50</span><span>    -</span><span style='color: #BB0000;'>5.27</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>1</span><span>996  -</span><span style='color: #BB0000;'>0.901</span><span>   -</span><span style='color: #BB0000;'>6.94</span><span>    -</span><span style='color: #BB0000;'>3.93</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>2</span><span>011  -</span><span style='color: #BB0000;'>0.778</span><span>   -</span><span style='color: #BB0000;'>7.04</span><span>    -</span><span style='color: #BB0000;'>3.93</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>2</span><span>003  -</span><span style='color: #BB0000;'>1.05</span><span>    -</span><span style='color: #BB0000;'>6.73</span><span>    -</span><span style='color: #BB0000;'>3.89</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>2</span><span>009   0.07    -</span><span style='color: #BB0000;'>7.37</span><span>    -</span><span style='color: #BB0000;'>3.66</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>1</span><span>990  -</span><span style='color: #BB0000;'>0.159</span><span>   -</span><span style='color: #BB0000;'>6.03</span><span>    -</span><span style='color: #BB0000;'>3.11</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>2</span><span>005   0.387   -</span><span style='color: #BB0000;'>6.54</span><span>    -</span><span style='color: #BB0000;'>3.08</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 20 more rows</span></span>
</code></pre>

</div>

What about longest polar events? This recent snap was ranked 8.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>last_30_cold</span>, <span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>Length</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 171 x 14</span></span>
<span class='c'>#&gt;    Winter Start      End        Length ColdestDay WarmestDay MaxTemp MinTemp</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>1</span><span>990 1989-12-26 1990-01-19     25      -</span><span style='color: #BB0000;'>22.2</span><span>        5.1   -</span><span style='color: #BB0000;'>5.02</span><span>  -</span><span style='color: #BB0000;'>10.8</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1994-01-02 1994-01-26     25      -</span><span style='color: #BB0000;'>25.9</span><span>        2.6   -</span><span style='color: #BB0000;'>7.25</span><span>  -</span><span style='color: #BB0000;'>14.1</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>2</span><span>006 2005-12-23 2006-01-13     22      -</span><span style='color: #BB0000;'>12.9</span><span>        3     -</span><span style='color: #BB0000;'>0.9</span><span>    -</span><span style='color: #BB0000;'>6.03</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>2</span><span>011 2010-12-06 2010-12-26     21      -</span><span style='color: #BB0000;'>12.6</span><span>        4.5   -</span><span style='color: #BB0000;'>1.71</span><span>   -</span><span style='color: #BB0000;'>6.56</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>2</span><span>005 2005-01-15 2005-02-03     20      -</span><span style='color: #BB0000;'>22.7</span><span>        2.6   -</span><span style='color: #BB0000;'>5.10</span><span>  -</span><span style='color: #BB0000;'>13.0</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1994-01-29 1994-02-16     19      -</span><span style='color: #BB0000;'>20</span><span>          3.3   -</span><span style='color: #BB0000;'>6.03</span><span>  -</span><span style='color: #BB0000;'>12.5</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>2</span><span>001 2000-12-18 2001-01-05     19      -</span><span style='color: #BB0000;'>16.6</span><span>        2.4   -</span><span style='color: #BB0000;'>3.93</span><span>   -</span><span style='color: #BB0000;'>9.84</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18      -</span><span style='color: #BB0000;'>22.3</span><span>        0.6   -</span><span style='color: #BB0000;'>6.99</span><span>  -</span><span style='color: #BB0000;'>14.3</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>2</span><span>011 2011-01-19 2011-02-04     17      -</span><span style='color: #BB0000;'>18.5</span><span>        0.7   -</span><span style='color: #BB0000;'>3.89</span><span>  -</span><span style='color: #BB0000;'>10.1</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-02-02 2014-02-18     17      -</span><span style='color: #BB0000;'>14.2</span><span>        0.9   -</span><span style='color: #BB0000;'>4.28</span><span>  -</span><span style='color: #BB0000;'>11.0</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 161 more rows, and 6 more variables: MeanTemp </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, MaxSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   MinSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, cool </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, trans </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, `&lt;NA&gt;` </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

How about when we look at the coldest polar event in terms of one-day temperature extremes? This year's event was ranked 5.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>last_30_cold</span>, <span class='nv'>ColdestDay</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 171 x 14</span></span>
<span class='c'>#&gt;    Winter Start      End        Length ColdestDay WarmestDay MaxTemp MinTemp</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1994-01-02 1994-01-26     25      -</span><span style='color: #BB0000;'>25.9</span><span>        2.6   -</span><span style='color: #BB0000;'>7.25</span><span>   -</span><span style='color: #BB0000;'>14.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>2</span><span>015 2015-02-15 2015-02-23      9      -</span><span style='color: #BB0000;'>25.1</span><span>       -</span><span style='color: #BB0000;'>2.7</span><span>  -</span><span style='color: #BB0000;'>10.5</span><span>    -</span><span style='color: #BB0000;'>18.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>2</span><span>016 2016-02-10 2016-02-14      5      -</span><span style='color: #BB0000;'>24.7</span><span>       -</span><span style='color: #BB0000;'>0.1</span><span>   -</span><span style='color: #BB0000;'>7.28</span><span>   -</span><span style='color: #BB0000;'>17.6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>2</span><span>005 2005-01-15 2005-02-03     20      -</span><span style='color: #BB0000;'>22.7</span><span>        2.6   -</span><span style='color: #BB0000;'>5.10</span><span>   -</span><span style='color: #BB0000;'>13.0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18      -</span><span style='color: #BB0000;'>22.3</span><span>        0.6   -</span><span style='color: #BB0000;'>6.99</span><span>   -</span><span style='color: #BB0000;'>14.3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>1</span><span>990 1989-12-26 1990-01-19     25      -</span><span style='color: #BB0000;'>22.2</span><span>        5.1   -</span><span style='color: #BB0000;'>5.02</span><span>   -</span><span style='color: #BB0000;'>10.8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-01-07 2014-01-09      3      -</span><span style='color: #BB0000;'>22.2</span><span>       -</span><span style='color: #BB0000;'>2.9</span><span>   -</span><span style='color: #BB0000;'>8.8</span><span>    -</span><span style='color: #BB0000;'>16.7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>2</span><span>004 2004-01-05 2004-01-12      8      -</span><span style='color: #BB0000;'>22.1</span><span>        0.7   -</span><span style='color: #BB0000;'>4.82</span><span>   -</span><span style='color: #BB0000;'>12.1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>2</span><span>004 2004-01-15 2004-01-20      6      -</span><span style='color: #BB0000;'>21.4</span><span>       -</span><span style='color: #BB0000;'>2</span><span>     -</span><span style='color: #BB0000;'>7.67</span><span>   -</span><span style='color: #BB0000;'>15.7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>1</span><span>995 1995-02-02 1995-02-14     13      -</span><span style='color: #BB0000;'>21.2</span><span>        1.6   -</span><span style='color: #BB0000;'>5.57</span><span>   -</span><span style='color: #BB0000;'>12.9</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 161 more rows, and 6 more variables: MeanTemp </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, MaxSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   MinSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, cool </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, trans </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, `&lt;NA&gt;` </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

Which were the coldest polar events (in terms of $T_{mean}$)? The polar event this year is ranked 10.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>last_30_cold</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>`ColdestDay`</span>, <span class='o'>-</span><span class='nv'>`WarmestDay`</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>MeanTemp</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 171 x 12</span></span>
<span class='c'>#&gt;    Winter Start      End        Length MaxTemp MinTemp MeanTemp MaxSwing</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>2</span><span>015 2015-02-15 2015-02-23      9  -</span><span style='color: #BB0000;'>10.5</span><span>    -</span><span style='color: #BB0000;'>18.1</span><span>    -</span><span style='color: #BB0000;'>14.3</span><span>   -</span><span style='color: #BB0000;'>13.5</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-01-07 2014-01-09      3   -</span><span style='color: #BB0000;'>8.8</span><span>    -</span><span style='color: #BB0000;'>16.7</span><span>    -</span><span style='color: #BB0000;'>12.8</span><span>   -</span><span style='color: #BB0000;'>18.2</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>2</span><span>016 2016-02-10 2016-02-14      5   -</span><span style='color: #BB0000;'>7.28</span><span>   -</span><span style='color: #BB0000;'>17.6</span><span>    -</span><span style='color: #BB0000;'>12.5</span><span>   -</span><span style='color: #BB0000;'>10.9</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-01-26 2014-01-29      4   -</span><span style='color: #BB0000;'>7.07</span><span>   -</span><span style='color: #BB0000;'>16.4</span><span>    -</span><span style='color: #BB0000;'>11.8</span><span>    -</span><span style='color: #BB0000;'>7.9</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>2</span><span>004 2004-01-15 2004-01-20      6   -</span><span style='color: #BB0000;'>7.67</span><span>   -</span><span style='color: #BB0000;'>15.7</span><span>    -</span><span style='color: #BB0000;'>11.7</span><span>    -</span><span style='color: #BB0000;'>5.7</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>2</span><span>015 2015-02-26 2015-02-28      3   -</span><span style='color: #BB0000;'>7.23</span><span>   -</span><span style='color: #BB0000;'>15.8</span><span>    -</span><span style='color: #BB0000;'>11.6</span><span>    -</span><span style='color: #BB0000;'>1.90</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2013-12-30 2014-01-02      4   -</span><span style='color: #BB0000;'>8.78</span><span>   -</span><span style='color: #BB0000;'>13.2</span><span>    -</span><span style='color: #BB0000;'>11.0</span><span>   -</span><span style='color: #BB0000;'>12.2</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-01-18 2014-01-23      6   -</span><span style='color: #BB0000;'>7.17</span><span>   -</span><span style='color: #BB0000;'>14.7</span><span>    -</span><span style='color: #BB0000;'>11.0</span><span>   -</span><span style='color: #BB0000;'>12.4</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1994-01-02 1994-01-26     25   -</span><span style='color: #BB0000;'>7.25</span><span>   -</span><span style='color: #BB0000;'>14.1</span><span>    -</span><span style='color: #BB0000;'>10.7</span><span>   -</span><span style='color: #BB0000;'>11.4</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>2</span><span>018 2017-12-21 2018-01-07     18   -</span><span style='color: #BB0000;'>6.99</span><span>   -</span><span style='color: #BB0000;'>14.3</span><span>    -</span><span style='color: #BB0000;'>10.7</span><span>    -</span><span style='color: #BB0000;'>7.2</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 161 more rows, and 4 more variables: MinSwing </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, cool </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>,</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>#   trans </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span style='color: #555555;'>, `&lt;NA&gt;` </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

Now let's look at the largest one-day temperature change in $T_{max}$. The event this year is ranked 80.)

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>last_30_cold</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>`ColdestDay`</span>, <span class='o'>-</span><span class='nv'>`WarmestDay`</span>, <span class='o'>-</span><span class='nv'>`MinTemp`</span>, <span class='o'>-</span><span class='nv'>`MeanTemp`</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
<span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>MaxSwing</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 171 x 10</span></span>
<span class='c'>#&gt;    Winter Start      End        Length MaxTemp MaxSwing MinSwing  cool trans</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>2</span><span>014 2014-01-07 2014-01-09      3  -</span><span style='color: #BB0000;'>8.8</span><span>      -</span><span style='color: #BB0000;'>18.2</span><span>    -</span><span style='color: #BB0000;'>6.40</span><span>     3     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>1</span><span>993 1993-02-06 1993-02-09      4  -</span><span style='color: #BB0000;'>4.55</span><span>     -</span><span style='color: #BB0000;'>18</span><span>     -</span><span style='color: #BB0000;'>20.7</span><span>      3     1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1993-12-11 1993-12-13      3  -</span><span style='color: #BB0000;'>0.467</span><span>    -</span><span style='color: #BB0000;'>17.3</span><span>   -</span><span style='color: #BB0000;'>15.6</span><span>      3     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>1</span><span>991 1991-01-21 1991-01-26      6  -</span><span style='color: #BB0000;'>5.48</span><span>     -</span><span style='color: #BB0000;'>17.1</span><span>   -</span><span style='color: #BB0000;'>14.5</span><span>      6     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>2</span><span>000 2000-01-17 2000-01-31     15  -</span><span style='color: #BB0000;'>5.22</span><span>     -</span><span style='color: #BB0000;'>16</span><span>     -</span><span style='color: #BB0000;'>12.5</span><span>     12     3</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>2</span><span>006 2006-02-18 2006-02-20      3  -</span><span style='color: #BB0000;'>4.83</span><span>     -</span><span style='color: #BB0000;'>14.8</span><span>    -</span><span style='color: #BB0000;'>6.7</span><span>      3     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>1</span><span>996 1996-02-12 1996-02-18      7  -</span><span style='color: #BB0000;'>5.2</span><span>      -</span><span style='color: #BB0000;'>14.4</span><span>   -</span><span style='color: #BB0000;'>13</span><span>        7     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>1</span><span>989 1989-02-02 1989-02-10      9  -</span><span style='color: #BB0000;'>4.89</span><span>     -</span><span style='color: #BB0000;'>14.1</span><span>    -</span><span style='color: #BB0000;'>6.1</span><span>      9     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>1</span><span>995 1995-02-02 1995-02-14     13  -</span><span style='color: #BB0000;'>5.57</span><span>     -</span><span style='color: #BB0000;'>13.9</span><span>   -</span><span style='color: #BB0000;'>15.1</span><span>     13     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>1</span><span>992 1992-01-15 1992-01-21      7  -</span><span style='color: #BB0000;'>5.94</span><span>     -</span><span style='color: #BB0000;'>13.8</span><span>    -</span><span style='color: #BB0000;'>5.7</span><span>      7     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 161 more rows, and 1 more variable: `&lt;NA&gt;` </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

Finally, we'll look at the largest one-day temperature change in $T_{min}$. This year's cold snap ranks 39.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>last_30_cold</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='o'>-</span><span class='nv'>`ColdestDay`</span>, <span class='o'>-</span><span class='nv'>`WarmestDay`</span>, <span class='o'>-</span><span class='nv'>`MaxTemp`</span>, <span class='o'>-</span><span class='nv'>`MeanTemp`</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nv'>MinSwing</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 171 x 10</span></span>
<span class='c'>#&gt;    Winter Start      End        Length MinTemp MaxSwing MinSwing  cool trans</span>
<span class='c'>#&gt;     <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>     </span><span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span>   </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span>   </span><span style='text-decoration: underline;'>1</span><span>993 1993-02-06 1993-02-09      4  -</span><span style='color: #BB0000;'>13.7</span><span>     -</span><span style='color: #BB0000;'>18</span><span>      -</span><span style='color: #BB0000;'>20.7</span><span>     3     1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span>   </span><span style='text-decoration: underline;'>1</span><span>994 1993-12-11 1993-12-13      3   -</span><span style='color: #BB0000;'>7.07</span><span>    -</span><span style='color: #BB0000;'>17.3</span><span>    -</span><span style='color: #BB0000;'>15.6</span><span>     3     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span>   </span><span style='text-decoration: underline;'>1</span><span>995 1995-02-02 1995-02-14     13  -</span><span style='color: #BB0000;'>12.9</span><span>     -</span><span style='color: #BB0000;'>13.9</span><span>    -</span><span style='color: #BB0000;'>15.1</span><span>    13     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span>   </span><span style='text-decoration: underline;'>1</span><span>991 1991-01-21 1991-01-26      6  -</span><span style='color: #BB0000;'>12.7</span><span>     -</span><span style='color: #BB0000;'>17.1</span><span>    -</span><span style='color: #BB0000;'>14.5</span><span>     6     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span>   </span><span style='text-decoration: underline;'>1</span><span>989 1989-02-22 1989-02-28      7   -</span><span style='color: #BB0000;'>8.5</span><span>     -</span><span style='color: #BB0000;'>11.1</span><span>    -</span><span style='color: #BB0000;'>14.3</span><span>     6     1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span>   </span><span style='text-decoration: underline;'>1</span><span>997 1997-01-26 1997-01-31      6  -</span><span style='color: #BB0000;'>12.0</span><span>      -</span><span style='color: #BB0000;'>8.1</span><span>    -</span><span style='color: #BB0000;'>13.6</span><span>     4     2</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span>   </span><span style='text-decoration: underline;'>1</span><span>989 1989-01-01 1989-01-06      6   -</span><span style='color: #BB0000;'>9.33</span><span>     -</span><span style='color: #BB0000;'>6.8</span><span>    -</span><span style='color: #BB0000;'>13.6</span><span>     6     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span>   </span><span style='text-decoration: underline;'>1</span><span>991 1991-02-23 1991-02-28      6   -</span><span style='color: #BB0000;'>7.77</span><span>     -</span><span style='color: #BB0000;'>7.1</span><span>    -</span><span style='color: #BB0000;'>13.5</span><span>     6     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span>   </span><span style='text-decoration: underline;'>1</span><span>993 1992-12-21 1992-12-27      7   -</span><span style='color: #BB0000;'>6.26</span><span>     -</span><span style='color: #BB0000;'>6.5</span><span>    -</span><span style='color: #BB0000;'>13.5</span><span>     6     1</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span>   </span><span style='text-decoration: underline;'>1</span><span>996 1996-02-12 1996-02-18      7  -</span><span style='color: #BB0000;'>12.3</span><span>     -</span><span style='color: #BB0000;'>14.4</span><span>    -</span><span style='color: #BB0000;'>13</span><span>       7     0</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 161 more rows, and 1 more variable: `&lt;NA&gt;` </span><span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span></span>
</code></pre>

</div>

Conclusions
-----------

The cold snap this year was ranked highly in terms of length, mean temperature and coldest day, but not very highly when we consider the one-day temperature change. Indeed, it was cold, but the low temperatures arrived slowly, not suddenly. It is interesting to note that among the top polar events, we see a number in recent years. While polar events since 2000 have been cold, it seems that extreme temperature swings were more common in the 1990s. We see a handul of cold events from winter 1993/94 and the two winters that we examined in Anderson and Gough ([2017](#ref-Anderson_2017_Evolution)) among the coldest and longest. I should flag that these rankings vary based on how many days of transitional air we include in the analysis, but the coldest events are relatively consistent and concentrated in the early 1990s and recent 2010s.

------------------------------------------------------------------------

*This post was compiled on 2020-10-09 11:13:43. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package     * version    date       lib source                             </span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  canadaHCD   * 0.0-2      2020-10-01 [1] Github (ConorIA/canadaHCD@e2f4d2b) </span>
<span class='c'>#&gt;  canadaHCDx  * 0.0.8      2020-10-01 [1] gitlab (ConorIA/canadaHCDx@0f99419)</span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  downlit       0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  dplyr       * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  fs            1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  geosphere     1.5-10     2019-05-26 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  glue          1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)    </span>
<span class='c'>#&gt;  knitr         1.30       2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  labeling      0.3        2014-08-23 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  lubridate   * 1.7.9      2020-06-08 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  pillar        1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  plyr          1.8.6      2020-03-03 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  purrr         0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  rappdirs      0.3.1      2016-03-28 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  Rcpp          1.0.5      2020-07-06 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  readr       * 1.3.1      2018-12-21 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  reshape2    * 1.4.4      2020-04-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rlang         0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  rmarkdown     2.3        2020-06-18 [1] RSPM (R 4.0.1)                     </span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  sp            1.4-2      2020-05-20 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  storr         1.2.1      2018-10-18 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  stringi       1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  stringr       1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  tibble      * 3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyr       * 1.1.2      2020-08-27 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  vctrs         0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  withr         2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  xfun          0.18       2020-09-29 [2] RSPM (R 4.0.2)                     </span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt;  zoo         * 1.8-8      2020-05-02 [1] RSPM (R 4.0.0)                     </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

References
----------

<div id="refs" class="references">

<div id="ref-Anderson_2017_Evolution">

Anderson, Conor I., and William A. Gough. 2017. "Evolution of Winter Temperature in Toronto, Ontario, Canada: A Case Study of Winters 2013/14 and 2014/15." *Journal of Climate* 30 (14): 5361--76. <https://doi.org/10.1175/JCLI-D-16-0562.1>.

</div>

<div id="ref-Sheridan_2002_redevelopment">

Sheridan, Scott C. 2002. "The Redevelopment of a Weather-Type Classification Scheme for North America." *International Journal of Climatology* 22 (1): 51--68. <https://doi.org/10.1002/joc.709>.

</div>

</div>

