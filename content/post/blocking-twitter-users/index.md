---
output: hugodown::md_document
title: "Blocking Twitter users from R"
slug: "blocking-twitter-users"
subtitle: ""
summary: "Making Twitter a little more pleasant to peruse"
authors: []
categories:
  - R
tags:
  - Twitter
  - bots
date: 2020-10-11
lastmod: 2020-10-11
rmd_hash: fcbcc8445db3a336

---

Last year, I undertook a brief [analysis](https://conr.ca/post/twitter-abuse-environment-minister/) of abusive tweets directed at Canada's (then) Minister of the Environment and Climate Change, Catherine McKenna. Twitter can be a really great platform for the exchange of information, but it can also be a cesspool of hate and negativity. I'm not an important person, so I can usually avoid the bots and the trolls fairly easily (with the exception of the occasional dust-up with climate change deniers). However, many more important climate scientists, and of course, the political class, are less likely to avoid abusive replies.

While I am totally on board with the right to free expression, I am also a proponent of the right to not have to listen to jerks. I was curious to see whether I could use R to find and block people that I don't have any interest in hearing from on Twitter.\[^Whether they be be "bots", or just humans with whom I have no desire to interact.\] I will accomplish this goal with the help of the awesome **rtweet** package, from the [rOpenSci](https://ropensci.org/) community. I'll also use a few packages from the tidyverse, and **httr**.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://CRAN.R-project.org/package=rtweet'>rtweet</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://httr.r-lib.org/'>httr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://tibble.tidyverse.org/'>tibble</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://ggplot2.tidyverse.org'>ggplot2</a></span><span class='o'>)</span>
</code></pre>

</div>

You can do lots of things with **rtweet** without needing Twitter developer credentials, however, we're going to need to perform some "post" actions, which means that we will require our own login token. I won't go into the details of obtaining these details and creating the token, because the **rtweet** authors have already done a great job of documenting those steps. Check out the "auth" vignette, by typing:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/vignette.html'>vignette</a></span><span class='o'>(</span><span class='s'>"auth"</span><span class='o'>)</span>
</code></pre>

</div>

Once you have followed the instructions in the "auth" vignette, you should have a working token that will allow you to use the post endpoints of the Twitter API to modify your account.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>token</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/rtweet/man/get_tokens.html'>get_token</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Identifying users to block
--------------------------

There are surely myriad ways of identifying antagonistic users on Twitter, but an easy way to find them is to look at popular hashtags. This isn't a perfect solution. For instance, a search for "\#maga" turn up a lot of folks who have "reclaimed" the hashtag to tick off Trump supporters, so, you could end up blocking folks you could be interested in hearing from. In this blog post, we'll use a slightly more niche hashtag, based on last year's look into Catherine McKenna's Twitter experience. It seems like her new portfolio in infrastructure has not brought relief from jerks on Twitter. She is still the target of consistent pestering on Twitter in the form of the "\#wheresthemoneycatherine" hashtag.

Let's search for tweets using our hashtag.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/rtweet/man/search_tweets.html'>search_tweets</a></span><span class='o'>(</span><span class='s'>"#wheresthemoneycatherine"</span>, n <span class='o'>=</span> <span class='m'>1000</span>, include_rts <span class='o'>=</span> <span class='kc'>FALSE</span>,
                        verbose <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span>
</code></pre>

</div>

The [`search_tweets()`](https://rdrr.io/pkg/rtweet/man/search_tweets.html) function uses Twitter's free [search](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/search/overview/standard) API endpoint, so it only returns data from the last 7 days. It can also return up to 18,000 statuses, but I have opted to limit my search to 1000. There is a *lot* of information returned by the API.

<details>

<summary>Click here for a sample of the data.</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://tibble.tidyverse.org/reference/glimpse.html'>glimpse</a></span><span class='o'>(</span><span class='nv'>tweets</span><span class='o'>)</span>

<span class='c'>#&gt; Rows: 462</span>
<span class='c'>#&gt; Columns: 90</span>
<span class='c'>#&gt; $ user_id                 <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "23505313", "2817035628", "2817035628", "2817…</span></span>
<span class='c'>#&gt; $ status_id               <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "1315663302102376448", "1315662762765414400",…</span></span>
<span class='c'>#&gt; $ created_at              <span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span> 2020-10-12 14:39:09, 2020-10-12 14:37:01, 20…</span></span>
<span class='c'>#&gt; $ screen_name             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "SwimCoachClint", "overthemoonmark", "overthe…</span></span>
<span class='c'>#&gt; $ text                    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "@cathmckenna Today is Monday, October 12, 20…</span></span>
<span class='c'>#&gt; $ source                  <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Twitter for Android", "Twitter for iPhone", …</span></span>
<span class='c'>#&gt; $ display_text_width      <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 238, 54, 54, 54, 213, 183, 105, 213, 274, 280…</span></span>
<span class='c'>#&gt; $ reply_to_status_id      <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "1315384462239244294", "1315407437336252416",…</span></span>
<span class='c'>#&gt; $ reply_to_user_id        <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "140252240", "1199445363691646976", "94322696…</span></span>
<span class='c'>#&gt; $ reply_to_screen_name    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "cathmckenna", "Krisster81", "will_bouw", "Em…</span></span>
<span class='c'>#&gt; $ is_quote                <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FAL…</span></span>
<span class='c'>#&gt; $ is_retweet              <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FAL…</span></span>
<span class='c'>#&gt; $ favorite_count          <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 0, 0, 0, 0, 4, 7, 0, 0, 0, 1, 3, 0, 0, 0, 2, …</span></span>
<span class='c'>#&gt; $ retweet_count           <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 3, …</span></span>
<span class='c'>#&gt; $ quote_count             <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ reply_count             <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ hashtags                <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> ["WheresTheMoneyCatherine", &lt;"TrudeauCorrupt…</span></span>
<span class='c'>#&gt; $ symbols                 <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ urls_url                <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ urls_t.co               <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ urls_expanded_url       <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ media_url               <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ media_t.co              <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ media_expanded_url      <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ media_type              <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ ext_media_url           <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ ext_media_t.co          <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ ext_media_expanded_url  <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,…</span></span>
<span class='c'>#&gt; $ ext_media_type          <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ mentions_user_id        <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [&lt;"140252240", "140252240"&gt;, &lt;"1199445363691…</span></span>
<span class='c'>#&gt; $ mentions_screen_name    <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [&lt;"cathmckenna", "cathmckenna"&gt;, &lt;"Krisster8…</span></span>
<span class='c'>#&gt; $ lang                    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "en", "und", "und", "und", "en", "en", "und",…</span></span>
<span class='c'>#&gt; $ quoted_status_id        <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_text             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_created_at       <span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, …</span></span>
<span class='c'>#&gt; $ quoted_source           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_favorite_count   <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_retweet_count    <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_user_id          <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_screen_name      <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_name             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_followers_count  <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_friends_count    <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_statuses_count   <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_location         <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_description      <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ quoted_verified         <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_status_id       <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_text            <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_created_at      <span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, …</span></span>
<span class='c'>#&gt; $ retweet_source          <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_favorite_count  <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_retweet_count   <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_user_id         <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_screen_name     <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_name            <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_followers_count <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_friends_count   <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_statuses_count  <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_location        <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_description     <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ retweet_verified        <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ place_url               <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ place_name              <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ place_full_name         <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ place_type              <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ country                 <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ country_code            <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ geo_coords              <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [&lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA…</span></span>
<span class='c'>#&gt; $ coords_coords           <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [&lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA, NA&gt;, &lt;NA…</span></span>
<span class='c'>#&gt; $ bbox_coords             <span style='color: #555555;font-style: italic;'>&lt;list&gt;</span><span> [&lt;NA, NA, NA, NA, NA, NA, NA, NA&gt;, &lt;NA, NA, …</span></span>
<span class='c'>#&gt; $ status_url              <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "https://twitter.com/SwimCoachClint/status/13…</span></span>
<span class='c'>#&gt; $ name                    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Coach Clint", "Noodles", "Noodles", "Noodles…</span></span>
<span class='c'>#&gt; $ location                <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Canada", "Toronto, Ontario", "Toronto, Ontar…</span></span>
<span class='c'>#&gt; $ description             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Have strong opinions. I am in protest of my …</span></span>
<span class='c'>#&gt; $ url                     <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ protected               <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FAL…</span></span>
<span class='c'>#&gt; $ followers_count         <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 552, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, …</span></span>
<span class='c'>#&gt; $ friends_count           <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 962, 153, 153, 153, 153, 153, 153, 153, 153, …</span></span>
<span class='c'>#&gt; $ listed_count            <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, …</span></span>
<span class='c'>#&gt; $ statuses_count          <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 2233, 8006, 8006, 8006, 8006, 8006, 8006, 800…</span></span>
<span class='c'>#&gt; $ favourites_count        <span style='color: #555555;font-style: italic;'>&lt;int&gt;</span><span> 1182, 6998, 6998, 6998, 6998, 6998, 6998, 699…</span></span>
<span class='c'>#&gt; $ account_created_at      <span style='color: #555555;font-style: italic;'>&lt;dttm&gt;</span><span> 2009-03-09 21:30:46, 2014-09-18 13:48:02, 20…</span></span>
<span class='c'>#&gt; $ verified                <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FAL…</span></span>
<span class='c'>#&gt; $ profile_url             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ profile_expanded_url    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ account_lang            <span style='color: #555555;font-style: italic;'>&lt;lgl&gt;</span><span> NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N…</span></span>
<span class='c'>#&gt; $ profile_banner_url      <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "https://pbs.twimg.com/profile_banners/235053…</span></span>
<span class='c'>#&gt; $ profile_background_url  <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "http://abs.twimg.com/images/themes/theme1/bg…</span></span>
<span class='c'>#&gt; $ profile_image_url       <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "http://pbs.twimg.com/profile_images/13006486…</span></span>
</code></pre>

</div>

</details>

------------------------------------------------------------------------

So, are any of these users actually bots? Since we know that the API only returns data from the last 7 days, we might assume that any user that is repeating the same hashtag multiple times in that period is a bot. Let's count the number of instances that each account uses the hashtag.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tweets</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/group_by.html'>group_by</a></span><span class='o'>(</span><span class='nv'>screen_name</span>, <span class='nv'>user_id</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/summarise.html'>summarize</a></span><span class='o'>(</span>n <span class='o'>=</span> <span class='nf'><a href='https://dplyr.tidyverse.org/reference/context.html'>n</a></span><span class='o'>(</span><span class='o'>)</span>, .groups <span class='o'>=</span> <span class='s'>"drop"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/arrange.html'>arrange</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/desc.html'>desc</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>counts</span>

<span class='nv'>counts</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 220 x 3</span></span>
<span class='c'>#&gt;    screen_name     user_id                 n</span>
<span class='c'>#&gt;    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>           </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>               </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 1</span><span> wilson_the_dogg 3380224119             34</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 2</span><span> overthemoonmark 2817035628             32</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 3</span><span> OttawaGordon    278770541              31</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 4</span><span> Dphitchcock     815602484123144193      8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 5</span><span> KRS61           292091883               8</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 6</span><span> BlushingBelles  1228719819974832128     7</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 7</span><span> don_unka        1090381795256864768     6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 8</span><span> RlHilts         2850783528              6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'> 9</span><span> spock246        1045546682              6</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>10</span><span> bkruhlak        444269422               5</span></span>
<span class='c'>#&gt; <span style='color: #555555;'># … with 210 more rows</span></span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='nv'>counts</span>, <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_density.html'>geom_density</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>xintercept<span class='o'>=</span><span class='nf'><a href='https://rdrr.io/r/stats/median.html'>median</a></span><span class='o'>(</span><span class='nv'>n</span><span class='o'>)</span><span class='o'>)</span>,
            colour <span class='o'>=</span> <span class='s'>"blue"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/geom_abline.html'>geom_vline</a></span><span class='o'>(</span><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/aes.html'>aes</a></span><span class='o'>(</span>xintercept<span class='o'>=</span><span class='nf'><a href='https://rdrr.io/r/stats/quantile.html'>quantile</a></span><span class='o'>(</span><span class='nv'>n</span>, <span class='m'>0.95</span><span class='o'>)</span><span class='o'>)</span>,
            colour <span class='o'>=</span> <span class='s'>"red"</span>, linetype <span class='o'>=</span> <span class='s'>"dashed"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/labs.html'>ggtitle</a></span><span class='o'>(</span><span class='s'>"Density of number of tweets containing \"wheresthemoneycatherine\" by user"</span>,
          subtitle <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste</a></span><span class='o'>(</span><span class='s'>"Median (blue) ="</span>, <span class='nf'><a href='https://rdrr.io/r/stats/median.html'>median</a></span><span class='o'>(</span><span class='nv'>counts</span><span class='o'>$</span><span class='nv'>n</span><span class='o'>)</span>,
                           <span class='s'>"/ 95th percentile (red) ="</span>, <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/stats/quantile.html'>quantile</a></span><span class='o'>(</span><span class='nv'>counts</span><span class='o'>$</span><span class='nv'>n</span>, <span class='m'>0.95</span><span class='o'>)</span>, <span class='m'>3</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>

</code></pre>
<img src="figs/unnamed-chunk-7-1.png" width="700px" style="display: block; margin: auto;" />

</div>

We can see that the vast majority of tweeters used the hashtag only once in our search history, and almost all of them could count the number of times they tweeted the hashtag on one hand. However, a handful of users used the hashtag dozens of times.

It might be enough to assume that a user that is using the same hashtag dozens of times in the space of a week is a bot, or at least an agitator, but if you want to be really sure before blocking the user, you can try a bot-checking API.

Optional: Using a bot-checking API
----------------------------------

[Botometer](https://botometer.osome.iu.edu/) is a tool to check whether a Twitter user is likely to be a bot, or just a regular user. Botometer offers an API, which has been implemented in the [**botcheck**](https://github.com/marsha5813/botcheck) package. **botcheck** has not been updated for a number of years. I'll admit that I didn't try the package, and it may still work, but I decided to emulate the functionality of the package in this post.

First, we will need an API key for the Botometer API. Sign up for a [RapidAPI](https://rapidapi.com/) account and [subscribe](https://rapidapi.com/OSoMe/api/botometer-pro/pricing) (for free) to the Botometer API, and you will be able to see your key in the example code provided on the [endpoints](https://rapidapi.com/OSoMe/api/botometer-pro/endpoints) page.

I have saved my RapidAPI key to an environment variable called `RAPIDAPI_KEY`.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>RAPIDAPI_KEY</span> <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Sys.getenv.html'>Sys.getenv</a></span><span class='o'>(</span><span class='s'>"RAPIDAPI_KEY"</span><span class='o'>)</span>
</code></pre>

</div>

Now let's reproduce the `botcheck()` function from the **botcheck** package. The Botometer API asks for request body that contains user information, the user's timeline, and tweets that mention the user. Let's collect those details from Twitter. For this example, we can use our top-ranked tweeter, who happens to be wilson\_the\_dogg, with user ID 3380224119, who tweeted the hashtag 34 times.

### Request Twitter data

The **rtweet** package doesn't seem to have a function that accesses the [`users/show`](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-users-show) endpoint, and the other functions convert the JSON to an R object. When I converted to information back to JSON, I couldn't get it to play nice with the Botometer API. Rather than using **rtweet** here, we'll create our own little function to request the necessary data from Twitter, based on the `botcheck()` function.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>twitter_data</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>type</span>, <span class='nv'>screen_name</span>, <span class='nv'>user_id</span>, <span class='nv'>token</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='nv'>url</span> <span class='o'>&lt;-</span> <span class='kr'><a href='https://rdrr.io/r/base/switch.html'>switch</a></span><span class='o'>(</span><span class='nv'>type</span>,
                <span class='s'>"user"</span> <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sprintf.html'>sprintf</a></span><span class='o'>(</span><span class='s'>"https://api.twitter.com/1.1/users/show.json?screen_name=%s&amp;user_id=%s&amp;count=200"</span>,
                                 <span class='nv'>screen_name</span>, <span class='nv'>user_id</span><span class='o'>)</span>,
                <span class='s'>"timeline"</span> <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sprintf.html'>sprintf</a></span><span class='o'>(</span><span class='s'>"https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&amp;user_id=%s&amp;count=200&amp;include_rts=true"</span>,
                                     <span class='nv'>screen_name</span>, <span class='nv'>user_id</span><span class='o'>)</span>,
                <span class='s'>"mentions"</span> <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sprintf.html'>sprintf</a></span><span class='o'>(</span><span class='s'>"https://api.twitter.com/1.1/search/tweets.json?q=%%40%s&amp;count=200"</span>, <span class='nv'>screen_name</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nv'>r</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://httr.r-lib.org/reference/GET.html'>GET</a></span><span class='o'>(</span><span class='nv'>url</span>, <span class='nv'>token</span><span class='o'>)</span>
  <span class='kr'>if</span> <span class='o'>(</span><span class='nv'>r</span><span class='o'>$</span><span class='nv'>status_code</span> <span class='o'>==</span> <span class='m'>200</span><span class='o'>)</span> <span class='o'>{</span>
    <span class='nf'><a href='https://httr.r-lib.org/reference/content.html'>content</a></span><span class='o'>(</span><span class='nv'>r</span>, type <span class='o'>=</span> <span class='s'>"application/json"</span><span class='o'>)</span>
  <span class='o'>}</span> <span class='kr'>else</span> <span class='o'>{</span>
    <span class='kr'><a href='https://rdrr.io/r/base/stop.html'>stop</a></span><span class='o'>(</span><span class='s'>"Status was not 200"</span><span class='o'>)</span>
  <span class='o'>}</span>
<span class='o'>}</span>
</code></pre>

</div>

Collect user data.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>user</span> <span class='o'>&lt;-</span> <span class='nf'>twitter_data</span><span class='o'>(</span>type <span class='o'>=</span> <span class='s'>"user"</span>, screen_name <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>screen_name</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>,
                     user_id <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>user_id</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>, <span class='nv'>token</span><span class='o'>)</span>
</code></pre>

</div>

Collect the user's timeline.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>timeline</span> <span class='o'>&lt;-</span> <span class='nf'>twitter_data</span><span class='o'>(</span>type <span class='o'>=</span> <span class='s'>"timeline"</span>, screen_name <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>screen_name</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>,
                         user_id <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>user_id</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>, <span class='nv'>token</span><span class='o'>)</span>
</code></pre>

</div>

Collect tweets mentioning the user.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>mentions</span> <span class='o'>&lt;-</span> <span class='nf'>twitter_data</span><span class='o'>(</span>type <span class='o'>=</span> <span class='s'>"mentions"</span>, screen_name <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>screen_name</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>,
                         user_id <span class='o'>=</span> <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>user_id</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>, <span class='nv'>token</span><span class='o'>)</span>
</code></pre>

</div>

### Making the request

Now we can put this all together into a body object.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>body</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>user <span class='o'>=</span> <span class='nv'>user</span>,
             timeline <span class='o'>=</span> <span class='nv'>timeline</span>,
             mentions <span class='o'>=</span> <span class='nv'>mentions</span><span class='o'>)</span>
</code></pre>

</div>

Next we make the Botometer API request.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>result</span> <span class='o'>&lt;-</span>  <span class='nf'><a href='https://httr.r-lib.org/reference/POST.html'>POST</a></span><span class='o'>(</span><span class='s'>"https://rapidapi.p.rapidapi.com/4/check_account"</span>,
                encode <span class='o'>=</span> <span class='s'>"json"</span>,
                <span class='nf'><a href='https://httr.r-lib.org/reference/add_headers.html'>add_headers</a></span><span class='o'>(</span><span class='s'>"X-RapidAPI-Host"</span> <span class='o'>=</span> <span class='s'>"botometer-pro.p.rapidapi.com"</span>,
                            <span class='s'>"X-RapidAPI-Key"</span> <span class='o'>=</span> <span class='nv'>RAPIDAPI_KEY</span>,
                            useQueryString <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
                body <span class='o'>=</span> <span class='nf'>jsonlite</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/jsonlite/man/fromJSON.html'>toJSON</a></span><span class='o'>(</span><span class='nv'>body</span>, null <span class='o'>=</span> <span class='s'>"null"</span>, digits <span class='o'>=</span> <span class='m'>50</span>,
                                        auto_unbox <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Check the user's score.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>out</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://httr.r-lib.org/reference/content.html'>content</a></span><span class='o'>(</span><span class='nv'>result</span>, <span class='s'>"parsed"</span>, encoding <span class='o'>=</span> <span class='s'>"UTF-8"</span><span class='o'>)</span>
<span class='nv'>out</span><span class='o'>$</span><span class='nv'>display_scores</span><span class='o'>$</span><span class='nv'>english</span><span class='o'>$</span><span class='nv'>overall</span>

<span class='c'>#&gt; [1] 1.4</span>
</code></pre>

</div>

Botometer ranks accounts on a scale of 1 to 5, with higher scores indicating a higher likelihood of being a bot. With a score of 1.4, it looks like @wilson\_the\_dogg isn't a bot after all, just someone who feels the need to tweet a hashtag dozens of times in the space of a week! In fact, at the time of writing this post, [Bot Sentinel](https://botsentinel.com/) (which identifies malicious users, not just bots) had classed this user as "[disruptive](https://web.archive.org/web/20201012123742/https://botsentinel.com/profile/3380224119)". *Note, a Bot Sentinel API is marked as "coming soon".*

We can check the second user as well. We can stick all of the requests into one call (usually a bad idea).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>result</span> <span class='o'>&lt;-</span>  <span class='nf'><a href='https://httr.r-lib.org/reference/POST.html'>POST</a></span><span class='o'>(</span><span class='s'>"https://rapidapi.p.rapidapi.com/4/check_account"</span>,
                encode <span class='o'>=</span> <span class='s'>"json"</span>,
                <span class='nf'><a href='https://httr.r-lib.org/reference/add_headers.html'>add_headers</a></span><span class='o'>(</span><span class='s'>"X-RapidAPI-Host"</span> <span class='o'>=</span> <span class='s'>"botometer-pro.p.rapidapi.com"</span>,
                            <span class='s'>"X-RapidAPI-Key"</span> <span class='o'>=</span> <span class='nv'>RAPIDAPI_KEY</span>,
                            useQueryString <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>,
                body <span class='o'>=</span> <span class='nf'>jsonlite</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/jsonlite/man/fromJSON.html'>toJSON</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/lapply.html'>lapply</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span>user <span class='o'>=</span> <span class='s'>"user"</span>, timeline <span class='o'>=</span> <span class='s'>"timeline"</span>,
                                                 mentions <span class='o'>=</span> <span class='s'>"mentions"</span><span class='o'>)</span>,
                                               <span class='nv'>twitter_data</span>, <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>screen_name</span><span class='o'>[</span><span class='m'>2</span><span class='o'>]</span>,
                                               <span class='nv'>counts</span><span class='o'>$</span><span class='nv'>user_id</span><span class='o'>[</span><span class='m'>2</span><span class='o'>]</span>, <span class='nv'>token</span><span class='o'>)</span>,
                                        null <span class='o'>=</span> <span class='s'>"null"</span>, digits <span class='o'>=</span> <span class='m'>50</span>,
                                        auto_unbox <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Let's see the user's score.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>out</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://httr.r-lib.org/reference/content.html'>content</a></span><span class='o'>(</span><span class='nv'>result</span>, <span class='s'>"parsed"</span>, encoding <span class='o'>=</span> <span class='s'>"UTF-8"</span><span class='o'>)</span>
<span class='nv'>out</span><span class='o'>$</span><span class='nv'>display_scores</span><span class='o'>$</span><span class='nv'>english</span><span class='o'>$</span><span class='nv'>overall</span>

<span class='c'>#&gt; [1] 1.6</span>
</code></pre>

</div>

It looks like @overthemoonmark is even less of a bot, but Bot Sentinel thinks that the user has "[questionable](https://web.archive.org/web/20201012125026/https://botsentinel.com/profile/278770541)" activity.

Blocking users
--------------

Bots or not, can we block these users? **rtweet** includes functions to mute users ([`post_mute()`](https://rdrr.io/pkg/rtweet/man/post_follow.html)), but there doesn't seem to be any function to block users. Let's write one, borrowing some of the internal functions from **rtweet** (though we could just as easily use the `POST` function from **httr** as we did for the Botometer API).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>block_user</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>screen_name</span>, <span class='nv'>user_id</span>, <span class='nv'>token</span> <span class='o'>=</span> <span class='kc'>NULL</span><span class='o'>)</span> <span class='o'>{</span>
  <span class='c'>## The rtweet authors turn off scientific notation so that </span>
  <span class='c'>## R doesn't misrepresent the user_id strings.</span>
  <span class='nv'>op_enc</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/options.html'>getOption</a></span><span class='o'>(</span><span class='s'>"encoding"</span><span class='o'>)</span>
  <span class='nv'>op_sci</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/options.html'>getOption</a></span><span class='o'>(</span><span class='s'>"scipen"</span><span class='o'>)</span>
  <span class='nf'><a href='https://rdrr.io/r/base/on.exit.html'>on.exit</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>sencodingcipen <span class='o'>=</span> <span class='nv'>op_sci</span>,
                  encoding <span class='o'>=</span> <span class='nv'>op_enc</span><span class='o'>)</span>, add <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
  <span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>scipen <span class='o'>=</span> <span class='m'>14</span>, encoding <span class='o'>=</span> <span class='s'>"UTF-8"</span><span class='o'>)</span>

  <span class='nv'>query</span> <span class='o'>&lt;-</span> <span class='s'>"blocks/create"</span>
  <span class='nv'>token</span> <span class='o'>&lt;-</span> <span class='nf'>rtweet</span><span class='nf'>:::</span><span class='nf'>check_token</span><span class='o'>(</span><span class='nv'>token</span><span class='o'>)</span>
  <span class='nv'>get</span> <span class='o'>&lt;-</span> <span class='kc'>FALSE</span>
  <span class='nv'>params</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/list.html'>list</a></span><span class='o'>(</span>screen_name <span class='o'>=</span> <span class='nv'>screen_name</span>, user_id <span class='o'>=</span> <span class='nv'>user_id</span><span class='o'>)</span>
  <span class='nv'>url</span> <span class='o'>&lt;-</span> <span class='nf'>rtweet</span><span class='nf'>:::</span><span class='nf'>make_url</span><span class='o'>(</span>query <span class='o'>=</span> <span class='nv'>query</span>, param <span class='o'>=</span> <span class='nv'>params</span><span class='o'>)</span>
  <span class='nv'>resp</span> <span class='o'>&lt;-</span> <span class='nf'>rtweet</span><span class='nf'>:::</span><span class='nf'>TWIT</span><span class='o'>(</span>get <span class='o'>=</span> <span class='nv'>get</span>, <span class='nv'>url</span>, <span class='nv'>token</span><span class='o'>)</span>
  <span class='nf'>rtweet</span><span class='nf'>:::</span><span class='nf'>from_js</span><span class='o'>(</span><span class='nv'>resp</span><span class='o'>)</span>
<span class='o'>}</span>
</code></pre>

</div>

Let's look at users that have tweeted the hashtag more than a dozen times, and block them.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>counts</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>n</span> <span class='o'>&gt;</span> <span class='m'>12</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>highn</span>

<span class='nv'>highn</span>

<span class='c'>#&gt; <span style='color: #555555;'># A tibble: 3 x 3</span></span>
<span class='c'>#&gt;   screen_name     user_id        n</span>
<span class='c'>#&gt;   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>           </span><span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span>      </span><span style='color: #555555;font-style: italic;'>&lt;int&gt;</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>1</span><span> wilson_the_dogg 3380224119    34</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>2</span><span> overthemoonmark 2817035628    32</span></span>
<span class='c'>#&gt; <span style='color: #555555;'>3</span><span> OttawaGordon    278770541     31</span></span>
</code></pre>

</div>

Now we can block some of these users! Our function only works on one user at a time, but we lazily vectorize it using [`Vectorize()`](https://rdrr.io/r/base/Vectorize.html). The Twitter API users a user information object for the API calls to the [blocks/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-blocks-create) endpoint. I'm not interested in printing all of this information in this blog post, so I'll store it into an R object, but this isn't technically necessary.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>block_user_v</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/Vectorize.html'>Vectorize</a></span><span class='o'>(</span><span class='nv'>block_user</span><span class='o'>)</span>
<span class='nv'>rs</span> <span class='o'>&lt;-</span> <span class='nf'>block_user_v</span><span class='o'>(</span><span class='nv'>highn</span><span class='o'>$</span><span class='nv'>screen_name</span>, <span class='nv'>highn</span><span class='o'>$</span><span class='nv'>user_id</span><span class='o'>)</span>
</code></pre>

</div>

If all of our requests succeeded, then I won't see any tweets from @wilson\_the\_dogg, @overthemoonmark, or @OttawaGordon anymore!

Now we know how we can find and block antagonistic Twitter users from R. This is probably useful if you want to occasionally check and block users, but for more permanent protection, you might consider a tool like Bot Sentinel's [autoblocker](https://botsentinel.com/advanced-tools/autoblocker/settings).

------------------------------------------------------------------------

*This post was compiled on 2020-10-12 14:42:24. Since that time, there may have been changes to the packages that were used in this post. If you can no longer use this code, please notify the author in the comments below.*

<details>
<summary>Packages Used in this post</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>sessioninfo</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/package_info.html'>package_info</a></span><span class='o'>(</span>dependencies <span class='o'>=</span> <span class='s'>"Depends"</span><span class='o'>)</span>

<span class='c'>#&gt;  package     * version    date       lib source                         </span>
<span class='c'>#&gt;  askpass       1.1        2019-01-13 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  downlit       0.2.0      2020-09-25 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  dplyr       * 1.0.2      2020-08-18 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  fs            1.5.0      2020-07-31 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  glue          1.4.2      2020-08-27 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  httr        * 1.4.2      2020-07-20 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-10-08 [1] Github (r-lib/hugodown@18911fc)</span>
<span class='c'>#&gt;  jsonlite      1.7.1      2020-09-07 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  knitr         1.30       2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  labeling      0.3        2014-08-23 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  openssl       1.4.3      2020-09-18 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  pillar        1.4.6      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  purrr         0.3.4      2020-04-17 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  rlang         0.4.7      2020-07-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  rmarkdown     2.3        2020-06-18 [1] RSPM (R 4.0.1)                 </span>
<span class='c'>#&gt;  rtweet      * 0.7.0      2020-01-08 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  stringi       1.5.3      2020-09-09 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  stringr       1.4.0      2019-02-10 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  tibble      * 3.0.3      2020-07-10 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt;  vctrs         0.3.4      2020-08-29 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  withr         2.3.0      2020-09-22 [1] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  xfun          0.18       2020-09-29 [2] RSPM (R 4.0.2)                 </span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1] RSPM (R 4.0.0)                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /home/conor/Library</span>
<span class='c'>#&gt; [2] /usr/local/lib/R/site-library</span>
<span class='c'>#&gt; [3] /usr/local/lib/R/library</span>
</code></pre>

</div>

</details>

