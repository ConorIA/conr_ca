---
output: hugodown::md_document
title: Extended Heat Warning Interview with UTSC Communications
author: ''
date: '2018-07-31'
slug: heat-warning-interview
tags:
  - Toronto
  - heat
bibliography: "../../../static/files/bib/bibliography.bib"
link-citations: true
summary: "An interview with UTSC Communications regarding my recent analysis of heat warning weather at Toronto Pearson"
rmd_hash: c8570ffffed47250

---

This is an extended version of my interview with UTSC Communications writer Alexa Battler about my [recent post](https://conr.ca/post/toronto-heat-warning-weather-yyz/) on heat warning weather in Toronto. The abridged version will appear in the UTSC newsletter, and I'll update this post with a link when it lands.

------------------------------------------------------------------------

**AB: You state in your blog post examining heat patterns in Toronto that, while 2018 has already seen three heat warnings, it is not far above average (2.72 warnings). Why, then, has it felt like such a hot summer?**

CA: It has felt like a hot summer this year because it has been a hot summer! Here we can make a distinction between weather extremes, like those that I focused on in my blog, and the overall seasonal average. If we look at the Period from June 1 to July 15 over the past 30 years, 2018 has been the third warmest, with a mean hourly temperature of 21.5°C. The 30-year average over the same time period is 20.1°C. The nighttime temperture was the most affected, it seems, with an average nighttime minimum of 17.0°C compared to the overall average of 15.5°C over the same period since 1988. This summer especially we've had a couple of really nasty warm periods, which have nudged those averages up. Of course, we can put a special emphasis on your use of the word "felt" in this question: I think it is human nature to feel the extremes rather than the "every day" weather. We've had lots of days this summer that saw temperatures in the low to mid 20s, close to what we'd expect as "seasonal" for Toronto. Those don't tend to be the days that stick out in our memory, though!

**AB: Does it look like we will beat the record of six heat-warning events from 2002?**

CA: This is a great question, and something that I "wondered aloud" in my blog post. However, I hesitate to give a yes or no answer. As a climatologist, I look at long-term deviations from average conditions, but don't examine the short- to medium-term projections that fit within the realm of meteorology. Climatologically speaking, July is the warmest month of the year in Toronto, which means that it is also the month during which we are most likely to experience extreme heat events. Indeed, of the 68 times that we experienced the kind of summertime weather that merits a heat warning since 1988, 55 of them occurred during the months of June or July, with just 13 occurring during the month of August. So, while it is less common to have the kind of weather that earns itself a heat warning in August, it is certainly not impossible. Up to July 19, 2018, we had experienced weather at the level of a heat alert three times (though two of those could, technically be considered one long event). I think that it is unlikely that we top the 2002 record of 6 warnings, but I might have to eat my words by the time the fall terms rolls around in September.

**AB: In a UTSC article from February, you mentioned how jet streams influenced the severe cold of last winter. What role do these jet streams play in our extremely cold winters, followed by extremely hot summers?**

CA: The northern polar jet stream occurs between the cold, polar air masses, and warmer air from the temperate and tropical areas of the northern hemisphere. During the winter, when the northern hemisphere leans away from the sun, the entire pattern of the earth's climate system is shifted south, which is why we can expect the jet stream to give us a nice wallop of cold air sporadically during the winter. In the summer, however, the opposite is true, the northern half of the Earth tilts towards the sun, and we see a slight poleward migration of the polar jet, away from Toronto. When we experience hot, tropical weather in the city. That usually means that the jet stream is far off to the north. In that sense, it is really the *absence* of the jet stream that leads to extremely hot summers. I should mention, however, that some recent studies have suggested that the same blocking patterns that can cause mild winters in western Canada and cold winters in eastern Canada (which I mentioned in the February article) can also affect summer temperatures, causing severe heatwaves in the west and nasty, stormy weather in the east. For instance, [NOAA reported](https://www.ncdc.noaa.gov/sotc/synoptic/201707) that the stormy weather in July 2017 was a result of cold fronts, moving south along the jet stream into the eastern United States, and trapping moisture from the Gulf of Mexico and the Atlantic, leading to much higher amounts of rain than are normal.

**AB: In your blog post, you started your analysis in 1988, the second-hottest period of heat warning weather in the last 30 years. The mid-2000s also became extremely hot, but there were several insignificant years in the interim, including 2017. What accounts for these major variations in weather?**

CA: Our climate system is characterized by a huge amount of variability, with different conditions across time, space, and even scale. This variation can occur on many scales, but in the case of these summertime heat events, they are mostly at the synoptic scale, driven by large air masses moving around the continent. I can't comment on each year individually without performing an appropriate analysis, but we can often find some hints as to what could have caused past weather conditions. The year 1992, for instance, was much cooler than normal after the Mt. Pinatubo eruption in 1991; there were no heat warnings in 1992. There were also no heat warnings in 2014; Gough and Sokappadu ([2015](#ref-gough_2015_climate)) found that July 2014 experienced less tropical air than is seasonal, making for a cooler temperatures than those that we would typically expect for July. The year 2017 also saw less tropical air than is seasonal, with just one heat warning between June 11 and 12, when there was an incursion of dry tropical air masses into the city.

**AB: Regarding these variations in weather, you state in your blog that you are "interested to follow this topic into the future, as we certainly seem to be seeing something since around the year 2000." Could you explain what that "something" might be and how you would go about tracking it?**

CA: In my blog post, I looked at the past 30 years, as 30 years is what we usually consider the minimum amount of time to detect changing patterns, or signals, in a climate series. I didn't detect any statistically significant changes in the maximum daily temperature, the maximum daily humidex, or the number of summer days that met the heat warning criteria over that period. If I expand my analysis back an extra two decades, however, *all* of those variables have shown significant increases. Indeed, in the 1970s, there were an average of 0.6 heat warnings per year. This number increased to 1.0 in the 1980s, 1.7 in the 1990s, and again jumped to 2.5 in the first decade of the 2000s. From 2010 to 2017, we've had an average of 2.4 summertime heat warnings per year, a slight decrease, but similar to the previous decade. Given the above, perhaps the signal that I am looking for actually began before the 2000s, toward the end of the 1980s and into the 1990s. The rapid increase here is interesting to me, but there is still much to unpack from these numbers. First, and foremost, I got my temperature data from the Toronto Pearson airport, and we know from previous studies (Mohsin and Gough [2010](#ref-mohsin_2010_trend)) that a large degree of the temperature changes at this site was due to urbanization in the area around the airport during the latter decades of the 20<sup>th</sup> century. (Note, I explain some of the warming associated with urbanization in general in my most recent publication, see Anderson, Gough, and Mohsin ([2018](#ref-anderson_2018_characterization))). The fact that the increase appears to have stalled between 2000 and the present is also interesting, as it could suggest that the area around the airport is now fully urbanized and all of the microclimatic changes that urbanization brings have already stabilized as Mohsin and Gough ([2010](#ref-mohsin_2010_trend)) suggested. The next step to analyze this problem would be to find a nearby rural station and look for concomitant changes between the two stations. Mohsin and Gough ([2012](#ref-mohsin_2012_characterization)) did this for the period from 1971 to 2000, and found that around half of the total warming at Pearson over that time period. Performing this analysis past 2000 would allow me to continue to tease apart the background changes due to climate change, and the shorter-term changes from urbanization, and to see whether the changes due to urbanization have continued. Unfortunately, there are very few stations that report rural climate data since the early 2000s, so analyses of this type have become more difficult.

**AB: The Canada Day 2018 heat event was the longest in the past 30 years, if we leave room for one brief moment of "relief." What kind of significance does this milestone have?**

CA: I think that the Canada day heat event becomes more interesting within the wider context of this summer. In the month of July, we've seen temperature records broken across the world, an extended, deadly heatwave in Japan, and sweltering temperatures across the world. In fact, the CBC put together a great round up for a recent issue of the [National newsletter](https://links.lists.cbc.ca/v/443/6bfb647e3a526fec32ff01250c27cbe7d99b7fa29a6b4c8b23f1f694ea285081). Our heatwave, which was really part of a continental scale event, is just another notch in the belt of summer 2018! Myriad studies exist that suggest that we should expect climate extremes to become more frequent, so this year may just be a preview of whats to come!

**AB: You state that super-hot weather in Toronto is due to warm fronts of hot, tropical air. Why was there such an absence of polar air this summer? And why did the tropical air hit so hard?**

CA: There seem to be a combination of factors that have led to the large proportion of tropical air over Toronto. This year in particular saw the formation of what some meteorologists call a "heat dome", with the development of a ridge of high pressure south of the jet stream that trapped air and caused it to sink (and therefore compress and become warmer). However, this seems to be part of a larger pattern of change in the summertime composition of air masses at Toronto Pearson. We can examine the air mass classifications provided by Sheridan ([2002](#ref-Sheridan_2002_redevelopment)). In this 1970s, about one quarter of summer days showed air of polar origin. This proportion dropped to below one fifth in the 1980s, 13% in 1990s, 8% in the first decade of the 2000s, and 5% between 2010 and 2017. This polar air has largely been replaced by tropical air, which increased from around 20% in the 1970s up to 35% over the past 8 years. The change in "moderate" air masses has only changed around 6%, from 47% in the 70s to 53% by the 1990s, but this number has remained relatively flat since then. So we are seeing, overall, the replacement of polar air by tropical air, which seems to be indicative of lagged changes in the summertime behaviour of the polar jet and a *really interesting* topic for further study.

**AB: Would you consider this weather to be proof of global warming?**

CA: Given that there is so much variation in the climate system, I don't think that we can ever say that one or a few weather events are *proof* of global warming. Indeed, as a scientist, I set out to disprove hypothesis, rather than to prove them. That said, our understanding of the greenhouse effect is based in the laws of physics, so we understand the mechanisms that drive global warming. We also have a long-term observational record that provides significant evidence that the earth is warming. In that sense, global warming really is "settled science". Also, a recent study published in Science by researchers form the U.S. Lawrence Livermore National Laboratory (Santer et al. [2018](#ref-santer_2018_human)) found that human-driven climate change has caused an increase in the difference in the temperature of the lower atmosphere between the coldest month and the warmest month of the year, with an especially stark increase in summer temperatures in Russia and over eastern Canada. Previous studies of Canada have also shown significant warming in the winter and at the nighttime; this is consistent with my findings from Pearson: a significant increase in the nightttime minimum temperatures since 1988.

**AB: Was there anything that surprised you while conducting the research from your blog post?**

CA: As a climate scientist, it is sometimes hard to divorce my perception of the weather from the observed data record. For instance, I am living in a new apartment this year, having moved from a basement apartment with air conditioning to the 22<sup>nd</sup> floor of an apartment block. When the hot weather hit at the beginning of June, I was *virtually certain* that I was experiencing some of the hottest temperatures Toronto had ever seen! Of course, as I show in my blog, that is not entirely true! It was hot, and it was long, but I've experienced worse within my lifetime. It is so easy to fall into the trap of believing that every weather event should somehow fulfill a checklist of characteristics that we associate with climate change--increasing temperatures, larger-magnitude extremes, unpredictable precipitation. Instead, we should remember that these are the kinds of changes that we can expect to see over a long period of time, with reference to a baseline period in the past. We cannot expect every summer to be the hottest ever, or that every winter will be milder than the previous one. As climate change continues to wreaks havoc on large-scale weather patterns the world over; I think that the only thing that we can *expect* is that our expectations will probably be wrong!

References:
-----------

<div id="refs" class="references">

<div id="ref-anderson_2018_characterization">

Anderson, Conor I., William A. Gough, and Tanzina Mohsin. 2018. "Characterization of the Urban Heat Island at Toronto: Revisiting the Choice of Rural Sites Using a Measure of Day-to-Day Variation." *Urban Climate* 25 (September): 187--95. <https://doi.org/10.1016/J.UCLIM.2018.07.002>.

</div>

<div id="ref-gough_2015_climate">

Gough, William A., and Srishtee Sokappadu. 2015. "Climate Context of the Cold Summer of 2014 in Toronto, ON, Canada." *Theoretical and Applied Climatology*, July, 1--7. <https://doi.org/10.1007/s00704-015-1571-2>.

</div>

<div id="ref-mohsin_2010_trend">

Mohsin, Tanzina, and William A. Gough. 2010. "Trend Analysis of Long-Term Temperature Time Series in the Greater Toronto Area (GTA)." *Theoretical and Applied Climatology* 101 (3-4): 311--27. <https://doi.org/10.1007/s00704-009-0214-x>.

</div>

<div id="ref-mohsin_2012_characterization">

---------. 2012. "Characterization and Estimation of Urban Heat Island at Toronto: Impact of the Choice of Rural Sites." *Theoretical and Applied Climatology* 108 (April): 105--17. <https://doi.org/10.1007/s00704-011-0516-7>.

</div>

<div id="ref-santer_2018_human">

Santer, Benjamin D., Stephen Po-Chedley, Mark D. Zelinka, Ivana Cvijanovic, Céline Bonfils, Paul J. Durack, Qiang Fu, et al. 2018. "Human Influence on the Seasonal Cycle of Tropospheric Temperature." *Science* 361 (6399): eaas8806. <https://doi.org/10.1126/SCIENCE.AAS8806>.

</div>

<div id="ref-Sheridan_2002_redevelopment">

Sheridan, Scott C. 2002. "The Redevelopment of a Weather-Type Classification Scheme for North America." *International Journal of Climatology* 22 (1): 51--68. <https://doi.org/10.1002/joc.709>.

</div>

</div>

