---
title: "Accounting for missing data in monthly temperature series: Testing rule-of-thumb omission of months with missing values"
authors:
- admin
- William A. Gough
date: "2018-08-27T00:00:00Z"
doi: "10.1002/joc.5801"

publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*International Journal of Climatology*"
publication_short: "*Int. J. Climatol.*"

abstract: The '3/5 rule' is a commonly used rule-of-thumb for dealing with missing data when calculating monthly climate normals. The rule states that any month that is missing more than 3 consecutive daily values, or more than 5 daily values in total, should not be included in calculated monthly climate normals. We quantify the impact of missing data in a given year&ndash;month for between 1 and 25 missing values. As such, we describe the error the '3/5 rule' (and a related rule that we have dubbed the '4/10 rule') permits. We tested the statistical robustness of these rules using observed temperature data from a temperate station and a tropical station. We show that, for observed data, the '3/5 rule' permits an average of between 0.06 and 0.07 standard deviations of error in the calculated monthly mean (_&epsilon;_) when 3 consecutive or 5 random values are missing. For its part, the '4/10 rule' permits a maximum _&epsilon;_ of between 0.07 and 0.09 when four consecutive values are missing, or up to 0.10 when ten random values are missing. The proportional impact of missing values was similar across variables. We performed a correlation analysis, and show that each additional missing value from a year&ndash;month of data increases _&epsilon;_ by between 0.008 and 0.018 for up to 19 missing values. There is a significant relationship between the lag-1 autocorrelation of a year&ndash;month, and _&epsilon;_. _&epsilon;_ can be reduced by simple linear interpolation when values are missing at random and the year&ndash;month exhibits lag-1 autocorrelation. Overall, we find that the application of any \"rule of thumb\" should be based on the particular characteristics of the source data and the goals of the research project.

links:
- name: Wiley Online Library
  url: https://rmets.onlinelibrary.wiley.com/doi/10.1002/joc.5801
url_pdf: https://rmets.onlinelibrary.wiley.com/doi/pdf/10.1002/joc.5801
url_code: 'https://gitlab.com/ConorIA/claut/blob/master/R/missing_value_lab.R'
---
