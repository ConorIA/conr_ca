---
title: "Characterization of the urban heat island at Toronto: Revisiting the choice of rural sites using a measure of day-to-day variation"
authors:
  - admin
  - William A. Gough
  - Tanzina Mohsin
date: "2018-07-17T00:00:00Z"
doi: "10.1016/j.uclim.2018.07.002"

publication_types: ["2"]
publication: "*Urban Climate*"
publication_short: "*Urban Clim.*"

abstract: "The quantification of the Urban Heat Island (UHI) relies on the establishment of urban–rural station pairs for comparison. We revisited the selection of three rural stations (Albion Field Centre, Millgrove, and King Smoke Tree) and two urban stations (Toronto and Toronto Pearson International Airport) that have previously been used to perform UHI analysis in Toronto, Ontario, Canada. We explored the seasonal patterns of day-to-day temperature variation. We employed a derived measure of day-to-day temperature variability, the novel ∆DTD metric—the difference between the variation in daytime maximum temperatures and nighttime minimum temperatures—to determine whether our stations exhibited the ∆DTD values that are characteristic of rural (negative or less positive ∆DTD) and urban (positive or less negative ∆DTD) sites. Our results indicate that both of our urban stations do, indeed, exhibit day-to-day variation that is characteristic of urban stations. Of our three rural sites, Albion Field Centre was found to be the most rural. King Smoke Tree, an agricultural station, showed the highest ∆DTD values of the three rural sites, indicating that ∆DTD is, therefore, a useful tool for the detection of anthropogenic disturbance and the evaluation of urban and rural members of urban–rural station pairs."

links:
- name: ScienceDirect
  url: https://www.sciencedirect.com/science/article/pii/S2212095518301706
url_pdf: https://www.sciencedirect.com/science/article/pii/S2212095518301706/pdfft?md5=e4a161c78e20c959d4405874c06f20b8&pid=1-s2.0-S2212095518301706-main.pdf
url_code: https://gitlab.com/ConorIA/claut/blob/master/R/deltaDTD.R
---


