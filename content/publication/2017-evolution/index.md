---
title: "Evolution of Winter Temperature in Toronto, Ontario, Canada: A Case Study of Winters 2013/14 and 2014/15"
authors:
- admin
- William A. Gough
date: "2017-04-06T00:00:00Z"
doi: "10.1175/JCLI-D-16-0562.1"

publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Journal of Climate*"
publication_short: "*J. Climate*"

abstract: "Globally, 2014 and 2015 were the two warmest years on record. At odds with these global records, eastern Canada experienced pronounced annual cold anomalies in both 2014 and 2015, especially during the 2013&#47;14 and 2014&#47;15 winters. This study sought to contextualize these cold winters within a larger climate context in Toronto, Ontario, Canada. Toronto winter temperature (_T<sub>max</sub>_, _T<sub>min</sub>_, and _T<sub>mean</sub>_) for the 2013&#47;14 and 2014&#47;15 seasons were ranked among all winters for three periods: 1840&#47;41 to 2015 (175 winters), 1955&#47;56 to 2015 (60 winters), and 1985&#47;86 to 2015 (30 winters), and the average warming trend for each temperature metric during these three periods was analyzed using the Mann&ndash;Kendall test and Thiel&ndash;Sen slope estimation. Winter 2013&#47;14 and 2014&#47;15 were the 34th and 36th coldest winters in Toronto since record-keeping began in 1840, however these events are much rarer, relatively, over shorter periods of history. Overall, Toronto winter temperatures have warmed considerably since winter 1840&#47;41. The Mann&ndash;Kendall analysis showed statistically significant monotonic trends in winter _T<sub>max</sub>_, _T<sub>min</sub>_, and _T<sub>mean</sub>_ over the last 175 years, and 60 years. These trends notwithstanding, there has been no clear signal in Toronto winter temperature since 1985&#47;86. However, there was a statistically significant increase in the diurnal temperature range in that period, indicating an expansion of winter extremes. We propose that the possible saturation of urban heat island-related warming in Toronto may partially explain this increase in variation. We also identify anomalies in the position of the polar jetstream over Toronto during these cold events. We find no direct influence of major teleconnections on Toronto winter temperature."

links:
 - name: "AMS Journals"
   url: "https://journals.ametsoc.org/doi/abs/10.1175/JCLI-D-16-0562.1"
url_pdf: http://journals.ametsoc.org/doi/pdf/10.1175/JCLI-D-16-0562.1
---
