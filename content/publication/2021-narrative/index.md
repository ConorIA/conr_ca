---
title: "A narrative approach to building computational capacity for climate change impact assessment in professional master's students"
authors:
  - admin
  - Karen L. Smith
date: "2021-12-30T00:00:00Z"
doi: "10.21105/jose.00100"

publication_types: ["2"]
publication: "*The Journal of Open Source Education*"
publication_short: "*JOSE*"

abstract: "With the growing recognition of the cascading consequences of climate change, there is an increasing demand for graduate education in climate change impact assessment (CCIA). To facilitate improved transparency and technical skill-building in CCIA, we have developed a new series of step-by-step, coherently narrated, open-source Python labs aimed at building professional master’s students’ computational capacity and confidence, while providing foundational knowledge in CCIA and the opportunity to engage with state-of-the-art methods and data. The labs are presented in an open-source (CC-BY-SA 4.0) lab manual entitled Climate Change Impact Assessment: A practical walk-through, featuring accessibly annotated code that can be used both for independent study, or during interactive live-coding lab sessions."

links:
- name: The Open Journal
  url: https://jose.theoj.org/papers/10.21105/jose.00100
url_pdf: https://www.theoj.org/jose-papers/jose.00100/10.21105.jose.00100.pdf
url_code: https://zenodo.org/record/5807576
---


