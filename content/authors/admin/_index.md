---
# Display name
name: Conor I. Anderson, PhD

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Alumnus, Climate Lab

# Organizations/Affiliations
organizations:
- name: University of Toronto Scarborough
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: Conor is a recent PhD graduate from the Department of Physical and Environmental Sciences at the University of Toronto Scarborough (UTSC).

interests:
- Climate Change
- Extreme Temperatures
- R
- Open Methodologies
- Open Science

education:
  courses:
  - course: PhD in Environmental Science
    institution: University of Toronto Scarborough
    year: 2022
  - course: BSc in International Development Studies (co-op specialist), major in Environmental Biology
    institution: University of Toronto Scarborough
    year: 2012


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: mastodon
  icon_pack: fab
  link: https://socl.conr.ca/@climoconor
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/climoconor
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/ConorIA
- icon: github
  icon_pack: fab
  link: https://github.com/ConorIA
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-2619-1207
- icon: researchgate
  icon_pack: ai
  link: https://researchgate.net/profile/Conor_Anderson
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- []
---

Conor is a recent PhD graduate from the Department of Physical and Environmental Sciences at the University of Toronto Scarborough (UTSC). Conor's research focused on changes in multi-day temperature extremes across Canada. 
