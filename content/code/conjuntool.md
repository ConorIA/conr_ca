---
title: Conjuntool
summary: A open-source platform for the visualization, analysis, and sub-setting of CMIP5 climate data.
tags:
  - R
  - shiny
  - open_data
  - open_methods
date: 2018-01-26
external_link: https://gitlab.com/ConorIA/conjuntool
---
