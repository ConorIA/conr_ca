---
title: claut
summary: Collected functions developed at the University of Toronto Climate lab.
tags:
  - R
  - open_methods
date: 2016-08-21

external_link: https://gitlab.com/ConorIA/claut
---
