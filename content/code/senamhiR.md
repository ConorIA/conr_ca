---
title: senamhiR
summary: A collection of functions to obtain Peruvian climatological or hydrological data from the Peruvian National Meterology and Hydrology Service
tags:
  - R
  - open_data
date: 2016-08-22
external_link: https://gitlab.com/ConorIA/senamhiR
---
