---
title: ec3
summary: A Python program and standalone command-line executable to search for and download historical Canadian climate data from Environment and Climate Change Canada's historical data archive
tags:
  - Python
  - open_data
date: 2019-01-27
external_link: https://gitlab.com/ConorIA/ec3.py
---

