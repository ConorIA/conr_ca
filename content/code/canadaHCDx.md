---
title: canadaHCDx
summary: Additional functions for G. L. Simpson's [canadaHCD](https://github.com/gavinsimpson/canadaHCD) package for acquiring Canadian climate data
tags:
  - R
  - open_methods

date: 2017-01-15
external_link: https://gitlab.com/ConorIA/canadaHCDx

image:
  caption: ''
  focal_point: ''
---
