#! /bin/bash
cd ..
Rscript -e "hugodown::hugo_build()"
sudo rsync -a $PWD/public/* /srv/http/testsite/
sudo chown -R root:root /srv/http/testsite
sudo systemctl restart nginx
